<?php
  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Leave_report extends CI_Controller {

	public function __construct(){

		parent::__construct();
		
		$this->load->model('employee_model');
		$this->load->model('user_model','user');
		$this->load->helper('url');
	}

	
	public function _remap($method, $params = []){
		$login_session = $this->session->userdata('logged_in');

		if(isset($login_session)){
			
			$is_password_change =$this->session->userdata['logged_in']['is_password_change'];
			if($is_password_change == 0){
				redirect('auth/change-password', 'location');
			}

			$this->$method($params);
		}else{
			redirect('auth/index', 'location');
		}
	}


	public function index(){
		$employeeid = $this->session->userdata['logged_in']['employeeid'];
		$data['user_details'] = $this->user->get_details($employeeid);

		///////////////////////////////////////////////////////////////////////////////	
		$data['employee'] = $this->employee_model->get_employee_details($employeeid);
		$data['users'] = $this->employee_model->get_user_id($employeeid);
		$data['header'] = 'LEAVE REPORT';
		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Leave/index',$data,'app');

}
