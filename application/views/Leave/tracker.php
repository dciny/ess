<div class="container-fluid">
  <div class="row mb-4">
    <div class="col-md-12">
      <div class="ml-4">
        <a class="cta-def-btn" href="<?php echo base_url(); ?>">
          <span class="back-icon">Back to home</span>
        </a>
      </div>
    </div>
  </div>
</div>

<div class="border mx-4 leave-dashboard">
	<div class="border-bottom">
		<ul class="d-flex m-0">
			<li class="p-4"><a href="<?php echo base_url('leave') ?>">Dashboard</a></li>
			<li class="p-4"><a href="<?php echo base_url('leave/apply') ?>">Apply Leave</a></li>
			<?php if($user_details->leave_role_id == 1){ ?>
				<li class="p-4"><a href="<?php echo base_url('leave/delegate') ?>">Leave Delegate</a></li>
			<?php } ?>
			<?php if($is_approver){ ?>
				<li class="p-4"><a href="<?php echo base_url('leave/requests') ?>">Leave Requests</a></li>
				<li class="p-4"><a href="<?php echo base_url('leave/approver/leave-count-page') ?>">Leave Count</a></li>
			<?php } ?>
			<li class="p-4 leave-tab-active">Leave Tracker</li>
			<?php if($user_details->leave_role_id == 1){ ?>
				<li class="p-4"><a href="<?php echo base_url('leave/monthly-report/approved') ?>">Monthly Tracker</a></li>
			<?php } ?>
		</ul>
	</div>
	<div class="p-3 table-responsive">
		<table class="table">
			<thead class="thead-dark">
				<tr>
					<th class="text-center">Employee Id</th>
					<th class="text-center">Last Name</th>
					<th class="text-center">First Name</th>
					<th class="text-center">MOD Credit</th>
					<th class="text-center">Leave Balance</th>
					<th class="text-center">Accumulated Leaves</th>
					<th class="text-center">VL</th>
					<th class="text-center">SL</th>
					<th class="text-center">BVMT</th>
					<th class="text-center">LPAT</th>
					<th class="text-center">LMAT</th>
					<th class="text-center">PTO</th>
					<th class="text-center">EL</th>
					<th class="text-center">Unpaid</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($employees as $employee) { ?>
					<tr>
						<td class="text-center"><?php echo $employee->employeeid ?></td>
						<td class="text-center"><?php echo $employee->lastname ?></td>
						<td class="text-center"><?php echo $employee->firstname ?></td>
						<td class="text-center"><?php echo $employee->leave_accrual ?></td>
						<td class="text-center"><?php echo $employee->leave_balance ?></td>
						<td class="text-center"><?php echo $employee->leave_total ?></td>
						<td class="text-center <?php echo isset($employee->vl) ? 'bg-success text-light' : '' ?>" ><?php echo isset($employee->vl) ? $employee->vl : 0 ?></td>
						<td class="text-center <?php echo isset($employee->sl) ? 'bg-success text-light' : '' ?>"><?php echo isset($employee->sl) ? $employee->sl : 0 ?></td>
						<td class="text-center <?php echo isset($employee->bvmt) ? 'bg-success text-light' : '' ?>"><?php echo isset($employee->bvmt) ? $employee->bvmt : 0 ?></td>
						<td class="text-center <?php echo isset($employee->lpat) ? 'bg-success text-light' : '' ?>"><?php echo isset($employee->lpat) ? $employee->lpat : 0 ?></td>
						<td class="text-center <?php echo isset($employee->lmat) ? 'bg-success text-light' : '' ?>"><?php echo isset($employee->lmat) ? $employee->lmat : 0 ?></td>
						<td class="text-center <?php echo isset($employee->pto) ? 'bg-success text-light' : '' ?>"><?php echo isset($employee->pto) ? $employee->pto : 0 ?></td>
						<td class="text-center <?php echo isset($employee->el) ? 'bg-success text-light' : '' ?>"><?php echo isset($employee->el) ? $employee->el : 0 ?></td>
						<td class="text-center <?php echo isset($employee->unpaid) ? 'bg-success text-light' : '' ?>"><?php echo isset($employee->unpaid) ? $employee->unpaid : 0 ?></td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>