<?php
  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct(){

		parent::__construct();
		
		$this->load->model('auth_model');
		$this->load->model('employee_model');
		$this->load->model('sendmail_model');
		$this->load->helper('url');
	}

	private function is_logged_in(){
		$login_session = $this->session->userdata('logged_in');
		if(isset($login_session)){
			return true;
		}
		return false;
	}


	public function index(){
		$this->load->view('Auth/index');
	}


	public function login(){
		$this->form_validation->set_rules('username', 'username', 'trim|required');
		$this->form_validation->set_rules('password', 'password', 'trim|required|callback_validate_password');

		if($this->form_validation->run()){
			$data = array(
							'username' => $this->input->post('username'),
							'password' => $this->input->post('password')
						 );

			$employee = $this->auth_model->get_user_info($data['username']);
			$user = $this->auth_model->get_user($data['username']);

			$session_data = array(
				'employeeid' => $employee->employeeid,
				'is_password_change' => $user->is_password_change
			);

			$this->session->set_userdata('logged_in',$session_data);
			if($user->is_password_change == 1){
				redirect('dashboard/index','location');
			}else{
				redirect('auth/change-password','location');
			}
		}else{
 			$this->session->set_flashdata('error','Invalid Username or Password');
			redirect('auth/index');
 		}

 	}


 	public function logout(){
		if($this->is_logged_in()){
			session_destroy();
		}
		redirect('auth/index');
	}


 	public function change_password(){
 		if($this->is_logged_in()){
	 		$username = $this->session->userdata['logged_in']['employeeid'];
	 		$emp['is_password_change'] = $this->session->userdata['logged_in']['is_password_change'];
	 		$emp['employee'] = $this->auth_model->get_user_info($username);
	 		$emp['users'] = $this->employee_model->get_user_id($username);
	 		$emp['username'] = $username;

	 		$this->load->library('layouts'); // load layout library
			$this->layouts->view('Auth/change_password',$emp,'app');
		}else{
			redirect('auth/index');
		}
 	}


 	public function update_password(){
 		if($this->is_logged_in()){
 			$this->form_validation->set_error_delimiters('<p class="invalid-feedback m-0">', '</p>');
	 		$this->form_validation->set_rules('old_pw', 'Old password', 'trim|required|callback_validate_password');
	 		$this->form_validation->set_rules('new_pw', 'New password', 'trim|required');
	 		$this->form_validation->set_rules('confirm_pw', 'New password', 'trim|required|matches[new_pw]');

	 		$username = $this->session->userdata['logged_in']['employeeid'];
	 		$new_pw = $this->input->post('new_pw');
	 		$emp['employee'] = $this->auth_model->get_user_info($username);
	 		$emp['is_password_change'] = $this->session->userdata['logged_in']['is_password_change'];
	 		$emp['username'] = $username;
	 		$emp['users'] = $this->employee_model->get_user_id($username);

	 		if ($this->form_validation->run() == FALSE){
				$this->load->library('layouts'); // load layout library
				$this->layouts->view('Auth/change_password',$emp,'app');
			}else{
				$this->auth_model->update_password($username,$new_pw);
				$this->logout();
			}
		}else{
			redirect('auth/index');
		}
 	}


	public function validate_password($pw){
		$username = $this->input->post('username');
		$employee = $this->auth_model->get_user($username);


		if (password_verify($pw, $employee->password)){
            return true;
        }
        else{
        	$this->form_validation->set_message('validate_password', 'The {field} does not match');
            return false;
        }
	}

	public function validate_user($username){
		$user = $this->auth_model->get_user($username);
		$employee = $this->employee_model->is_active_user($username); // returns true or false


		if(isset($user) && $employee){
            return true;
        }
        else{
        	$this->form_validation->set_message('validate_user', 'Please enter a valid {field}');
            return false;
        }
	}


	public function reset_and_hash_password(){
		$users = $this->auth_model->get_all_pw_not_updated_users();
		$def_pw = '123456';

		foreach ($users as $user) {
			$this->auth_model->update_password($user->username,$def_pw);
		}
	}


	public function forgot_password(){
		$data['header'] = 'ESS | Forgot Password';
		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Auth/forgot_password',$data,'auth');
	}


	// public function password_send(){
	// 	$username = $this->input->post('username');
	// 	$msg = '';
	// 	$key = '';

	// 	$this->form_validation->set_rules('username', 'username', 'required|callback_validate_user');


	// 	if ($this->form_validation->run() == FALSE){
	// 		$this->session->set_flashdata('error','User does not exist');
	// 		redirect('auth/forgot-password');
	// 	}else{
	// 		// send email if successfully inserted the request to the table
	// 		if($this->auth_model->create_password_reset($username)){
	// 			// load email library
	// 			$this->load->library('email'); 

	// 			// set email message
	// 			$token = $this->auth_model->get_password_reset_token($username);
	// 			$employee = $this->employee_model->get_employee_details($username);

	// 			$params['employeeid'] = $username;
	// 			$params['token'] = $token;

	// 			$mail_template = $this->sendmail_model->template();
	// 			$mail_header = $this->sendmail_model->password_reset_header();
	// 			$mail_body = $this->sendmail_model->password_reset_body($params);

	// 			$mail_content = str_replace('{mail_body}',$mail_body,str_replace('{mail_header}', $mail_header, $mail_template));

	// 			// set email configurations
	// 			$mail_config = $this->sendmail_model->config();
	// 			$this->email->initialize($mail_config);
	// 			$this->email->set_newline("\r\n");
	// 			$this->email->set_crlf( "\r\n" );

	// 			$this->email->from($mail_config['smtp_user'], 'ESS');
	// 			if(!empty(trim($employee->email1))){
	// 				$this->email->to($employee->email1);
	// 				// $this->email->cc('hr@digicononline.com');
	// 			}else{
	// 				// $this->email->to('hr@digicononline.com');
	// 				$this->email->to('lourencejohn@digicononline.com');
	// 			}
	// 			$this->email->subject(join(' ',['REQUEST: Password Reset -',$employee->firstname,$employee->lastname]));
	// 			$this->email->message($mail_content);

	// 			if($this->email->send()){
	// 				$this->session->set_flashdata('success','Password reset request sent on the registered email');
	// 			}else{
	// 				$this->session->set_flashdata('error',$this->email->print_debugger());
	// 			}

	// 			redirect('auth/forgot-password');
	// 		}
	// 	}
	// }

	public function password_send(){
		$username = $this->input->post('username');
		$msg = '';
		$key = '';

		$this->form_validation->set_error_delimiters('<p class="m-0">', '</p>');
		$this->form_validation->set_rules('username', 'username', 'trim|required|callback_validate_user');


		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('error',form_error('username'));
		}else{
			// send email if successfully inserted the request to the table
			if($this->auth_model->create_password_reset($username)){
				// load email library
				$this->load->library('email'); 

				// set email message
				$token = $this->auth_model->get_password_reset_token($username);
				$employee = $this->employee_model->get_employee_details($username);

				$params['employeeid'] = $username;
				$params['token'] = $token;

				$mail_template = $this->sendmail_model->template();
				$mail_header = $this->sendmail_model->password_reset_header();
				$mail_body = $this->sendmail_model->password_reset_body($params);

				$mail_content = str_replace('{mail_body}',$mail_body,str_replace('{mail_header}', $mail_header, $mail_template));

				// set email configurations
				$mail_config = $this->sendmail_model->config();
				$this->email->initialize($mail_config);
				$this->email->set_newline("\r\n");
				$this->email->set_crlf( "\r\n" );

				$this->email->from($mail_config['smtp_user'], 'ESS');
				if(!empty(trim($employee->email1))){
					$this->email->to($employee->email1);
					$this->email->cc('hr@digicononline.com');
				}else{
					$this->email->to('hr@digicononline.com');
				}
				$this->email->subject(join(' ',['REQUEST: Password Reset -',$employee->firstname,$employee->lastname]));
				$this->email->message($mail_content);

				if($this->email->send()){
					$this->session->set_flashdata('success','Password reset request sent on the registered email');
				}else{
					$this->session->set_flashdata('error',$this->email->print_debugger());
				}
			}
		}
	}


	public function reset($employeeid=0,$token=0){
		$pw_reset = $this->auth_model->get_password_reset_details($employeeid,$token);

		if(isset($pw_reset->status)){
			$data['status'] = $pw_reset->status;

			if($pw_reset->status == 0){
				$str_options = '3xy4P^Cu';
				$new_pw = str_shuffle($str_options);
				
				if($this->auth_model->update_password($employeeid,$new_pw,0)){
					$this->auth_model->set_password_reset_to_complete($pw_reset->id);

					$data['username'] = $employeeid;
					$data['password'] = $new_pw;
					$data['msg'] = "
							<div class='msg-box msg-box-success border-left border-10 border-success'>
								<p class='m-0'>Password successfully changed</p>
							</div>
					";

					// logout if currently logged in
					if($this->is_logged_in()){
						session_destroy();
					}
				}
			}else{
				$data['msg'] = "
						<div class='msg-box msg-box-info border-left border-10 border-info'>
							<p class='m-0'>This request has been already verified</p>
						</div>
				";
			}
		}else{
			$data['msg'] = "
					<div class='msg-box msg-box-danger border-left border-10 border-danger'>
						<p class='m-0'>The request you're trying to open does not exist</p>
					</div>
			";
		}

		$data['header'] = 'ESS | Reset Password';
		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Auth/reset_password',$data,'auth');
	}

}
