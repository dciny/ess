<?php

  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model{

	public function get_details($employeeid){
		$this->db->where('username',$employeeid);
		$query = $this->db->get('user');

		return $query->row();
	}

}

