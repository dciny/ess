<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<?php

//session_start()
$empID=$_SESSION['logged_in']['employeeid'];
//echo $_SESSION['logged_in']['employeeid']; echo "string";
?>
<!-- <iframe src="http://122.53.217.171/test/index.php?empVal=<?=$empID?>" name="iframe_a" title="Iframe Example" width="100%" height="100%"></iframe> -->
<!-- <iframe src="index.php?empVal=<?=$empID?>" name="iframe_a" title="Iframe Example" width="100%" height="100%"></iframe> -->

<!-- <table>
	<tr>
		<th>ID</th>
		<th>date</th>
		<th>comment</th>
	</tr>
	
	<tr>
		
	</tr>


</table> -->
<form action="" method="post">
<!--  <label for="employee">Employee ID:</label>
 <input type="text" name="employee" id="employee"> -->
 <label for="year">Year:</label>
<select id="year" name="year">
  <option value="2020">2020</option>
</select>
<label for="Month">Choose a Month:</label>
<select id="Month" name="Month">
  <option value="1">January</option>
  <option value="2">February</option>
  <option value="3">March</option>
  <option value="4">April</option>
  <option value="5">May</option>
  <option value="6">June</option>
  <option value="7">July</option>
  <option value="8">August</option>
  <option value="9">September</option>
  <option value="10">October</option>
  <option value="11">November</option>
  <option value="12">December</option>
</select>

<label for="Pay">Pay Period:</label>
<select id="Pay" name="Pay">
  <option value="1:16:31">10</option>
  <option value="0:1:15">25</option>
</select>
<input type="submit" name="submit">
</form>


<?php
if(isset($_POST['submit'])){
$year=$_POST["year"];
/*$empID=$_POST["employee"];*/
$data = $_POST["Pay"];
list($dateMinus, $start, $end) = explode(":", $data);
//echo $dateMinus."<br>".$start."<br>".$end."<br>------"; 

$dataDate=$_POST["Month"];
$update= $dataDate - $dateMinus;
if ($update==0) {
	$update='12';
}
 $dateStart = $update."-".$start;
echo "<br>";
 $dateEnd = $update."-".$end;
//echo "2020-".$update."-".$start;
echo "<br>";
//echo "2020-".$update."-".$end;

$searchStart = "2020-".$update."-".$start;
$searchEnd = "2020-".$update."-".$end;



$servername = "localhost";
$username = "WebDevTeam";
$password = "L34dG3n789";
$dbname = "internal";
//
//
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

//$sql = "SELECT * FROM `timelog` WHERE `userid` = '$empID' AND `dates` BETWEEN '$searchStart' AND '$searchEnd'";
$sql = "SELECT `shiftdate`, `userid`,SUM(`regular_hours`),SUM(`nightdiff_hours`),SUM(`regular_ot_hours`),SUM(`late`),SUM(`undertime`), `status` FROM `finalhourstable` WHERE `userid`='$empID' AND `shiftdate` BETWEEN '$searchStart' AND '$searchEnd' GROUP BY `shiftdate`";
$result = $conn->query($sql);
?>

<div class="container">
  <h2>Approve Schedule</h2>
<div class="row">
<?php

if ($result->num_rows > 0) {
  // output data of each row
	echo "<div class='col-sm-12'>";
	echo "<table class='table table-bordered'>";
	echo "<tr>";
    echo "<th>Shift Day</th>";
    echo "<th>Status</th>"; 
    echo "<th>Regular Hours</th>";
    echo "<th>Nightdiff Hours</th>";
    echo "<th>Regular OT Hours</th>";
    echo "<th>Late</th>";
    echo "<th>Undertime</th>";
    echo "<th>Shiftday</th>";
    echo "<th>Start Shift</th>";
    echo "<th>End Shift</th>";
   	echo "<th>OT Start</th>";
   	echo "<th>OT End</th>";
    echo "</tr>";
  while($row = $result->fetch_assoc()) {
  	echo "<tr>";
  	if ($row['status'] == 'Restday') {
  		$classStatus = 'warning';
  	}else if ($row['status'] == 'Present') {
  		$classStatus = 'success';
  	}else if ($row['status'] == 'Absent') {
  		$classStatus = 'danger';
  	}
  	?><td class="<?=$classStatus?>"><?=$row["shiftdate"]?></td><?php
    ?><td class="<?=$classStatus?>"><?=$row["status"]?></td><?php
    ?><td class="<?=$classStatus?>"><?=$row["SUM(`regular_hours`)"]?></td><?php
    ?><td class="<?=$classStatus?>"><?=$row["SUM(`nightdiff_hours`)"]?></td><?php
    ?><td class="<?=$classStatus?>"><?=$row["SUM(`regular_ot_hours`)"]?></td><?php
    ?><td class="<?=$classStatus?>"><?=$row["SUM(`late`)"]?></td><?php
    ?><td class="<?=$classStatus?>"><?=$row["SUM(`undertime`)"]?></td><?php
    $shiftequal = $row["shiftdate"];
     $sql2 = "SELECT `dates`, `shiftday`, `userid`, `startshift`, `endshift`, `otstart`, `otend` FROM `timelog` WHERE `userid` = '$empID' AND `dates` = '$shiftequal'";
    $result2 = $conn->query($sql2);
    if ($result2->num_rows > 0) {
     $row2 = $result2->fetch_assoc();
    ?><td class="<?=$classStatus?>"><?=$row2["shiftday"]?></td><?php
    ?><td class="<?=$classStatus?>"><?=$row2["startshift"]?></td><?php
    ?><td class="<?=$classStatus?>"><?=$row2["endshift"]?></td><?php
    ?><td class="<?=$classStatus?>"><?=$row2["otstart"]?></td><?php
    ?><td class="<?=$classStatus?>"><?=$row2["otend"]?></td><?php
    }
    echo "</tr>";
    //echo "id: " . $row["userid"]. " - status: ".$row["status"]."- startShift " . $row["startshift"]."- startday " . $row["shiftday"]. "<br>";
  }
  echo "</table>";
  echo "</div>";
/*$sql2 = "SELECT `dates`, `shiftday`, `userid`, `startshift`, `endshift`, `otstart`, `otend` FROM `timelog` WHERE `userid` = '$empID' AND `dates` = '$row["shiftdate"]'";*/
/*$result2 = $conn->query($sql2);
	echo "<div class='col-sm-6'>";
if ($result2->num_rows > 0) {
	echo "<table class='table table-bordered'>";
	echo "<tr>";*/
	/*echo "<th>Shiftday</th>";
    echo "<th>Start Shift</th>";
    echo "<th>End Shift</th>";
   	echo "<th>OT Start</th>";
   	echo "<th>OT End</th>";*/
/*   	echo "</tr>";
	$row2 = $result2->fetch_assoc();
	echo "<tr>";*/
  	/*echo "<td>".$row2["shiftday"]."</td>";
    echo "<td>".$row2["startshift"]."</td>";
    echo "<td>".$row2["endshift"]."</td>";
    echo "<td>".$row2["otstart"]."</td>";
    echo "<td>".$row2["otend"]."</td>";*/
  /*  echo "</tr>";
	
	echo "</table>";
	echo "</div>";*/

	/*echo "</div>";*/
  
} else {
  echo "Schedule not Posted..";
}
$conn->close();

}
?>



 
</div>
</div>
</body>
</html>

