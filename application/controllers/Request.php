<?php
  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Request extends CI_Controller {

	public function __construct(){

		parent::__construct();
		
		$this->load->model('employee_model');
		$this->load->helper('url');
	}

	public function _remap($method, $params = []){
		$login_session = $this->session->userdata('logged_in');

		if(isset($login_session)){

			$is_password_change = $this->session->userdata['logged_in']['is_password_change'];
			if($is_password_change == 0){
				redirect('auth/change-password', 'location');
			}
			
			$this->$method($params);
		}else{
			redirect('auth/index', 'location');
		}
	}


	public function basic_info(){
		$employeeid = $this->session->userdata['logged_in']['employeeid'];
		$data['employee'] = $this->employee_model->get_employee_details($employeeid);
		$data['users'] = $this->employee_model->get_user_id($employeeid);
		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Request/basic_info/new',$data,'app');
	}

	public function contact(){
		$employeeid = $this->session->userdata['logged_in']['employeeid'];
		$data['employee'] = $this->employee_model->get_employee_details($employeeid);
		$data['users'] = $this->employee_model->get_user_id($employeeid);
		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Request/contact/new',$data,'app');
	}

	public function address(){
		$employeeid = $this->session->userdata['logged_in']['employeeid'];
		$data['employee'] = $this->employee_model->get_employee_details($employeeid);
		$data['users'] = $this->employee_model->get_user_id($employeeid);
		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Request/address/new',$data,'app');
	}

	public function work(){
		$employeeid = $this->session->userdata['logged_in']['employeeid'];
		$data['employee'] = $this->employee_model->get_employee_details($employeeid);
		$data['users'] = $this->employee_model->get_user_id($employeeid);
		$data['groupscheds'] = $this->employee_model->get_group_schedule();
		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Request/work/new',$data,'app');
	}


}
