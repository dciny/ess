<?php
  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test_model extends CI_Controller {

	public function __construct(){

		parent::__construct();
		
		$this->load->model('employee_model');
		$this->load->model('leave_request_model','leave_request');
		$this->load->model('leave_status_model','leave_status');
		$this->load->model('leave_assignment_model','leave_assignment');
		$this->load->model('approver_hierarchy_model','approver_hierarchy');
		$this->load->model('leave_emaillogs_model','leave_emaillogs');
		$this->load->model('employee_master_model','employee_master');
		$this->load->model('finalhours_model','finalhours');
		$this->load->model('holiday_model','holiday');
		$this->load->model('leave_role_model','leave_role');


		$this->load->helper('url');
	}

	
	public function index(){
		$email_log_params = $this->approver_hierarchy->get_next_approver(1, 1);

		echo "leave_request_id: " . $email_log_params->id . "<br>";
		echo "requestor_id: " . $email_log_params->employeeid . "<br>";
		echo "requestor_name: " . implode(' ',[$email_log_params->firstname,$email_log_params->lastname]) . "<br>";
		echo "to_approver: " . $email_log_params->approver_email . "<br>";
		echo "leave_dt: " . $email_log_params->leavedt . "<br>";
		echo "leave_category: " . $email_log_params->leave_category . "<br>";
		echo "leave_type: " . $email_log_params->leave_name . "<br>";

		$this->leave_emaillogs->create($email_log_params);
	}

	public function test(){
		$year = $this->finalhours->get_year();
		
		print_r($year);
	}


	
	public function backend_apply($employeeid, $leave_type_id, $leave_category, $leave_date){
		$data = array(
			'leave_type_id' => $leave_type_id,
			'employeeid' => $employeeid,
			'leavedt' => $leave_date,
			'leave_category' => $leave_category,
			'createdttm' => date('Y-m-d'),
			'updatedttm' => date('Y-m-d')
		);

		$this->db->insert('leave_requests',$data);
		$leave_request_id = $this->db->insert_id();

		$duration = 0;
		if($this->db->affected_rows() > 0){
			if($leave_category == 'paid'){
				$duration++;
			}

			$mail_details = $this->get_maillog_leave_details($leave_request_id);
			$this->leave_emaillogs->create($mail_details);
		}

		$leave_details = $this->leave_assignment->get_leave_details($employeeid);
		$leave_credits = $leave_details->leave_credits - $duration;
		$this->leave_assignment->update_duration($employeeid,$leave_credits);
	}

	private function get_maillog_leave_details($leave_request_id){
		$leave_details = $this->leave_request->maillog_details($leave_request_id);
		$to_approver = $this->get_first_approvers($leave_request_id);

		$data = (object)[
			'id' => $leave_details->id,
			'employeeid' => $leave_details->employeeid,
			'firstname' => $leave_details->firstname,
			'lastname' => $leave_details->lastname,
			'approver_email' => $to_approver,
			'leavedt' => $leave_details->leavedt,
			'leave_category' => $leave_details->leave_category,
			'leave_name' => $leave_details->leave_name
		];

		return $data;
	}

	private function get_first_approvers($leave_request_id){
		$approvers = $this->approver_hierarchy->get_first_approvers($leave_request_id);
		$approvers_arr = [];
		foreach ($approvers as $approver) {
			array_push($approvers_arr, $approver->email1);
		}
		return implode(',', $approvers_arr);
	}


	public function holidays(){
		$holidays = $this->holiday->get_holidays();

		print_r($holidays);
	}

	public function leave_roles(){
		$holidays = $this->leave_role->all();

		print_r($holidays);
	}

	public function test_hiredate($employeeid){
		$feb = date('Y-02-01');
		$this->db->from('leave_requests');
		$this->db->where('employeeid',$employeeid);
		$this->db->where('leavedt >=', $feb);
		$this->db->where('leave_category', 'paid');
		$this->db->where_in('overall_status',[3,5]);
		$ctr = $this->db->count_all_results();

		echo $ctr;
		
	}


	public function test_get_employees(){
		$employees = $this->employee_model->get_employees('cabaluna');

		print_r($employees);
	}
}
