<?php
  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Leave extends CI_Controller {
	public function __construct(){

		parent::__construct();
		
		$this->load->model('employee_model');
		$this->load->model('leave_type_model','leave_type');
		$this->load->model('leave_assignment_model','leave_assignment');
		$this->load->model('leave_request_model','leave_request');
		$this->load->model('approver_hierarchy_model','approver_hierarchy');
		$this->load->model('leave_status_model','leave_status');
		$this->load->model('leave_emaillogs_model','leave_emaillogs');
		$this->load->model('holiday_model','holiday');
		$this->load->model('user_model','user');
		$this->load->model('leave_approver_model','leave_approver');
		$this->load->helper('url');
		// load pagination library
		$this->load->library('pagination');
	}

	
	public function _remap($method, $params = []){
		$login_session = $this->session->userdata('logged_in');

		if(isset($login_session)){
			
			$is_password_change =$this->session->userdata['logged_in']['is_password_change'];
			if($is_password_change == 0){
				redirect('auth/change-password', 'location');
			}

			$this->$method($params);
		}else{
			redirect('auth/index', 'location');
		}
	}



	public function index(){
		$employeeid = $this->session->userdata['logged_in']['employeeid'];
		$data['user_details'] = $this->user->get_details($employeeid);
		$data['myleaves'] = $this->leave_request->myleaves($employeeid);
		$data['is_approver'] = $this->leave_approver->is_leave_approver($employeeid);
		$leave_balance = $this->leave_assignment->get_leave_details($employeeid);
		// $data['leave_balance'] = isset($leave_balance) && !empty($leave_balance) ? $leave_balance->leave_credits : 0;
		// $data['leave_used'] = $this->leave_request->total_leaves($employeeid);
		$data['leave_balance'] = $this->leave_request->get_calculated_leave_balance($employeeid);
		$data['leave_used'] = $this->leave_request->get_total_approved_leaves($employeeid);

		$data['leave_total'] = $data['leave_balance'] + $data['leave_used'];

		$data['approvers'] = $this->approver_hierarchy->get_approvers($employeeid);
		$data['is_assigned'] = $this->leave_assignment->is_assigned($employeeid);

		///////////////////////////////////////////////////////////////////////////////	
		$data['employee'] = $this->employee_model->get_employee_details($employeeid);
		$data['users'] = $this->employee_model->get_user_id($employeeid);
		$data['header'] = 'MY LEAVES';
		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Leave/index',$data,'app');
	}


	public function apply(){
		$date = "";
		$employeeid = $this->session->userdata['logged_in']['employeeid'];
		$data = $this->generate_calendar($date);
		$data['user_details'] = $this->user->get_details($employeeid);
		$data['seldt'] = [];
		$data['leaves'] = $this->leave_request->leavedates_arr($employeeid);
		$data['leave_types'] = $this->leave_type->all();

		// /////////////////////////////////////////////////////////////////////////////////
		$data['leave_assignment'] = $this->leave_assignment->get_leave_details($employeeid);
		$data['is_approver'] = $this->leave_approver->is_leave_approver($employeeid);
		$data['employee'] = $this->employee_model->get_employee_details($employeeid);
		$data['users'] = $this->employee_model->get_user_id($employeeid);
		$data['header'] = 'LEAVE CALENDAR';
		$data['css'] = 'calendar.css';
		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Leave/apply',$data,'app');
	}


	public function delegate(){
		$searchtxt = $this->input->get('stxt');
		$data['stxt'] = $searchtxt;

		// pagination configuration
		$config['base_url'] = base_url('leave/delegate');
		$config['per_page'] = 10;
		$config['num_links'] = 10;
		$config['reuse_query_string'] = TRUE;
		$config['page_query_string'] = TRUE;
		$config['total_rows'] = count($this->employee_model->get_employees($searchtxt));
		$quantity = $this->input->get('per_page');

		$this->pagination->initialize($config);

		$data['employees'] = $this->employee_model->get_employees($searchtxt, $config['per_page'], $quantity);
		$data['leave_types'] = $this->leave_type->all();
		$employeeid = $this->session->userdata['logged_in']['employeeid'];
		$data['user_details'] = $this->user->get_details($employeeid);
		$data['is_approver'] = $this->leave_approver->is_leave_approver($employeeid);
		$data['employee'] = $this->employee_model->get_employee_details($employeeid);
		$data['users'] = $this->employee_model->get_user_id($employeeid);
		$data['header'] = 'LEAVE DELEGATE';
		$data['css'] = 'calendar.css';
		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Leave/delegate',$data,'app');
	}


	public function employee_calendar($employeeid){
		$id = $employeeid[0];

		$date = "";
		$employeeid = $this->session->userdata['logged_in']['employeeid'];
		
		$data = $this->generate_calendar($date);
		$data['id'] = $id;
		$data['delegatee'] = $this->employee_model->get_employee_details($id);
		$data['user_details'] = $this->user->get_details($id);
		$data['seldt'] = [];
		$data['leaves'] = $this->leave_request->leavedates_arr($id);
		$data['leave_types'] = $this->leave_type->all();

		// /////////////////////////////////////////////////////////////////////////////////
		$data['leave_assignment'] = $this->leave_assignment->get_leave_details($id);
		$data['is_approver'] = $this->leave_approver->is_leave_approver($employeeid);
		$data['employee'] = $this->employee_model->get_employee_details($employeeid);
		$data['users'] = $this->employee_model->get_user_id($employeeid);
		$data['header'] = 'DELEGATE LEAVE';
		$data['css'] = 'calendar.css';
		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Leave/apply_delegate',$data,'app');
	}


	public function tracker(){
		$employeeid = $this->session->userdata['logged_in']['employeeid'];
		$user_details = $this->user->get_details($employeeid);
		$data['user_details'] = $this->user->get_details($employeeid);
		$data['is_approver'] = $this->leave_approver->is_leave_approver($employeeid);
		$data['employee'] = $this->employee_model->get_employee_details($employeeid);
		$data['users'] = $this->employee_model->get_user_id($employeeid);

		if($user_details->leave_role_id == 1){ // if role is HR
			$employees = $this->leave_request->summary();
			$data['employees'] = [];
			foreach ($employees as $employee) {
				$pending_leaves = $this->leave_request->paid_leave_count($employee->employeeid,3);
				$approved_leaves = $this->leave_request->paid_leave_count($employee->employeeid,5);
				// $pending_credits_to_add = $pending_leaves * $employee->leave_accrual;
				// $approved_credits_to_add = $approved_leaves * $employee->leave_accrual;
				array_push($data['employees'], (object) [
					'employeeid' => $employee->employeeid,
					'firstname' => $employee->firstname,
					'lastname' => $employee->lastname,
					'leave_accrual' => $employee->leave_accrual,
					'leave_balance' => $employee->leave_credits + $pending_leaves,
					'leave_total' => $employee->leave_credits + $pending_leaves + $approved_leaves,
					'vl' => $employee->vl,
					'sl' => $employee->sl,
					'bvmt' => $employee->bvmt,
					'lpat' => $employee->lpat,
					'lmat' => $employee->lmat,
					'pto' => $employee->pto,
					'el' => $employee->el,
					'unpaid' => $employee->unpaid
				]);
			}
			$this->load->library('layouts'); // load layout library
			$this->layouts->view('Leave/tracker',$data,'app');
		}else{
			redirect('leave', 'location');
		}
		
	}

	public function monthly_report_approved(){
		$months = [
			'January' => '01',
			'February' => '02',
			'March' => '03',
			'April' => '04',
			'May' => '05',
			'June' => '06',
			'July' => '07',
			'August' => '08',
			'September' => '09',
			'October' => '10',
			'November' => '11',
			'December' => '12'
		];

		// IDs are from leave_hierarchies table
		$departments = [
			'ops' => [1,2,3,4,5,6,7,8,9],
			'it' => [10],
			'qa' => [11,12],
			'hr' => [13],
			'facilities' => [14,15]
		];

		foreach ($months as $mkey => $month) {
			$counts = [];
			foreach ($departments as $dept) {
				$date_from = date("Y-$month-01");
				array_push($counts, $this->leave_request->get_monthly_leave_count_approved($dept,$date_from));
			}
			
			// replace the value of each month
			$months[$mkey] = $counts;
		}

		$data['months'] = $months;

		///////////////////////////////////////////////////////////////////////////////	
		$employeeid = $this->session->userdata['logged_in']['employeeid'];
		$data['employee'] = $this->employee_model->get_employee_details($employeeid);
		$data['users'] = $this->employee_model->get_user_id($employeeid);
		$data['header'] = 'MONTHLY REPORT';
		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Leave/monthly_approved',$data,'app');
	}


	public function monthly_report_pending(){
		$months = [
			'January' => '01',
			'February' => '02',
			'March' => '03',
			'April' => '04',
			'May' => '05',
			'June' => '06',
			'July' => '07',
			'August' => '08',
			'September' => '09',
			'October' => '10',
			'November' => '11',
			'December' => '12'
		];

		// IDs are from leave_hierarchies table
		$departments = [
			'ops' => [1,2,3,4,5,6,7,8,9],
			'it' => [10],
			'qa' => [11,12],
			'hr' => [13],
			'facilities' => [14,15]
		];

		foreach ($months as $mkey => $month) {
			$counts = [];
			foreach ($departments as $dept) {
				$date_from = date("Y-$month-01");
				array_push($counts, $this->leave_request->get_monthly_leave_count_pending($dept,$date_from));
			}
			
			// replace the value of each month
			$months[$mkey] = $counts;
		}

		$data['months'] = $months;

		///////////////////////////////////////////////////////////////////////////////	
		$employeeid = $this->session->userdata['logged_in']['employeeid'];
		$data['employee'] = $this->employee_model->get_employee_details($employeeid);
		$data['users'] = $this->employee_model->get_user_id($employeeid);
		$data['header'] = 'MONTHLY REPORT';
		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Leave/monthly_pending',$data,'app');
	}



	public function show($id){
		$employeeid = $this->session->userdata['logged_in']['employeeid'];
		$lrid = $id[0];
		$data['leave'] = $this->leave_request->details($lrid);

		$data['rank_status'] = [];
		$lr_status = $this->leave_request->get_leave_status($lrid);
		foreach ($lr_status as $status) {
			$approvers_arr = [];
			$approvers = $this->leave_request->get_rank_approvers($lrid, $status->approver_rank);
			foreach ($approvers as $approver) {
				$name = $approver->firstname . ' ' . $approver->lastname;
				array_push($approvers_arr, $name);
			}
			$lr_approvers = implode(', ', $approvers_arr);

			$rank_status = (object) [
				'approvers' => $lr_approvers,
				'status' => $status->status_name
			];
			array_push($data['rank_status'], $rank_status);
		}

		///////////////////////////////////////////////////////////////////////////////	
		$data['employee'] = $this->employee_model->get_employee_details($employeeid);
		$data['users'] = $this->employee_model->get_user_id($employeeid);
		$data['header'] = 'MY LEAVES';
		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Leave/show',$data,'app');
	}


	public function test(){
		$this->load->library('pagination');
		
		$config['base_url'] = base_url('leave/test');
		$config['per_page'] = 10;
		$config['num_links'] = 5;
		$config['total_rows'] = $this->db->get('leave_email_logs')->num_rows();

		$this->pagination->initialize($config);

		$data['query'] = $this->db->get('leave_email_logs', $config['per_page'], $this->uri->segment(3))->result();

		// $this->load->view('leave/test', $data);
		echo $this->pagination->create_links();
	}


	public function test_input(){
		$stxt = $this->input->get('searchtxt');

		var_dump($stxt);
	}


	public function requests(){
		$employeeid = $this->session->userdata['logged_in']['employeeid'];
		$searchtxt = $this->input->get('searchtxt');
		$data['searchtxt'] = $searchtxt;

		// pagination configuration
		$config['base_url'] = base_url('leave/requests');
		$config['per_page'] = 15;
		$config['num_links'] = 10;
		$config['reuse_query_string'] = true;
		$config['total_rows'] = count($this->leave_request->get_approver_requests_for_approval($employeeid,$searchtxt));
		$quantity = $this->uri->segment(3);

		$this->pagination->initialize($config);
		///////////////////////////////////////////////////////

		$data['user_details'] = $this->user->get_details($employeeid);
		$requests = $this->leave_request->get_approver_requests_for_approval($employeeid, $searchtxt, $config['per_page'], $quantity);
		$data['leave_statuses'] = $this->leave_status->all();
		$data['is_approver'] = $this->leave_approver->is_leave_approver($employeeid);

		$data['requests'] = [];
		foreach ($requests as $request){
			$rank = $this->approver_hierarchy->approver_rank($request->id, $employeeid)->approver_rank;
			$lr_curr_rank = 0;
			$max_status = $this->leave_status->get_max_rank_status($request->id);
			$rank_status = $this->leave_status->get_rank_status($request->id, $rank);
			if($max_status->leave_status_id == 5){ // leave status 5 for approved
				// $lr_curr_rank = $max_status->approver_rank + 1; // rank for next approver
				$lr_curr_rank = $this->leave_status->get_next_rank($request->id, $max_status->approver_rank); // rank for next approver
			}else{
				$lr_curr_rank = $max_status->approver_rank;
			}

			$approvable = ($rank == $lr_curr_rank) ? 1 : 0;
			$leave_balance = $this->leave_request->get_calculated_leave_balance($request->employeeid);
			$leaves_taken = $this->leave_request->get_total_approved_leaves($request->employeeid);

			$r = (object) [
				'employeeid' => $request->employeeid,
				'firstname' => $request->firstname,
				'lastname' => $request->lastname,
				'approver_rank' => $rank,
				'id' => $request->id,
				'leavedt' => $request->leavedt,
				'leave_name' => $request->leave_name,
				'leave_category' => $request->leave_category,
				'leave_reason' => $request->leave_reason,
				'approvable' => $approvable,
				'rank_status' => $rank_status,
				'createdttm' => $request->createdttm,
				'leave_balance' => $leave_balance,
				'leaves_taken' => $leaves_taken
			];

			array_push($data['requests'], $r);
		}

		///////////////////////////////////////////////////////////////////////////////	
		$data['employee'] = $this->employee_model->get_employee_details($employeeid);
		$data['users'] = $this->employee_model->get_user_id($employeeid);
		$data['header'] = 'LEAVE REQUESTS';
		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Leave/requests',$data,'app');
	}


	public function requests_completed(){
		$employeeid = $this->session->userdata['logged_in']['employeeid'];
		$searchtxt = $this->input->get('searchtxt');
		$data['searchtxt'] = $searchtxt;

		// pagination configuration
		$config['base_url'] = base_url('leave/requests_completed');
		$config['per_page'] = 15;
		$config['num_links'] = 10;
		$config['reuse_query_string'] = true;
		$config['total_rows'] = count($this->leave_request->get_approver_requests_completed($employeeid,$searchtxt));
		$quantity = $this->uri->segment(3);

		$this->pagination->initialize($config);
		///////////////////////////////////////////////////////

		$data['user_details'] = $this->user->get_details($employeeid);
		$data['is_approver'] = $this->leave_approver->is_leave_approver($employeeid);
		// $data['requests'] = $this->leave_request->get_approver_requests_completed($employeeid, $searchtxt, $config['per_page'], $quantity);
		$requests = $this->leave_request->get_approver_requests_completed($employeeid, $searchtxt, $config['per_page'], $quantity);
		foreach ($requests as $request) {
			$request->leave_balance = $this->leave_request->get_calculated_leave_balance($request->employeeid);
			$request->leaves_taken = $this->leave_request->get_total_approved_leaves($request->employeeid);
		}
		$data['requests'] = $requests;
		$data['pagination_link'] = $this->pagination->create_links();

		///////////////////////////////////////////////////////////////////////////////	
		$data['employee'] = $this->employee_model->get_employee_details($employeeid);
		$data['users'] = $this->employee_model->get_user_id($employeeid);
		$data['header'] = 'LEAVE REQUESTS';
		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Leave/requests_completed',$data,'app');
	}


	public function generate_calendar($date){
		date_default_timezone_set("Asia/Kuala_Lumpur");

		$data['now'] = date('Y-m-d');
		$date = !empty($date) ? $date : date('Y-m-d');
		$date = date('Y-m-01',strtotime($date)) == date('Y-m-01') ? date('Y-m-d') : $date;
		$holidays = $this->holiday->get_holidays();
		$data['holidays_arr'] = [];
		foreach ($holidays as $holiday){
			array_push($data['holidays_arr'], $holiday->holidaydate);
		}
		

		$data['days'] = ['Su','Mo','Tu','We','Th','Fr','Sa'];

		// current month days: loop through currdt_no_days and add number of days in first_day
		$last_day = date('Y-m-t',strtotime($date));
		$last_day_arr = getdate(strtotime($last_day));
		$curr_first_day = date('Y-m-01',strtotime($date));
		$data['curr_last_day_idx'] = $last_day_arr['wday'];
		$data['currdt_no_days'] = date('t',strtotime($date));
		$data['curr_mo'] = $curr_first_day;
		$data['curr_date'] = date('Y-m-d',strtotime($date));
		
		// previous month days: loop through prevmo_no_days and add number of days in prevmo_strt_day
		$first_day_arr = getdate(strtotime($curr_first_day));
		$data['prevmo_no_days'] = $first_day_arr['wday'];
		$data['prevmo_strt_day'] = date('Y-m-d', strtotime("-{$data['prevmo_no_days']} days", strtotime($curr_first_day)));
		$data['prevmo_first_day'] = date('Y-m-01', strtotime("-1 months", strtotime($curr_first_day)));

		// next month
		$data['nxtmo_first_day'] = date('Y-m-01', strtotime("1 months", strtotime($curr_first_day))); // add 1 month to current month
		$nxtmo = getdate(strtotime($data['nxtmo_first_day']));
		$nxtmo_first_day_idx = $nxtmo['wday'];
		$data['nxtmo_no_days'] = 7 - $nxtmo_first_day_idx; // 7 for 7 days

		return $data;
	}


	public function get_calendar(){
		$date = $this->input->get('date');
		$employeeid = $this->input->get('employeeid');
		$data = $this->generate_calendar($date);

		// $employeeid = $this->session->userdata['logged_in']['employeeid'];
		$data['leaves'] = $this->leave_request->leavedates_arr($employeeid);
		
		$data['seldt'] = !empty($this->input->get('seldt')) ? $this->input->get('seldt') : [];
		$this->load->view('Leave/calendar',$data);
		
	}


	public function create(){
		$leave_type_id = $this->input->post('leave_type_id');
		$leave_category = $this->input->post('leave_category');
		$leave_reason = $this->input->post('leave_reason');
		// $duration = $this->input->post('duration');
		$employeeid = $this->input->post('employeeid');
		$leave_dates = $this->input->post('leave_dates');
		$leave_category = $this->input->post('leave_category');

		date_default_timezone_set('Asia/Kuala_Lumpur');
		$duration = 0;
		foreach ($leave_dates as $leave_date) {
			$data = array(
				'leave_type_id' => $leave_type_id,
				'employeeid' => $employeeid,
				'leavedt' => $leave_date,
				'leave_category' => $leave_category,
				'leave_reason' => $leave_reason,
				'createdttm' => date('Y-m-d H:i:s'),
				'updatedttm' => date('Y-m-d H:i:s')
			);

			$updated_leave_list = $this->leave_request->leavedates_arr($employeeid);
			if(!in_array($leave_date, $updated_leave_list)){
				$this->db->insert('leave_requests',$data);
				$leave_request_id = $this->db->insert_id();
				if($this->db->affected_rows() > 0){
					if($leave_category == 'paid'){
						$duration++;
					}

					$mail_details = $this->get_maillog_leave_details($leave_request_id);
					$this->leave_emaillogs->create($mail_details);
				}
			}
		}

		$leave_details = $this->leave_assignment->get_leave_details($employeeid);
		$leave_credits = $leave_details->leave_credits - $duration;
		$this->leave_assignment->update_duration($employeeid,$leave_credits);

	}


	public function leave_count_page(){
		$employeeid = $this->session->userdata['logged_in']['employeeid'];
		$data['grouped_approvees'] = $this->leave_approver->get_grouped_approvees($employeeid);

		$data['user_details'] = $this->user->get_details($employeeid);
		$data['is_approver'] = $this->leave_approver->is_leave_approver($employeeid);
		$data['employee'] = $this->employee_model->get_employee_details($employeeid);


		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Leave/leave_count_page',$data,'app');
	}

	public function leave_count_page_submit(){
		$from = $this->input->get('from');
		$to = $this->input->get('to');
		$employeeids = $this->input->get('employeeids');

		header('Content-Type: application/json');

		try{
			$leave_count_per_date = $this->leave_approver->leave_count_per_date($from, $to, $employeeids);
			echo json_encode($leave_count_per_date);
		}catch(Exception $e){
			echo 'Error: ' . $e->getMessage();
		}

	}


	private function get_first_approvers($leave_request_id){
		$approvers = $this->approver_hierarchy->get_first_approvers($leave_request_id);
		$approvers_arr = [];
		foreach ($approvers as $approver) {
			array_push($approvers_arr, $approver->email1);
		}
		return implode(',', $approvers_arr);
	}

	private function get_maillog_leave_details($leave_request_id){
		$leave_details = $this->leave_request->maillog_details($leave_request_id);
		$to_approver = $this->get_first_approvers($leave_request_id);

		$data = (object)[
			'id' => $leave_details->id,
			'employeeid' => $leave_details->employeeid,
			'firstname' => $leave_details->firstname,
			'lastname' => $leave_details->lastname,
			'approver_email' => $to_approver,
			'leavedt' => $leave_details->leavedt,
			'leave_category' => $leave_details->leave_category,
			'leave_name' => $leave_details->leave_name
		];

		return $data;
	}


	private function get_overall_status($leave_request_id){
		// $status = 'For Approval';
		$lr_status = $this->leave_request->get_leave_status($leave_request_id);
		// $last_approver_stat = end($lr_status);
		// $status = empty($last_approver_stat->status_name) ? 'For Approval' : $last_approver_stat->status_name;

		$status = '';
		foreach ($lr_status as $s) {
			// $s->status_name
			if(empty($s->status_name)){
				$status = "For Approval"; break;
			}

			if($s->status_name != 'Approved'){
				$status = $s->status_name; break;
			}
		}

		if(empty($status)){
			$status = 'Approved';
		}



		return $status;
	}

}
