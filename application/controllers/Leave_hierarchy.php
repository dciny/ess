<?php
  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Leave_hierarchy extends CI_Controller {

	public function __construct(){

		parent::__construct();
		
		$this->load->model('leave_hierarchy_model','leave_hierarchy');
		$this->load->model('approver_hierarchy_model','approver_hierarchy');
		$this->load->model('approver_model');
		$this->load->library('session');
		$this->load->helper('url');
	}


	public function index(){
		$data['ind'] = 'HR'; // used in the sidebar of the layout
		$data['leave_hierarchies'] = $this->leave_hierarchy->all();
		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Leave_hierarchy/index',$data,'leave');
	}

	
	public function new(){
		$data['ind'] = 'HR'; // used in the sidebar of the layout
		$data['approvers'] = $this->approver_model->approvers();
		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Leave_hierarchy/new',$data,'leave');
	}


	public function create(){
		$this->form_validation->set_error_delimiters('<p class="invalid-feedback">', '</p>');
		$this->form_validation->set_rules('rule_name', 'rule name', 'trim|required|is_unique[leave_hierarchies.rule_name]');

		if ($this->form_validation->run() == FALSE){
			$data['ind'] = 'HR'; // used in the sidebar of the layout
			$data['approvers'] = $this->approver_model->approvers();
			$this->load->library('layouts'); // load layout library
			$this->layouts->view('Leave_hierarchy/new',$data,'leave');
		}else{
			date_default_timezone_set("Asia/Kuala_Lumpur");
			$hierarchy['rule_name'] = $this->input->post('rule_name');
			$hierarchy['rule_desc'] = $this->input->post('rule_desc');
			$hierarchy['createdttm'] = date('Y-m-d H:i:s');
			$hierarchy['updatedttm'] = date('Y-m-d H:i:s');
			$approver_ids = $this->input->post('approver_ids');
			$ranks = $this->input->post('ranks');

			if($id = $this->leave_hierarchy->create($hierarchy)){
				foreach ($approver_ids as $approver_id){
					$app_hierarchy['leave_hierarchy_id'] = $id;
					$app_hierarchy['leave_approver_id'] = $approver_id;
					$app_hierarchy['approver_rank'] = $ranks["$approver_id"];
					$app_hierarchy['is_active'] = 1;
					$app_hierarchy['createdttm'] = date('Y-m-d H:i:s');
					$app_hierarchy['updatedttm'] = date('Y-m-d H:i:s');

					$this->approver_hierarchy->create($app_hierarchy);
				}
			}

			redirect(base_url('leave-hierarchy'),'location');
		}
	}


	public function edit($id){
		$data['lh'] = $this->leave_hierarchy->find($id);
		$data['approvers'] = $this->approver_hierarchy->approvers($data['lh']->id);
		$data['avail_approvers'] = $this->approver_hierarchy->avail_approvers($data['lh']->id);

		$data['ind'] = 'HR'; // used in the sidebar of the layout
		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Leave_hierarchy/edit',$data,'leave');
	}

	public function update(){
		$rule_name = $this->input->post('rule_name');
		$old_rule_name = $this->input->post('old_rule_name');
		$rule_desc = $this->input->post('rule_desc');
		$approver_ids = $this->input->post('approver_ids');
		$ranks = $this->input->post('ranks');
		$lhid = $this->input->post('id');

		$this->form_validation->set_error_delimiters('<p class="invalid-feedback">', '</p>');
		$this->form_validation->set_rules('rule_name', 'rule name', 'trim|required');
		if($old_rule_name != $rule_name){
			$this->form_validation->set_rules('rule_name', 'rule name', 'is_unique[leave_hierarchies.rule_name]');
		}

		if ($this->form_validation->run() == FALSE){
			$data['lh'] = $this->leave_hierarchy->find($lhid);
			$data['approvers'] = $this->approver_hierarchy->approvers($data['lh']->id);
			$data['avail_approvers'] = $this->approver_hierarchy->avail_approvers($data['lh']->id);
			$data['ind'] = 'HR'; // used in the sidebar of the layout
			$this->load->library('layouts'); // load layout library
			$this->layouts->view('Leave_hierarchy/edit',$data,'leave');
		}else{
			date_default_timezone_set("Asia/Kuala_Lumpur");
			$leave_hierarchy['rule_name'] = $rule_name;
			$leave_hierarchy['rule_desc'] = $rule_desc;
			$leave_hierarchy['updatedttm'] = date('Y-m-d H:i:s');
			$id = $this->input->post('id');
			if($this->leave_hierarchy->update($id,$leave_hierarchy)){
				// remove not on the array
				$this->approver_hierarchy->delete($id,$approver_ids);
				// insert or update data
				$this->approver_hierarchy->insert_or_update($id,$approver_ids,$ranks);
			}

			redirect(base_url('leave-hierarchy'),'location');
		}

	}


	public function delete(){
		$id = $this->input->post('id');
		$leave_assignments_count = $this->leave_hierarchy->leave_assignments($id)->count_all_results();
		// redirect with error: cannot delete because leave hierarchy is not empty
		if($leave_assignments_count){
			//add flash data 
         	$this->session->set_flashdata('error','There are employees assigned to this hierarchy');
         	redirect(base_url('leave-hierarchy'),'location'); 
		}

		$success = [];
		$is_approver_deleted = $this->approver_hierarchy->deletePerLeaveHierarchy($id);
		if($is_approver_deleted)
			array_push($success, 'approver hierarchies removed');

		$is_leave_hierarchy_deleted = $this->leave_hierarchy->delete($id);
		if($is_approver_deleted)
			array_push($success, 'leave hierarchy removed');

		//add flash data 
         $this->session->set_flashdata('success',$success);

		redirect(base_url('leave-hierarchy'),'location');

	}

}
