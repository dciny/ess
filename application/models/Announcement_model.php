<?php

  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Announcement_model extends CI_Model{
	public function find_by_id($id){
		$query = $this->db->from('announcements')
		                  ->where('id',$id)
		                  ->get();

		return $query->row();
	}

	public function all(){
		$query = $this->db->order_by('createdttm','DESC')->get('announcements');

		return $query->result();
	}

	public function delete_by_id($id){
		$this->db->delete('announcements', array('id' => $id));
	}
}

