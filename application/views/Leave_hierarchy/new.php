
<h5 class="dark-orange">Add Hierarchy Rules</h5>
<hr>

<form action="<?php echo base_url('leave-hierarchy/create') ?>" method="post">
	<div class="hierarchy-grid">
		<div class="border w-min360px">
			<div class="border-bottom px-4 py-2 bg-warning-gradient text-light">
				<strong>Hierarchy Rule Form</strong>
			</div>
			
			<div class="fontsize-14 px-4 py-2 intro-box">
				<div class="form-group">
					<label>Rule Name</label>
					<input type="text" name="rule_name" class="form-control form-control-sm <?php if(form_error('rule_name')) echo 'is-invalid' ?>" value="<?php echo set_value('rule_name') ?>">
					<?php echo form_error('rule_name'); ?>
				</div>
				<div class="form-group">
					<label>Short Description</label>
					<input type="text" name="rule_desc" class="form-control form-control-sm">
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-primary" value="Create">
				</div>
			</div>
		</div>
	</div>

	<hr>

	<h5 class="dark-orange">Define Hierarchy Rules</h5>
	<div class="hierarchy-grid">
		<div class="w-min360px">
			<h6 class="mt-3">Approvers List</h6>
			<div class="input-group mb-1">
				<input type="text" class="form-control form-control-sm" placeholder="Search...">
				<div class="input-group-append">
					<button class="btn btn-secondary btn-sm" type="submit">Submit</button>
				</div>
			</div>
			<div class="list-box border">
				<ul class="list-group list-group-flush" id="japrvr-list">
					<?php foreach ($approvers as $approver) { ?>
						<li class="list-group-item p-0 jparent-cntr" data-id="<?php echo $approver->id ?>">
							<table class="table table-borderless table-sm fontsize-14 mb-0">
								<tbody>
									<tr class="text-center">
										<td class="align-middle fname" style="width: 362px">
											<?php echo implode(' ', [$approver->firstname,$approver->lastname]) ?>
										</td>
										<td><button class="btn btn-sm btn-success jadd-aprvr-hrcy">Add</button></td>
									</tr>	
								</tbody>
							</table>
						</li>
					<?php } ?>
				</ul>
			</div>
		</div>

		<div class="w-min360px fontsize-14">
			<h6 class="mt-3">Approver's Hierarchy</h6>
			<table class="table table-sm table-bordered">
				<thead class="thead-light">
					<tr>
						<th>Approvers</th>
						<th>Rank</th>
					</tr>
				</thead>
				<tbody id="japrvr-hrcy-list">
					
				</tbody>
			</table>
		</div>
	</div>
</form>

