<div class="border">
	<div class="announce-head pl-3 pt-3">
		<h1 class="text-dark"><strong class="text-dark"><?php echo ucwords($announcement->title) ?></strong></h1>
		<em class="text-muted"><?php echo date('F m, d h:i A',strtotime($announcement->createdttm)) ?></em>
	</div>
	<div class="announce-body pl-3 pb-3 pr-3">
		<div class="text-dark mt-3">
			<?php echo $announcement->body ?>
		</div>
	</div>
</div>
<?php if($user->leave_role_id == 1){ ?>
	<div class="d-flex mt-3" style="gap: 1rem">
		<div>
			<a href="<?php echo base_url('announcement/'.$announcement->id.'/edit') ?>" class="btn btn-secondary">
				<small>
					<span class="fa fa-edit"></span>
					<span>Edit</span>
				</small>
			</a>
		</div>
		<div>
			<a href="<?php echo base_url('announcement/'.$announcement->id.'/destroy') ?>" class="btn btn-danger del-announcement" data-redirect="<?php echo base_url('announcement/all/') ?>">
				<small>
					<span class="fa fa-remove"></span>
					<span>Delete</span>
				</small>
			</a>
		</div>
	</div>
<?php } ?>