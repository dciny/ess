$(document).ready(function(){
	// initialize wysiwyg textarea
	// $('textarea.wysiwyg').jqte();
	if(document.querySelector( '.ckeditor5' ) != null){
		ClassicEditor
	                .create( document.querySelector( '.ckeditor5' ), {
	                    
	                toolbar: {
	                    items: [
	                        'heading',
	                        '|',
	                        'bold',
	                        'italic',
	                        'link',
	                        'bulletedList',
	                        'numberedList',
	                        '|',
	                        'outdent',
	                        'indent',
	                        '|',
	                        'blockQuote',
	                        'insertTable',
	                        'mediaEmbed',
	                        'undo',
	                        'redo',
	                        // 'imageUpload',
	                        'superscript',
	                        'subscript',
	                        'specialCharacters',
	                        'underline',
	                        'strikethrough'
	                    ]
	                },
	                language: 'en',
	                image: {
	                    toolbar: [
	                        'imageTextAlternative',
	                        'imageStyle:inline',
	                        'imageStyle:block',
	                        'imageStyle:side',
	                        'linkImage'
	                    ]
	                },
	                table: {
	                    contentToolbar: [
	                        'tableColumn',
	                        'tableRow',
	                        'mergeTableCells',
	                        'tableCellProperties',
	                        'tableProperties'
	                    ]
	                },
	                    licenseKey: '',
	                    
	                    
	                    
	                } )
	                .then( editor => {
	                    window.editor = editor;
	            
	                    
	                    
	                    
	                } )
	                .catch( error => {
	                    console.error( 'Oops, something went wrong!' );
	                    console.error( 'Please, report the following error on https://github.com/ckeditor/ckeditor5/issues with the build id and the error stack trace:' );
	                    console.warn( 'Build id: k6hflsncjxcd-78gkdaas4mpu' );
	                    console.error( error );
	                } );
	}            




	// initialize bootstrap 4 tooltip
	$('[data-toggle="tooltip"]').tooltip();


	$(document).on("click",".close-parent",function(){
		var parent = $(this).parent();

		parent.remove();
	});

	$(document).on("submit",".jp-reset",function(e){
		e.preventDefault();

		var url = $(this).attr('action');
		var method = $(this).attr('method');
		var username = $(this).find('[name]').val();


		$.ajax({
			url: url,
			method: method,
			data:{
				username: username
			},
			beforeSend: function(){
				$('.spinner-wrapper').show();
			},
			success: function(response){
				window.location = '/auth/forgot-password';
			}
		});

	});


	// Calendar related scripts
	$(document).on('click','.prev,.next',function(){
		var date = $(this).data('date');
		// var url = '/leave/';
		var cal_space = $('.cal-space');
		var employeeid = $('input[name=employeeid]').val();
		var seldt = [];
		$('input[name="leave_dates[]"]').each(function(){
			seldt.push($(this).val());
		});

		// location.href = url + date;
		$.ajax({
			url: '/leave/get_calendar',
			method: 'get',
			data: {
				date: date,
				seldt: seldt,
				employeeid: employeeid
			},
			success: function(response){
				cal_space.html(response).trigger('change-date');
			}
		});

	});

	$(document).on('click','.yrprev,.yrnxt',function(){
		var yr_to_add = $(this).data('val');
		var yr = $('h5#year');
		var yr_val = Number(yr.text().trim());

		yr_val = yr_val + yr_to_add;

		yr.text(yr_val);
	});

	$(document).on('click','.yrsprev,.yrsnxt',function(){
		var yr = $('h5#years');
		var grid_yrs = $('.grid-months');
		var yr_to_add = Number($(this).data('val'));
		var base_yr = Number(yr.attr('data-byr')) + yr_to_add;
		var upper_yr = Number(yr.attr('data-uyr')) + yr_to_add;
		var txt = base_yr + ' - ' + upper_yr;
		var years = get_years(base_yr);

		yr.attr('data-byr',base_yr);
		yr.attr('data-uyr',upper_yr);
		yr.text(txt);

		grid_yrs.html(years);
	});	

	$(document).on('click','#month',function(){
		var cal_space = $(this).parents('.cal-space');
		var dt = $('#date').data('date');
		var months_template = generate_months_template(dt);

		cal_space.fadeOut(200,function(){
			cal_space.html(months_template)
			         .fadeIn(200);
		});
	});

	$(document).on('click','#date',function(){
		var cal_space = $(this).parents('.cal-space');
		var date = $(this).data('date');
		var dt = new Date(date);
		var yr = dt.getFullYear();

		var base_yr = Number(yr);
		var years_template = generate_years_template(base_yr);

		cal_space.fadeOut(200,function(){
			cal_space.html(years_template)
			         .fadeIn(200);
		});
	});

	$(document).on('click','#year',function(){
		var cal_space = $(this).parents('.cal-space');
		var base_yr = Number($(this).text().trim());
		var years_template = generate_years_template(base_yr);

		cal_space.fadeOut(200,function(){
			cal_space.html(years_template)
			         .fadeIn(200);
		});
	});

	$(document).on('click','.yr_opt',function(){
		var yr = $(this).data('yr').toString();
		var cal_space = $(this).parents('.cal-space');
		var months_template = generate_months_template(yr);

		cal_space.fadeOut(200,function(){
			cal_space.html(months_template)
			         .fadeIn(200);
		});
	});

	$(document).on('click','.cal-month[data-month]',function(){
		var cal_space = $('.cal-space');
		var month = $(this).data('month');
		var year = $('h5#year').text().trim();
		var dt = year + '-' + month + '-01';
		var employeeid = $('input[name=employeeid]').val();

		// location.href = '/leave/' + dt;
		var seldt = [];
		$('input[name="leave_dates[]"]').each(function(){
			seldt.push($(this).val());
		});
		// console.log(seldt);

		$.ajax({
			url: '/leave/get_calendar',
			method: 'get',
			data: {
				date: dt,
				seldt: seldt,
				employeeid: employeeid
			},
			success: function(response){
				cal_space.html(response).trigger('change-date');
			}
		});

	});


	$(document).on('click','.cal-clickable:not(.dt-selected)',function(){
		var dt = $(this).data('val');
		var date_wrapper = $('#sel-dates');
		var date_template = generate_date_leave(dt);
		var duration = $('input[name=duration]');
		var cat_val = $('#leave_category').val();

		
		$(this).removeClass('current').addClass('dt-selected');

		date_wrapper.append(date_template);
		sort_selected_leaves();

		var num_el = $('#sel-dates').children().length;
		if(cat_val == 'paid'){
			// duration.val(Number(duration.val()) - 1);
			duration.val(Number(duration.data('val')) - num_el);
		}else{
			duration.val(Number(duration.data('val')));
		}
	});


	$(document).on('click','.dt-selected',function(){
		var date_wrapper = $('#sel-dates');
		var curdt = new Date($('#date').data('date'));
		var seldt = $(this).data('val');
		seldt_obj = new Date(seldt);
		var duration = $('input[name=duration]');
		var cat_val = $('#leave_category').val();

		$(this).removeClass('dt-selected');
		if(curdt.getTime() === seldt_obj.getTime()){
			$(this).addClass('current');
		}

		var selector = "[value='{date}']".replace('{date}',seldt);
		var dt_entry = date_wrapper.find(selector);
		dt_entry.parent().remove();

		var num_el = $('#sel-dates').children().length;
		if(cat_val == 'paid'){
			// duration.val(Number(duration.val()) - 1);
			duration.val(Number(duration.data('val')) - num_el);
		}else{
			duration.val(Number(duration.data('val')));
		}
		// duration.val(Number(duration.val()) + 1);
	});


	$(document).on('change','#leave_category',function(){
		var cat_val = $(this).val();
		var num_sel_days =  $('#sel-dates').children().length;
		var duration = $('input[name=duration]');

		var updated_duration = 0;
		if(cat_val == 'paid'){
			updated_duration = Number(duration.val()) - num_sel_days;
			duration.val(updated_duration);
		}else{ // unpaid
			updated_duration = Number(duration.val()) + num_sel_days;
			duration.val(updated_duration);
		}

		// console.log('activated')
	});

	
	/*----------------------------------------------------------------------- 
	|                 Calendar rules for the leave types                    |
	-----------------------------------------------------------------------*/

	$(document).ready(function(){
		calendar_rule();
	});


	$(document).on('change-date',function(){
		calendar_rule();
	});


	$(document).on('change','#leave_type',function(){
		var leave_type_val = $('#leave_type').val();

		$('#lt-note').empty();
		if(['2','5','6','7'].includes(leave_type_val)){
			let note = "Note: You can only select dates 7 days ahead from today's date";
			$('#lt-note').empty().append(note);
		}

		// reset the leave credits since the days selected will be removed
		let curr_leave_credits = $('[name=duration]').data('val');
		$('[name=duration]').val(curr_leave_credits);

		calendar_rule();
	});

	// apply rules when users are not able to click parts of the days
	function calendar_rule(){
		var leave_type_val = $('#leave_type').val();

		if(typeof leave_type_val !== 'undefined'){
			// Reset cal-clickable and cal-clickable-disabled
			$('.cal-clickable-disabled').addClass('cal-clickable')
			                            .removeClass('cal-clickable-disabled');
			// Reset date selected
			$('.dt-selected').removeClass('dt-selected');
			$('#sel-dates').empty();

			// if leave type IN VL, PTO, LPAT , LMAT: apply the 1 week ahead rule
			if(['2','5','6','7'].includes(leave_type_val)){
				$('.cal-clickable').each(function(){
					let day_val = $(this).data('val');
					let day_obj = new Date(day_val);
					let today = new Date();
					today.setDate(today.getDate() + 7);

					let tyear = today.getFullYear();
					let tmonth = today.getMonth() + 1;
					let tday = today.getDate();
					let seventh_day = tyear + '-' + tmonth + '-' + tday;
					let seventh_day_obj = new Date(seventh_day);

					if(day_obj.getTime() < seventh_day_obj.getTime()){
						$(this).removeClass('cal-clickable')
						       .addClass('cal-clickable-disabled');
					}
				});
			}else if(leave_type_val =='3'){ // if leave type is sick leave
				$('.cal-clickable').each(function(){
					let day_val = $(this).data('val');
					let day = new Date(day_val);
					let dyear = day.getFullYear();
					let dmonth = day.getMonth() + 1;
					let dday = day.getDate();
					let day_without_time = dyear + '-' + dmonth + '-' + dday;
					let day_without_time_obj = new Date(day_without_time);

					let today = new Date();
					let tyear = today.getFullYear();
					let tmonth = today.getMonth() + 1;
					let tday = today.getDate();
					let today_without_time = tyear + '-' + tmonth + '-' + tday;
					let today_without_time_obj = new Date(today_without_time);

					// if date is greater than current date or if date is saturday or sunday, date should not be clickable
					if(day_without_time_obj.getTime() > today_without_time_obj.getTime() || [0,6].includes(day_without_time_obj.getDay())){
						$(this).removeClass('cal-clickable')
						       .addClass('cal-clickable-disabled');
					}
				})
			}else{
				$('.cal-clickable').each(function(){
					let day_val = $(this).data('val');
					let day_obj = new Date(day_val);
					let today = new Date();
					// today.setDate(today.getDate() - 1);

					let tyear = today.getFullYear();
					let tmonth = today.getMonth() + 1;
					let tday = today.getDate();
					let today_without_time = tyear + '-' + tmonth + '-' + tday;
					let today_without_time_obj = new Date(today_without_time);

					if(day_obj.getTime() < today_without_time_obj.getTime()){
						$(this).removeClass('cal-clickable')
						       .addClass('cal-clickable-disabled');
					}
				});
			}
		}
	}

	//======================================================================= 


	$(document).on('submit','.lreq_form',function(e){
		e.preventDefault();
		$('.modal-hidden').removeClass('modal-hidden');

		var url = $(this).attr('action');
		var method = $(this).attr('method');
		var leave_dates = [];
		$(this).find('[name="leave_dates[]"]').each(function(){
			leave_dates.push($(this).val());    
		});
		var leave_type_id = $(this).find('[name=leave_type_id]').val();
		var leave_category = $(this).find('[name=leave_category]').val();
		var duration = $(this).find('[name=duration]').val();
		var original_duration = $(this).find('[name=duration]').data('val');
		var employeeid = $(this).find('[name=employeeid]').val();
		var leave_reason = $(this).find('[name=leave_reason]').val();
		if(leave_dates.length > 0){
			if(original_duration > 0 && duration >= 0 || leave_category == 'unpaid'){
				$.ajax({
					url: url,
					method: method,
					data:{
						leave_type_id: leave_type_id,
						leave_category: leave_category,
						duration: duration,
						employeeid: employeeid,
						leave_dates: leave_dates,
						leave_reason: leave_reason
					},
					success: function(response){
						location.reload();
					}
				});
			}else{

				var msg = "<div class='alert alert-danger d-block mt-3 alert-dismissible'><span>You don't have enough leave credits left</span><button type='button' class='close' data-dismiss='alert'>&times;</button></div>";
				$('#err-msg').append(msg);
			}
		}else{
			var msg = "<div class='alert alert-danger d-block mt-3 alert-dismissible'><span>You have not selected any dates yet.</span><button type='button' class='close' data-dismiss='alert'>&times;</button></div>";
				$('#err-msg').append(msg);
		}
	});


	$(document).on('click','input[name=selectall]',function(){
		var tbody = $(this).parents('thead').siblings('tbody'); // container having all the checkbox
		var check_val = this.checked; // get the value of the checkbox if it is checked or not
		
		tbody.find('input[type=checkbox]').each(function(){
			this.checked = check_val;
		});
	});


	$(document).on('click','#bulk-req-btn',function(){
		var gen_stat = $(this).siblings('select').val();
		var list = $('#approve-req-tbl').children();

		var req = [];
		list.each(function(){
			var is_selected = $(this).find('input[type=checkbox]').is(":checked");
			if(is_selected){

				req.push({
							leave_request_id: $(this).find('[name=leave_request_id]').val(), 
							approver_rank: $(this).find('[name=approver_rank]').val(),
							employeeid: $(this).find('[name=employeeid]').val(),
							leave_category: $(this).find('[name=leave_category]').val(),
							leave_status_id: gen_stat
						});
			}
			
		});


		$.ajax({
			url: '/leave-request-status/bulk_create',
			method: 'POST',
			data: {
				req: req
			},
			success: function(){
				location.reload();
			}
		});

	});





	////////////////////////////////////////////////
	function generate_date_leave(date){
		var fdate = new Date(date);
		var day = ("0" + fdate.getDate()).slice(-2);
		var month = ("0" + (fdate.getMonth()+1)).slice(-2);
		var year = fdate.getFullYear();
		fdate = month + '/' + day + '/' + year;
		var template = `
			<div class="dt-leave">
				<span>{fdate}</span>
				<input type="hidden" name="leave_dates[]" value="{date}">
			</div>
		`.replace('{date}',date)
		 .replace('{fdate}',fdate);

		return template;
	}

	function sort_selected_leaves(){
		var date_wrapper = $('#sel-dates');
		date_wrapper.children().sort(function(a,b){
			var dt1 = $(a).find('[name]').val();
			var dt2 = $(b).find('[name]').val();
			var dta = new Date(dt1);
			var dtb = new Date(dt2);

			if(dta < dtb)
				return -1;
			else if(dta > dtb)
				return 1;
			else
				return 0;
		}).appendTo(date_wrapper);
	}

	function get_months(){
		var months = {'January': '01','February': '02','March': '03','April': '04','May': '05','June': '06','July': '07','August': '08','September': '09','October': '10','November': '11','December': '12'};
		var html = '';
		
		for(var key in months){
			html = html + `<div class="cal-month text-center" data-month="{mo}">{month}</div>`.replace('{mo}',months[key]).replace('{month}',key);
		}

		return html;
	}

	function get_years(base_yr){
		html = '';
		var i;
		for(i = 0; i < 12; i++){
			var yr = base_yr + i;
			html = html + `<div class="cal-month text-center yr_opt" data-yr="{yr}">{yr}</div>`.replace(/{yr}/g,yr);
		}

		return html;
	}

	function generate_months_template(dt){
		var dt = new Date(dt);
		var yr = dt.getFullYear();
		var months = get_months();

		var template = `
			<div class="cal-year mx-auto mt-5">
				<div class="calendar-header yr-header">
					<span class="fa fa-angle-left yrprev" data-val="-1"></span>
					<span class="fa fa-angle-right yrnxt" data-val="1"></span>
					<h5 id="year">{yr}</h5>
				</div>
				<div class="grid-months">
					{months}
				</div>
			</div>
		`.replace('{yr}',yr)
		 .replace('{months}',months);

		 return template;
	}

	function generate_years_template(base_yr){
		var upper_yr = base_yr + 11;
		var years = get_years(base_yr);

		var template = `
			<div class="cal-year mx-auto mt-5">
				<div class="calendar-header yr-header">
					<span class="fa fa-angle-left yrsprev" data-val="-12"></span>
					<span class="fa fa-angle-right yrsnxt" data-val="12"></span>
					<h5 id="years" data-byr="{byr}" data-uyr="{uyr}">{byr} - {uyr}</h5>
				</div>
				<div class="grid-months">
					{years}
				</div>
			</div>
		`.replace(/{byr}/g,base_yr)
		 .replace(/{uyr}/g,upper_yr)
		 .replace('{years}',years);

		 return template;
	}

	////////////////////////////////////////////////


	// Announcement
	$(document).on('click','.del-announcement',function(e){
		e.preventDefault();
		if(confirm("Are you sure you want to delete this item?")){
			let url = $(this).attr('href');
			let redirect = $(this).data('redirect');
			$.ajax({
				url: url,
				method: 'post',
				success: function(){
					let notice = 1
					let redirect_url = redirect + '?action=delete&notice=' + notice;
					window.location.replace(redirect_url);
				}
			})
		}
	});

	$(document).on('click','.announce-item:not(.active) a',function(e){
		e.preventDefault();
		let url = $(this).attr('href');

		$.ajax({
			url: url,
			method: 'get',
			success: html_response => {
				$('#announce-highlight').html(html_response);
				$('.announce-item.active').removeClass('active');
				$(this).parent().addClass('active');
			}
		});
	});

	$(document).on('click','.announce-item.active a',function(e){
		e.preventDefault();
	});



	// Approver Leave count
	$(document).on('change','.hierachy-checkbox',function(){
		if(this.checked){
			toggle_hierarchy_checkbox(this, "[name='employeeids[]']", true);
		}else{
			toggle_hierarchy_checkbox(this, "[name='employeeids[]']", false);
		}
	});

	function toggle_hierarchy_checkbox(el, selector, boolean_val){
		$(el).parents('.card').find(selector).each(function(){
			$(this).prop("checked", boolean_val);
		});
	}


	$(document).on('submit','#hierarchy-form',function(e){
		e.preventDefault();

		let url = $(this).attr('action');
		let method = $(this).attr('method');
		let formData = $(this).serialize();

		$.ajax({
			url: url,
			method: method,
			data: formData,
			success: function(response){
				$('#table-count').html(generate_html_hierarchy(response));
			},
			error: function(err){
				// console.log(err.responseText);
				$('#table-count').html(generate_alert_html_hierarchy(err.responseText));
			}
		});
	});

	function generate_html_hierarchy(leave_counts){
		if(leave_counts.length){
			let table = `
				<table class="table table-sm table-bordered">
					<thead class="thead-dark">
						<tr class="text-center">
							<th>Date</th>
							<th>Pending Count</th>
							<th>Approved Count</th>
						</tr>
					</thead>
					<tbody>
						{rows}
					</tbody>
				</table>
			`;

			let rows = '';
			leave_counts.forEach(function(leave_count){
				rows = rows + "<tr class='text-center'>";
				for(let item in leave_count){
					rows = rows + `<td>${leave_count[item]}</td>`;
				}
				rows = rows + "</tr>";
			});

			return table.replace('{rows}',rows);
		}

		return generate_alert_empty_result_html_hierarchy();
	}

	function generate_alert_html_hierarchy(error){
		let alert = `
			<div class="alert alert-danger alert-dismissible fade show">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<strong>Error!</strong> {error}
			</div>
		`;

		return alert.replace('{error}',error).replace('Error:','');
	}

	function generate_alert_empty_result_html_hierarchy(){
		return `
				<div class="alert alert-info alert-dismissible fade show">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Notice!</strong> No records found
				</div>
			  `;
	}

});