<?php

  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Leave_emaillogs_model extends CI_Model{

	public function create($data){
		date_default_timezone_set('Asia/Kuala_Lumpur');

		$row = [
			'leave_request_id' => $data->id,
			'requestor_id' => $data->employeeid,
			'requestor_name' => implode(' ', [$data->firstname,$data->lastname]),
			'to_approver' => $data->approver_email,
			'leave_dt' => $data->leavedt,
			'leave_category' => $data->leave_category,
			'leave_type' => $data->leave_name,
			'createdttm' => date('Y-m-d H:i:s'),
			'updatedttm' => date('Y-m-d H:i:s')
		];

		$this->db->insert('leave_email_logs',$row);
		
		return $this->db->affected_rows();
	}

	public function get_mail_logs(){
		$this->db->select('id, leave_request_id, requestor_id, requestor_name, to_approver, leave_dt, leave_category, leave_type');
		$this->db->from('leave_email_logs');
		$this->db->where('status',0);
		$this->db->order_by('to_approver, requestor_id, leave_dt, leave_category, leave_type');
		$query = $this->db->get();

		return $query->result();
	}

	public function tag_mail_as_sent($id){
		$this->db->where('id',$id);
		$this->db->update('leave_email_logs',array('status' => 1));
	}

}

