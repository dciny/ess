<div class="container">

	<div class="row mb-4">
		<div class="col-md-12">
		  <div class="ml-4">
		    <a class="cta-def-btn" href="<?php echo base_url('leave'); ?>">
		      <span class="back-icon">Back</span>
		    </a>
		  </div>
		</div>
	</div>	

<ul class="nav nav-tabs">
	<li class="nav-item">
		<a class="nav-link" href="<?php echo base_url('leave/monthly-report/approved') ?>">Approved</a>
	</li>
	<li class="nav-item">
		<a class="nav-link active">Pending For Approval</a>
	</li>
</ul>

	<div class="row">
		<div class="col-md-12">
			<div class="table-responsive">
				<table class="table table-bordered">
					<thead class="thead-light">
						<tr class="text-center">
							<th>Month</th>
							<th>OPS</th>
							<th>IT</th>
							<th>QA</th>
							<th>HR</th>
							<th>Facilities</th>
							<th>Total</th>
						</tr>
					</thead>
					<tbody>
						<?php
							foreach ($months as $mkey => $month_counts){
								echo "<tr class='text-center'>";
								echo "<td>$mkey</td>";
								foreach ($month_counts as $count) {
									echo "<td>$count</td>";
								}
								echo "<td class='table-primary text-dark font-weight-bold'>". array_sum($month_counts) ."</td>";
								echo "</tr>";
							}
						?>
						<tr class="text-center table-info font-weight-bold">
							<td>YR Total</td>
							<td class="text-dark">
								<?php 
									$ops = array_sum([$months['January'][0],$months['February'][0],$months['March'][0],$months['April'][0],$months['May'][0],$months['June'][0],$months['July'][0],$months['August'][0],$months['September'][0],$months['October'][0],$months['November'][0],$months['December'][0]]); 
									echo $ops;
								?>
							</td>
							<td class="text-dark">
								<?php 
									$it = array_sum([$months['January'][1],$months['February'][1],$months['March'][1],$months['April'][1],$months['May'][1],$months['June'][1],$months['July'][1],$months['August'][1],$months['September'][1],$months['October'][1],$months['November'][1],$months['December'][1]]) ;
									echo $it;
								?>
							</td>
							<td class="text-dark">
								<?php 
									$qa = array_sum([$months['January'][2],$months['February'][2],$months['March'][2],$months['April'][2],$months['May'][2],$months['June'][2],$months['July'][2],$months['August'][2],$months['September'][2],$months['October'][2],$months['November'][2],$months['December'][2]]) ;
									echo $qa;
								?>
							</td>
							<td class="text-dark">
								<?php 
									$hr = array_sum([$months['January'][3],$months['February'][3],$months['March'][3],$months['April'][3],$months['May'][3],$months['June'][3],$months['July'][3],$months['August'][3],$months['September'][3],$months['October'][3],$months['November'][3],$months['December'][3]]) ;
									echo $hr;
								?>
							</td>
							<td class="text-dark">
								<?php 
									$fc = array_sum([$months['January'][4],$months['February'][4],$months['March'][4],$months['April'][4],$months['May'][4],$months['June'][4],$months['July'][4],$months['August'][4],$months['September'][4],$months['October'][4],$months['November'][4],$months['December'][4]]) ;
									echo $fc;
								?>
							</td>
							<td class="text-dark"><?php echo array_sum([$ops,$it,$qa,$hr,$fc]) ?></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>