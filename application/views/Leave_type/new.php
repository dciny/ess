
<h5 class="dark-orange">Add Leave Type</h5>
<hr>
<div class="hierarchy-grid">
	<div class="border w-min360px">
		<div class="border-bottom px-4 py-2 bg-warning-gradient text-light">
			<strong>Leave Type Form</strong>
		</div>
		<form action="<?php echo base_url('leave-type/create') ?>" method="post">
			<div class="fontsize-14 px-4 py-2 intro-box">
				<div class="form-group">
					<label>Leave Name</label>
					<input type="text" name="leave_name" class="form-control form-control-sm <?php if(form_error('leave_name')) echo 'is-invalid' ?>" value="<?php echo set_value('leave_name') ?>">
					<?php echo form_error('leave_name'); ?>
				</div>
				<div class="form-group">
					<label>Short Description</label>
					<input type="text" name="leave_desc" class="form-control form-control-sm">
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-primary" value="Create">
				</div>
			</div>
		</form>
	</div>
</div>