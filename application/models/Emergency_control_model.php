<?php

  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Emergency_control_model extends CI_Model{

	public function create_user($employeeid=0, $pw='', $leave_role_id='', $pw_chng_ind=0){
		$data = array(
			'username' => $employeeid,
			'password' => password_hash($pw, PASSWORD_DEFAULT),
			'leave_role_id' =>  $leave_role_id,
			'is_password_change' => $pw_chng_ind,
			'attempt' => 0
		);

		$this->db->insert('user', $data);
		return $this->db->affected_rows();
	}

	public function is_valid_userid($employeeid=0){
		$this->db->where('employeeid',$employeeid);
		$numrows = $this->db->count_all_results('prlemployeemaster');

		return $numrows >= 1 ? true : false;
	}

	public function is_user_exists($employeeid=0){
		$this->db->where('username',$employeeid);
		$numrows = $this->db->count_all_results('user');

		return $numrows >= 1 ? true : false;
	}

	public function update_password($username, $password){
		$data = array(
			'password' => password_hash($password, PASSWORD_DEFAULT),
			'is_password_change' => 0,
		);

		$this->db->where('username', $username);
		$this->db->update('user', $data);

		return $this->db->affected_rows();
	}	

}


