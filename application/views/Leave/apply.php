<div class="modal-bg1 modal-hidden">
	<div class="modal-content1">
		<div class="d-flex flex-column align-items-center">
			<span>Please Wait</span>
			<div class="d-flex justify-content-center" style="gap: 0.3rem">
				<div class="spinner-grow text-success"></div>
				<div class="spinner-grow text-info"></div>
				<div class="spinner-grow text-warning"></div>
			</div>
		</div>
	</div>
</div>

<div class="border mx-4 leave-dashboard">
	<div class="border-bottom">
		<ul class="d-flex m-0">
			<li class="p-4"><a href="<?php echo base_url('leave') ?>">Dashboard</a></li>
			<li class="p-4 leave-tab-active">Apply Leave</li>
			<?php if($user_details->leave_role_id == 1){ ?>
				<li class="p-4"><a href="<?php echo base_url('leave/delegate') ?>">Leave Delegate</a></li>
			<?php } ?>
			<?php if($is_approver){ ?>
				<li class="p-4"><a href="<?php echo base_url('leave/requests') ?>">Leave Requests</a></li>
				<li class="p-4"><a href="<?php echo base_url('leave/approver/leave-count-page') ?>">Leave Count</a></li>
			<?php } ?>
			<?php if($user_details->leave_role_id == 1){ ?>
				<li class="p-4"><a href="<?php echo base_url('leave/tracker') ?>">Leave Tracker</a></li>
			<?php } ?>	
			<?php if($user_details->leave_role_id == 1){ ?>
				<li class="p-4"><a href="<?php echo base_url('leave/monthly-report/approved') ?>">Monthly Tracker</a></li>
			<?php } ?>
		</ul>
	</div>
	<div class="p-3 leave-dashboard-grid font-14">
		
		<div class="cal-space">
			<?php 
				// $this->load->view('Leave/calendar',$leaves);
				$this->load->view('Leave/calendar');
			?>
		</div>

		<div class="cal-form">
			<?php 
				if(isset($leave_assignment )){ 
			?>
					<form class="lreq_form" action="<?php echo base_url('leave/create') ?>" method="post">
						<h5>Leave Form</h5>
						<div class="mb-3">
							<label>Selected Dates:</label>
							<div id="sel-dates" class="border p-2 font-14">
								
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Leave Type</label>
									<select class="form-control form-control-sm" id="leave_type" name="leave_type_id">
										<?php foreach ($leave_types as $leave_type) { ?>
											<option value="<?php echo $leave_type->id ?>"><?php echo $leave_type->leave_name  ?></option>
										<?php } ?>
									</select>
									<p class="text-danger mt-2" id="lt-note">
										<?php
											if(in_array($leave_types[0]->id,[2,5,6,7])){
												echo "Note: You can only select dates 7 days ahead from today's date";
											}
										?>
									</p>
								</div>
								<div class="form-group">
									<label>Leave Category</label>
									<select class="form-control form-control-sm" name="leave_category" id="leave_category">
										<option value="paid">Paid</option>
										<option value="unpaid">Unpaid</option>
									</select>
								</div>
								<div class="form-group">
									<label>Reason for leave</label>
									<textarea class="form-control" name="leave_reason"></textarea>
								</div>
								<div>
									<label>Leave Credits</label>
									<input type="number" class="borderless-input" name="duration" value="<?php echo $leave_assignment->leave_credits ?>" data-val="<?php echo $leave_assignment->leave_credits ?>" readonly>
									<input type="hidden" name="employeeid" value="<?php echo $employee->employeeid ?>">
								</div>
								<div>
									<input type="submit" class="btn btn-primary" value="Submit">
								</div>
								<div id="err-msg"></div>
							</div>
						</div>
					</form>
			<?php 
				}else{
					echo "<div class='mt-3'>You have not assigned any leave credits</div>";
				}
			?>
		</div>

	</div>
</div>