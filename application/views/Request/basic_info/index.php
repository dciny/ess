<div class="main container-fluid">
	<div class="row mb-4">
		<div class="col-md-12">
			<div class="ml-3">
				<a class="cta-def-btn" href="<?php echo base_url() ?>">
					<span class="back-icon">Back</span>
				</a>
			</div>
		</div>
	</div>
	<div class="row mb-5">
		<div class="col-xl-2 col-sm-12 mb-5">
			<ul class="list-group">
				<li class="list-group-item active"> Basic Details</a></li>
				<li class="list-group-item list-hover"><a href="<?php echo base_url('request/contact/index') ?>">Contacts</a></li>
				<li class="list-group-item list-hover"><a href="<?php echo base_url('request/address/index') ?>">Addresses</a></li>
				<li class="list-group-item list-hover"><a href="#">Government Info</a></li>
				<li class="list-group-item list-hover"><a href="#">Work Details</a></li>
			</ul>
		</div>
		<div class="col-xl-9 col-sm-12">
			<h6>Requests Details</h6>
			<div class="box">
				<a class="d-block mb-3 text-success" href="<?php echo base_url('request/basic-info/new') ?>">New Request</a>
				<div class="table-responsive">
					<table class="table table-sm">
						<thead class="thead-dark">
							<tr class="text-center">
								<th>First Name</th>
								<th>Middle Name</th>
								<th>Last Name</th>
								<th>Birthday</th>
								<th>Status</th>
								<th>Created</th>
								<th>Updated</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php 
								if(isset($basic_requests)){ 
									foreach ($basic_requests as $basic_request){
							?>
										<tr class="text-center">
											<td class="align-middle"><?php display_col($basic_request->firstname) ?></td>
											<td class="align-middle"><?php display_col($basic_request->middlename) ?></td>
											<td class="align-middle"><?php display_col($basic_request->lastname) ?></td>
											<td class="align-middle"><?php display_date_col($basic_request->birthdate) ?></td>
											<td class="align-middle <?php echo $basic_request->status == 'For approval' ? 'text-warning' : 'text-success' ?>">
												<?php display_col($basic_request->status) ?>
											</td>
											<td class="align-middle"><?php display_datetime_col($basic_request->createdttm) ?></td>
											<td class="align-middle"><?php display_datetime_col($basic_request->updatedttm) ?></td>
											<td class="align-middle">
												<?php if($basic_request->status == 'For approval'){ ?>
													<a href='<?php echo base_url("request/basic-info/edit/$basic_request->id") ?>'>
														<span class="btn btn-sm btn-primary">Edit</span>
													</a>
												<?php } ?>
											</td>
										</tr>
							<?php 
									}
								}else{
									echo "<tr class='text-center'><td colspan='8'>No active request found</td></tr>";
								}	 
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>