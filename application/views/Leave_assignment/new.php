
<h5 class="dark-orange">Assign Leaves</h5>
<hr>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-4 col-lg-3 col-xl-2 ">
			<form action="<?php echo base_url('leave-assignment/new') ?>" method="post">
				<div class="form-group">
					<h5>Hierarchy Rule</h5>
					<select id="jhrule" name="leave_hierarchy_id" class="form-control form-control-sm">
						<?php 
							foreach ($leave_hierarchies as $leave_hierarchy) { 
								$selected = ($lhid == $leave_hierarchy->id) ? 'selected' : '';
								echo "<option value='$leave_hierarchy->id' $selected>$leave_hierarchy->rule_name</option>";
							}
						 ?>
					</select>
				</div>
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 col-lg-5 col-xl-4 mt-4">
			<h5>Employees</h5>
			<div class="input-group mb-1">
				<input type="text" class="form-control form-control-sm" name="search" placeholder="Search...">
				<div class="input-group-append">
					<span class="btn btn-secondary btn-sm">Submit</span>
				</div>
			</div>
			<div class="list-box border">
				<ul class="list-group list-group-flush">
					<?php foreach ($unassigned_members as $unassigned_member) { ?>
						<li class="list-group-item p-0">
							<form action="<?php echo base_url('leave-assignment/create') ?>" method="post">
								<table class="table table-borderless table-sm fontsize-14 mb-0">
									<tbody>
										<tr class="text-center">
											<td class="align-middle" style="width: 362px">
												<?php echo implode(' ', [$unassigned_member->firstname,$unassigned_member->lastname]) ?>
												<input type="hidden" name="employeeid" value="<?php echo $unassigned_member->employeeid ?>">
												<input type="hidden" name="leave_hierarchy_id" value="<?php echo $lhid ?>">
											</td>
											<td><input type="submit" name="submit" class="btn btn-sm btn-success" value="Add"></td>
										</tr>
									</tbody>
								</table>
							</form>
						</li>
					<?php } ?>
				</ul>
			</div>
		</div>
		<div class="col-md-6 col-lg-5 col-xl-4 mt-4">
			<h5>Members</h5>
			<div class="input-group mb-1">
				<input type="text" class="form-control form-control-sm" name="search" placeholder="Search...">
				<div class="input-group-append">
					<span class="btn btn-secondary btn-sm">Submit</span>
				</div>
			</div>
			<div class="list-box border">
				<ul class="list-group list-group-flush">
					<?php foreach ($members as $member) { ?>
						<li class="list-group-item p-0">
							<form action="<?php echo base_url('leave-assignment/delete') ?>" method="post">
								<table class="table table-borderless table-sm fontsize-14 mb-0">
									<tbody>
										<tr class="text-center">
											<td class="align-middle" style="width: 362px">
												<?php echo implode(' ', [$member->firstname,$member->lastname]) ?>
												<input type="hidden" name="employeeid" value="<?php echo $member->employeeid ?>">
												<input type="hidden" name="leave_hierarchy_id" value="<?php echo $lhid ?>">
											</td>
											<td><input type="submit" name="submit" class="btn btn-sm btn-danger" value="Remove"></td>
										</tr>
									</tbody>
								</table>
							</form>
						</li>
					<?php } ?>
				</ul>
			</div>
		</div>
	</div>
</div>