<?php
  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Emergency_control extends CI_Controller {

	public function __construct(){

		parent::__construct();
		
		$this->load->model('emergency_control_model','ec_control');
		$this->load->model('auth_model');
		$this->load->model('leave_role_model','leave_role');
		$this->load->helper('url');
	}

	// public function _remap($method, $params = []){
	// 	$login_session = $this->session->userdata('logged_in');

	// 	if(isset($login_session)){
			
	// 		$is_password_change =$this->session->userdata['logged_in']['is_password_change'];
	// 		if($is_password_change == 0){
	// 			redirect('auth/change-password', 'location');
	// 		}

	// 		$this->$method($params);
	// 	}else{
	// 		redirect('auth/index', 'location');
	// 	}
	// }


	public function index(){
		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Emergency_control/index',[],'auth');
	}

	
	/**** Insert user ****/
	public function new_user(){
		$this->load->library('layouts'); // load layout library
		$data['leave_roles'] = $this->leave_role->all();
		$this->layouts->view('Emergency_control/new_user',$data,'auth');
	}

	public function create_user(){
		$this->form_validation->set_error_delimiters('<p class="invalid-feedback m-0">', '</p>');
		$this->form_validation->set_rules('username', 'username', 'trim|required|callback_is_username_available|callback_is_username_valid');
		$this->form_validation->set_rules('password', 'password', 'trim|required');
		$this->form_validation->set_rules('leave_role_id', 'leave role', 'trim|required');

		if ($this->form_validation->run() == FALSE){
			// show again the edit form
			$this->load->library('layouts'); // load layout library
			$this->layouts->view('Emergency_control/new_user',[],'auth');
		}else{
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$leave_role_id = $this->input->post('leave_role_id');

			if($this->ec_control->create_user($username,$password,$leave_role_id)){
				$this->session->set_flashdata('success','Successfully created a user');
			}else{
				$this->session->set_flashdata('error','Something went wrong during insert');
			}
			redirect('emergency-control/new-user', 'location');
		}
	}

	public function is_username_available($username){
		if(empty($this->auth_model->get_user($username))){
			return true;
		}
		$this->form_validation->set_message('is_username_available', 'The {field} already exist');
		return false;
	}

	public function is_username_valid($username){
		if($this->auth_model->get_user_info($username)){
			return true;
		}
		$this->form_validation->set_message('is_username_valid', 'The {field} is not a valid employeeid');
		return false;
	}
	/**** End ****/

	
	/**** Edit password ****/
	public function edit_password(){
		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Emergency_control/edit_password',[],'auth');
	}

	public function update_password(){
		$this->form_validation->set_error_delimiters('<p class="invalid-feedback m-0">', '</p>');
		$this->form_validation->set_rules('username', 'Username', 'trim|required|callback_is_username_exists');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');		

		if ($this->form_validation->run() == FALSE){
			// show again the edit form
			$this->load->library('layouts'); // load layout library
			$this->layouts->view('Emergency_control/edit_password',[],'auth');
		}else{
			$username = $this->input->post('username');
			$password = $this->input->post('password');	
					
			if($this->ec_control->update_password($username,$password)){
				$this->session->set_flashdata('success','Password has been changed');
			}else{
				$this->session->set_flashdata('error','Something went wrong during update');
			}
			redirect('emergency-control/edit-password', 'location');
		}

	}

	public function is_username_exists($username){
		if(!empty($this->auth_model->get_user($username))){
			return true;
		}
		$this->form_validation->set_message('is_username_exists', 'The {field} does not exist');
		return false;
	}

	/**** End ****/


}
