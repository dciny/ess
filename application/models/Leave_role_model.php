<?php

  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Leave_role_model extends CI_Model{

	public function all(){
		$query = $this->db->get('leave_roles');

		return $query->result();
	}

}

