<div class="row mx-auto">
	<div class="col-md-7">

		<?php if(isset($notice)){ 
			  	if($notice == 1){
			  		$alert = 'alert-success';
			  		if($action == 'delete'){
			  			$message = "<strong>Success!</strong> Deleted an announcement";	
			  		}else{
			  			$message = "<strong>Success!</strong> Created an announcement";
			  		}
			  	}else{
			  		$alert = 'alert-danger';
			  		$message = "<strong>Failed!</strong> Something went wrong";
			  	}
		?>

		<div class="alert <?php echo $alert ?> alert-dismissible">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $message ?>
		</div>

		<?php } ?>


		<div class="d-flex justify-content-end" style="gap: 1rem">
			<a href="<?php echo base_url('announcement') ?>" class="btn btn-secondary mb-3"><span class="fa fa-mail-reply"></span> Back to Home</a>
			<a href="<?php echo base_url('announcement/new') ?>" class="btn btn-success mb-3"><span class="fa fa-plus"></span> New</a>
		</div>
		<ul class="list-group">
			<?php foreach ($announcements as $announcement) { ?>
				<li class="list-group-item1">
					<div class="announce-head announce-head-sm pl-3 pt-3">
						<h5 class="text-dark"><strong class="text-dark"><?php echo ucwords($announcement->title) ?></strong></h5>
						<small class="text-muted"><?php echo date('F d, Y',strtotime($announcement->createdttm)) ?></small>
					</div>
					<div class="announce-body pl-3 pb-3 pr-3">
						<div class="text-dark mt-3 ellipsis">
							<small><?php echo strip_tags($announcement->body) ?></small>
						</div>
					</div>
					<div class="d-flex" style="gap: 1rem">
						<div>
							<a href="<?php echo base_url('announcement/'.$announcement->id.'/edit') ?>" class="btn btn-secondary">
								<small>
									<span class="fa fa-edit"></span>
									<span>Edit</span>
								</small>
							</a>
						</div>
						<div>
							<a href="<?php echo base_url('announcement/'.$announcement->id) ?>" class="btn btn-secondary">
								<small>
									<span class="fa fa-eye"></span>
									<span>Show</span>
								</small>
							</a>
						</div>
						<div>
							<a href="<?php echo base_url('announcement/'.$announcement->id.'/destroy') ?>" class="btn btn-danger del-announcement" data-redirect="<?php echo base_url('announcement/all/') ?>">
								<small>
									<span class="fa fa-remove"></span>
									<span>Delete</span>
								</small>
							</a>
						</div>
					</div>
				</li>
			<?php } ?>	
		</ul>
	</div>
</div>


