<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payslip_model extends CI_Model{

	public function get_latest_payperiod(){
		$this->db->select_max('payperiod');
		$query = $this->db->get('prlpaysummary');

		$result = $query->row();
		return $result->payperiod;
	}

	public function get_all_payperiod($employeeid=0){
		$this->db->select('payperiod');
		$this->db->where('employeeid',$employeeid);
		$this->db->distinct();
		$this->db->order_by('payperiod','desc');
		$query = $this->db->get('prlpaysummary');

		if ($query->num_rows() > 0){
			return $query->result();
		}

		return null;
	}

	public function get_emp_payslip_info($username,$payperiod){
		// $payperiod = $this->get_latest_payperiod();

		// set query string to be run
		$this->db->select('emp.firstname,emp.lastname,emp.employeeid,emp.hourlyrate,sum.basicpay,sum.holidaypay,sum.regot,sum.6thdayot as ot6,sum.7thdayot as ot7,sum.totalnd,sum.payperiod,sum.deductions,sum.loandeductions,sum.tax,sum.sss,sum.phic,sum.hdmf,sum.netpay');
		$this->db->from('prlemployeemaster as emp');
		$this->db->join('prlpaysummary as sum', 'emp.employeeid = sum.employeeid', 'left');
		$this->db->where('emp.employeeid',$username);
		$this->db->where('sum.payperiod',$payperiod);

		// run the query
		$query = $this->db->get();
		// get 1 row on the query result
		$result = $query->row();

		return $result;
	}

	public function get_prlotherinclog($username,$payperiod){
		// $payperiod = $this->get_latest_payperiod();

		$this->db->select('description, amount');
		$this->db->from('prlotherinclog');
		$this->db->where('employeeid',$username);
		$this->db->where('payperiod',$payperiod);
		$this->db->where('amount >',0);
		$query = $this->db->get();
		
		if ($query->num_rows() > 0){
			return $query->result();
		}

	}

	public function get_prlotherinclog2($username,$payperiod){
		// $payperiod = $this->get_latest_payperiod();

		$this->db->select('description,amount');
		$this->db->from('prlotherinclog');
		$this->db->where('employeeid',$username);
		$this->db->where('payperiod',$payperiod);
		$this->db->where('amount <',0);
		$query = $this->db->get();
		
		if ($query->num_rows() > 0){
			return $query->result();
		}

	}

	public function get_prladjustmentlog2($username,$payperiod){
		// $payperiod = $this->get_latest_payperiod();

		$this->db->select('description,amount');
		$this->db->from('prladjustmentlog');
		$this->db->where('employeeid',$username);
		$this->db->where('payrollid',$payperiod);
		$this->db->where('amount <', 0);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}

	public function get_prladjustmentlog($username,$payperiod){
		// $payperiod = $this->get_latest_payperiod();
		
		$this->db->select('description, amount');
		$this->db->from('prladjustmentlog');
		$this->db->where('employeeid',$username);
		$this->db->where('payrollid',$payperiod);
		$this->db->where('amount >', 0);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}

	public function get_prlloansgov($username){
		$date = "1025";
		$this->db->select('loantype,loanamt');
		$this->db->from('prlloansgov');
		$this->db->where('empid',$username);
		$this->db->where('deductiondate',$date);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}


}
