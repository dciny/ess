<?php

  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Leave_assignment_model extends CI_Model{

	public function unassigned($leave_type_id){
		$this->db->select('p.employeeid, p.firstname, p.lastname');
		$this->db->from('prlemployeemaster p');
		$this->db->join('leave_assignments as la',"la.employeeid = p.employeeid AND la.is_active = 1 AND la.leave_type_id = $leave_type_id",'left');
		$this->db->where('p.active',0);
		$this->db->where('la.id IS NULL');
		$query = $this->db->get();

		return $query->result();
	}

	public function insert_or_update($data){
		$leave_assignment = array(
			'employeeid' => $data['employeeid'],
			'leave_hierarchy_id' => $data['leave_hierarchy_id']
		);

		date_default_timezone_set('Asia/Kuala_Lumpur');

		// if(!empty($this->find_by_composite_ids($leave_assignment)))
		// 	$employee = $this->find_by_composite_ids($leave_assignment)[0];

		// make sure these statements are executed after calling find_by_composite_ids
		$leave_assignment['is_active'] = 1;
		$leave_assignment['updatedttm'] = date('Y-m-d H:i:s');

		// if(isset($employee) && !empty($employee)){
		// 	unset($leave_assignment['employeeid'], $leave_assignment['leave_hierarchy_id']);
		// 	$this->db->where('employeeid',$employee->employeeid)
		// 	         ->where('leave_hierarchy_id',$employee->leave_hierarchy_id)
		// 	         ->update('leave_assignments',$leave_assignment);
		// }else{
			$leave_assignment['createdttm'] = date('Y-m-d H:i:s');
			$leave_assignment['leave_credits'] = $this->compute_leave_credits($leave_assignment['employeeid']);
			$this->db->insert('leave_assignments',$leave_assignment);
		// }

		return $this->db->affected_rows();
	}

	public function delete($data){
		date_default_timezone_set('Asia/Kuala_Lumpur');

		$leave_assignment = array(
			'is_active' => 0,
			'updatedttm' => date('Y-m-d H:i:s')
		);

		$this->db->where('employeeid',$data['employeeid']);
		$this->db->where('leave_hierarchy_id',$data['leave_hierarchy_id']);
		// $this->db->update('leave_assignments',$leave_assignment);
		$this->db->delete('leave_assignments');
		
	}

	private function compute_leave_credits($employeeid){
		//  get hiredate
		$this->db->select('hiredate, leave_accrual');
		$this->db->from('prlemployeemaster');
		$this->db->where('employeeid',$employeeid);
		$query = $this->db->get();
		$hiredate_raw = $query->row()->hiredate;
		//  format the hiredate to 'Y-m-01'
		$hiredate_formatted = date_format(date_create($hiredate_raw),'Y-m-01');
		$hiredate = date_create($hiredate_formatted);
		//  format this months date to 'Y-m-01'
		$this_month = date_create(date('Y-m-01'));
		// month difference between hiredate and this month's date
		$date_diff = date_diff($hiredate, $this_month);
		$month_diff = ($date_diff->format('%y') * 12) + $date_diff->format('%m');
		$this_mo = date('m'); // this month's count

		$leave_credits = 0;
		if($month_diff == 6){
			$leave_credits = 6;
		}elseif($this_mo == 1){
			$leave_credits = 0;
		}elseif($month_diff > 6){
			// get employee's leave accrual
			$leave_accrual = (float) $query->row()->leave_accrual;
			$leave_credits = $this_mo * $leave_accrual;
			// if date execution is not the last day of the month then less 1 month leave accrual since job run didn't run yet
			if(date('t') > date('d')){
				$leave_credits = $leave_credits - $leave_accrual;
			}

			// get all leaves from leave_requests table with status 3 and 5 from february of current year
			// february because it's the start of the month where employees are eligble to leave base on leave credits
			$feb = date('Y-02-01');
			$this->db->from('leave_requests');
			$this->db->where('employeeid',$employeeid);
			$this->db->where('leavedt >=', $feb);
			$this->db->where('leave_category', 'paid');
			$this->db->where_in('overall_status',[3,5]);
			$ctr = $this->db->count_all_results();
			// multiply the number with the leave_accrual and minus the result from $leave_credits
			$leave_credits = $leave_credits - ($ctr * $leave_accrual);
		}

		return $leave_credits;

	}

	public function get_membersby_leavehierarchy($leave_hierarchy_id){
		$this->db->select('la.employeeid, p.firstname, p.lastname');
		$this->db->from('leave_assignments as la');
		$this->db->join('prlemployeemaster as p','la.employeeid = p.employeeid','left');
		$this->db->where('la.leave_hierarchy_id',$leave_hierarchy_id);
		$this->db->where('la.is_active',1);
		$query = $this->db->get();

		return $query->result();
	}

	public function get_unassigned_employee(){
		$this->db->select('p.employeeid, p.firstname, p.lastname');
		$this->db->from('prlemployeemaster as p');
		$this->db->join('leave_assignments as la','p.employeeid = la.employeeid AND la.is_active = 1','left');
		$this->db->where('p.active',0);
		$this->db->where('la.id IS NULL');
		$query = $this->db->get();

		return $query->result();
	}

	public function find_by_composite_ids($filters){
		foreach ($filters as $key => $val) {
			$this->db->where($key,$val);
		}
		$query = $this->db->get('leave_assignments');
		return $query->result();
	}

	public function get_leave_details($employeeid){
		$this->db->where('employeeid',$employeeid);
		$this->db->where('is_active',1);
		$query = $this->db->get('leave_assignments');

		return $query->row();
	}

	public function update_duration($employeeid, $leave_credits){
		date_default_timezone_set('Asia/Kuala_Lumpur');
		$data = array(
			'leave_credits' => $leave_credits,
			'updatedttm' => date('Y-m-d')
		);
		$this->db->where('employeeid', $employeeid);
		$this->db->update('leave_assignments',$data);
	}

	public function is_assigned($employeeid){
		$count = $this->db->from('leave_assignments')
		                  ->where('employeeid', $employeeid)
		                  ->count_all_results();

		if($count > 0)
			return true;
		return false;
	}
}

