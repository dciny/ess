<div class="main container-fluid">
	<div class="row mb-5">
		<div class="col-md-12">
			<div class="ml-5">
				<a class="cta-def-btn" href="<?php echo base_url('employee/show'); ?>">
					<span class="back-icon">Back</span>
				</a>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 p-0">
			<div class="mx-5">
				<p class="text-danger">* Send change request to HR</p>
			</div>
			<div class="box cyan-lin-bg box-min-size mx-5">
				<h5>Address</h5>
				<form action="/request/address/create" method="post">
					<div class="table-responsive">
						<table class="table">
							<tbody>
								<tr>
									<td style="width: 120px">Address 1:</td>
									<td>
										<input type="text" 
										       name="address1" 
											   class="form-control <?php if(form_error('address1')) echo 'is-invalid' ?>" 
											   value="<?php echo $employee->address1 ?>">
										<?php echo form_error('address1'); ?>
									</td>
								</tr>
								<tr>
									<td style="width: 120px">Address 2:</td>
									<td>
										<input type="text" 
										       name="address2" 
										       class="form-control <?php if(form_error('address2')) echo 'is-invalid' ?>" 
										       value="<?php echo $employee->address2 ?>">
										<?php echo form_error('address2'); ?>
									</td>
								</tr>
								<tr class="text-right">
									<td colspan="2">
										<input type="submit" name="submit" value="Send Change Request" class="btn cta-btn cyan-bg-btn">
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>