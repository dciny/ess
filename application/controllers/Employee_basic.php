<?php
  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Employee_basic extends CI_Controller {

	public function __construct(){

		parent::__construct();
		
		$this->load->model('employee_basic_model');
		$this->load->model('employee_model');
		$this->load->helper('url');
	}

	public function _remap($method, $params = []){
		$login_session = $this->session->userdata('logged_in');

		if(isset($login_session)){

			$is_password_change = $this->session->userdata['logged_in']['is_password_change'];
			if($is_password_change == 0){
				redirect('auth/change-password', 'location');
			}

			$this->$method($params);
		}else{
			redirect('auth/index', 'location');
		}
	}

	public function index(){
		$employeeid = $this->session->userdata['logged_in']['employeeid'];
		$data['employee'] = $this->employee_model->get_employee_details($employeeid);
		$data['users'] = $this->employee_model->get_user_id($employeeid);
		$data['basic_requests'] = $this->employee_basic_model->get_all_basic_details($employeeid);
		$data['header'] = 'REQUEST: BASIC DETAILS';

		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Request/basic_info/index',$data,'app');
	}

	public function create(){
		$params = $this->set_params();
		$is_exists = $this->is_exists($params['employeeid']); // return an id if exists else false

		if(!$is_exists){
			$this->set_form_validation();
			if ($this->form_validation->run() == FALSE){
				$data['employee'] = $this->employee_model->get_employee_details($params['employeeid']);
				$data['users'] = $this->employee_model->get_user_id($params['employeeid']);
				$this->load->library('layouts'); // load layout library
				$this->layouts->view('Request/basic_info/new',$data,'app');
			}else{
				$this->db->insert('req_employee_basic', $params);
				redirect('request/basic-info/index', 'location');
			}
		}else{
			$this->update($is_exists);
		}
	}

	public function edit($id){
		$id = is_array($id) ? $id[0] : $id;
		$employeeid = $this->session->userdata['logged_in']['employeeid'];
		if($this->employee_basic_model->is_authorized($id,$employeeid)){
			$data['employee'] = $this->employee_model->get_employee_details($employeeid);
			$data['users'] = $this->employee_model->get_user_id($employeeid);
			$data['basic_details'] = $this->employee_basic_model->get_basic_details($id);
			$this->load->library('layouts'); // load layout library
			$this->layouts->view('Request/basic_info/edit',$data,'app');
		}else{
			redirect('/unauthorized');
		}

	}

	public function update($id){
		$id = is_array($id) ? $id[0] : $id;
		$this->set_form_validation();
		$data['basic_details'] = $this->employee_basic_model->get_basic_details($id);

		if ($this->form_validation->run() == FALSE){
			$data['employee'] = $this->employee_model->get_employee_details($params['employeeid']);
			$data['users'] = $this->employee_model->get_user_id($params['employeeid']);
			$this->load->library('layouts'); // load layout library
			$this->layouts->view('Request/basic_info/edit',$data,'app');
		}else{
			$params = $this->set_params();
			unset($params['createdttm']);
			if($this->employee_basic_model->is_authorized($id,$params['employeeid'])){
				$this->db->update('req_employee_basic', $params, array('id' => $id));
				redirect('request/basic-info/index', 'location');
			}else{
				redirect('/unauthorized');
			}
		}
	}


	private function is_exists($params){
		return $this->employee_basic_model->is_exists($params);
	}

	private function set_params(){
		date_default_timezone_set("Asia/Kuala_Lumpur");

		$params['employeeid'] = $this->session->userdata['logged_in']['employeeid'];
		$params['firstname'] = $this->input->post('firstname');
		$params['middlename'] = $this->input->post('middlename');
		$params['lastname'] = $this->input->post('lastname');
		$params['birthdate'] = $this->input->post('birthdate');
		$params['createdttm'] = date('Y-m-d H:i:s');
		$params['updatedttm'] = date('Y-m-d H:i:s');


		return $params;
	}

	private function set_form_validation(){
		$this->form_validation->set_error_delimiters('<p class="invalid-feedback m-0">', '</p>');
		$this->form_validation->set_rules('firstname', 'First name', 'trim|required');
		$this->form_validation->set_rules('middlename', 'Middle name', 'trim|required');
		$this->form_validation->set_rules('lastname', 'Last name', 'trim|required');
		$this->form_validation->set_rules('birthdate', 'Birthday', 'trim|required');
	}


}
