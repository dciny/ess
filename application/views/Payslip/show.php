<style type="text/css">
  .bg-blue{
   background-color:#007bff ;
    color: white !important;

    font-size: 18px;


  }
    .bg-b{
    background-color: rgb(5,111,197);
    color: white;

    font-size: 13px;

  }

  .no-bg{
    background-color:#25e02a;
    color: black;
  }

</style>

<div class="container-fluid">
  <div class="row mb-4">
    <div class="col-md-12">
      <div class="ml-5">
        <a class="cta-def-btn" href="<?php echo base_url(); ?>">
          <span class="back-icon">Back to home</span>
        </a>
      </div>
    </div>
  </div>
</div>


<div class="container print">

  <div class="card">
    <div class="card-header bg-primary bg-b">
      <h1>
        <img src="<?php echo base_url(); ?>assets/image/logowhite.svg" height="4%" width="4%">
        &nbspPayslip
        <button style="float: right;" onclick="myFunction()" class="btn btn-success no-print">
          Print<i class="ml-2 fas fa-print"></i>
        </button>
      </h1>
    </div>

    <div class="card-body">
      <div class="row">
        <div class="col-sm-6">
          <div class="card card-primary">
            <div class="card-heading bg-primary bg-b"><h3 align="center">Employee Pay Summary</h3></div>
            <div class="card-body">
              <table class="table bg-blue">
                <tbody>    
                  <tr>
                    <td class= "print">Employee Name</td>
                    <td class= "print">:</td>
                    <td class= "print"><?php echo $employee->firstname. " " .$employee->lastname ?></td>
                  </tr>
                  <tr>
                    <td class= "print">Employee ID</td>
                    <td class= "print">:</td>
                    <td class= "print"><?php echo $employee->employeeid ?></td>
                  </tr>
                  <tr>
                    <td class= "print">Hourly Rate</td>
                    <td class= "print">:</td>
                    <td class= "print"><?php echo $employee->hourlyrate ?></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>

          <div class="card">
            <div class="card-header bg-primary bg-b"><h3>Earnings</h3></div>
            <div class="card-body">
              <table class="table bg-blue">
                <tbody>
                  <tr>
                    <td class= "print">Basic Pay</td>
                    <td class= "print">:</td>
                    <td class= "print"><?php echo $employee->basicpay ?></td>
                  </tr>
                  <tr>
                    <td class= "print">Holiday Pay</td>
                    <td class= "print">:</td>
                    <td class= "print"><?php echo $employee->holidaypay ?></td>

                  </tr>
                  <tr>
                    <td class= "print">Regular OT</td>
                    <td class= "print">:</td>
                    <td class= "print"><?php echo $employee->regot ?></td>

                  </tr>
                  <tr>
                    <td class= "print">6th Day OT</td>
                    <td class= "print">:</td>
                    <td class= "print"><?php echo $employee->ot6 ?></td>

                  </tr>
                  <tr>
                    <td class= "print">7th Day OT</td>
                    <td class= "print">:</td>
                    <td class= "print"><?php echo $employee->ot7 ?></td>

                  </tr>
                  <tr>
                    <td class= "print">Total ND</td>
                    <td class= "print">:</td>
                    <td class= "print"><?php echo $employee->totalnd ?></td>

                  </tr>
                
                  <?php if(!empty($adjustmentlogs)){ ?>
                    <?php foreach($adjustmentlogs as $adjustmentlog){ 
                        $ttlearn+=$adjustmentlog->amount;
                    ?>

                      <tr>
                        <td class="print"><?php echo $adjustmentlog->description ?></td>
                        <td class="print">:</td>
                        <td class="print"><?php echo $adjustmentlog->amount ?></td>
                      </tr>
                    <?php } ?>
                  <?php } ?>

                  <?php if(!empty($otherinclogs)){ ?>
                    <?php foreach ($otherinclogs as $otherinclog){ 
                        $ttlearn+=$otherinclog->amount;
                    ?>
                      <tr>
                        <td class="print"><?php echo $otherinclog->description ?></td>
                        <td class="print">:</td>
                        <td class="print"><?php echo $otherinclog->amount ?></td>
                      </tr>
                    <?php } ?>
                  <?php } ?>

                  <tr>
                    <td class="print no-bg"><strong>Total Earning</strong></td>
                    <td class="print no-bg">:</td>
                    <td class="print no-bg">
                      <strong>
                        <?php echo array_sum([$ttlearn,$employee->basicpay,$employee->holidaypay,$employee->regot,$employee->ot6,$employee->ot7,$employee->totalnd]) ?>
                      </strong>
                    </td>
                </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <div class="col-sm-6">
          <div class="card">
            <div class="card-header bg-primary bg-b"><h2>Payslip For Pay Period:</h2></div>
            <div class="card-body light-yellow-bg">  
              <form action="/payslip/show" method="post">
                <select name="period" class="custom-select" onchange="this.form.submit()">
                  <?php
                    foreach ($periods as $p) {
                      $selected = $p->payperiod == $employee->payperiod ? 'selected' : '';
                      echo "<option value='$p->payperiod' $selected>$p->payperiod</option>";
                    }
                  ?>
                </select>
              </form>
            </div>
          </div>
          
          <div class="card">
            <div class="card-header bg-primary bg-b"><h4>Deductions</h4></div>
            <div class="card-body">
              <table class="table bg-blue">
                <tbody>
                  <tr>
                    <td class= "print">Late/Absences/Others</td>
                    <td class= "print">:</td>
                    <td class= "print"><?php echo $employee->deductions ?></td>
                  </tr>
                  <tr>
                    <td class= "print">Loan Deduction</td>
                    <td class= "print">:</td>
                    <td class= "print"><?php echo $employee->loandeductions ?></td>
                  </tr>
                  <tr>
                    <td class= "print">With Holding Tax</td>
                    <td class= "print">:</td>
                     <td class= "print"><?php echo $employee->tax ?></td>
                  </tr>
                  <tr>
                    <td class= "print">SSS</td>
                    <td class= "print">:</td>
                    <td class= "print"><?php echo $employee->sss ?></td>

                  </tr>
                   <tr>
                    <td class= "print">PHIC</td>
                    <td class= "print">:</td>
                    <td class= "print"><?php echo $employee->phic ?></td>

                  </tr>
                   <tr>
                    <td class= "print">HDMF</td>
                    <td class= "print">:</td>
                    <td class= "print"><?php echo $employee->hdmf  ?></td>
                  </tr>

                  </tr>
                   <tr>
                    <td class= "print no-bg"><strong>Total Deduction</strong></td>
                    <td class= "print no-bg">:</td>
                    <td class= "print no-bg">
                      <strong>
                        <?php echo array_sum([$employee->deductions,$employee->loandeductions,$employee->tax,$employee->sss,$employee->phic,$employee->hdmf]) ?>
                      </strong>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="3">
                      <center>
                        <a class="btn btn-success" data-toggle="collapse" href="#multiCollapseExample2" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">Late/Absences/Other Details</a>
                      </center>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="3">
                      <div class="row">
                        <div class="col">
                          <div class="collapse multi-collapse" id="multiCollapseExample2">
                            <table class="bg-blue">
                              <tbody>
                                <?php if(!empty($adjustmentlogs2)){ ?>
                                  <?php foreach($adjustmentlogs2 as $adjustmentlog2){ ?>
                                    <tr>
                                      <td class="print"><?php echo $adjustmentlog2->description ?></td>
                                      <td class="print">:</td>
                                      <td class="print"><?php echo $adjustmentlog2->amount ?></td>
                                    </tr>
                                  <?php } ?>
                                <?php } ?>
                                <tr>
                                  <?php if(!empty($prlloansgovs)){ ?>
                                    <?php foreach($prlloansgovs as $prlloansgov){ ?>
                                      <td class="print"><?php echo $prlloansgov->loantype ?></td>
                                      <td class="print">:</td>
                                      <td class="print"><?php echo $prlloansgov->loanamt ?></td>
                                    <?php } ?>
                                  <?php } ?>
                                </tr>
                                <tr>
                                  <?php if(!empty($otherinclogs2)){ ?>
                                    <?php foreach($otherinclogs2 as $otherinclog2){ ?>
                                      <td class="print"><?php echo $otherinclog2->description ?></td>
                                      <td class="print">:</td>
                                      <td class="print"><?php echo $otherinclog2->amount ?></td>
                                    <?php } ?>
                                  <?php } ?>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>

          <div class="card">
            <div class="card-header bg-primary bg-b"><h4>Net Pay Summary</h4></div>
            <div class="card-body">
              <table class="table bg-blue">
                <tbody>
                  <tr>
                    <td class= "print">Total Earning</td>
                    <td class= "print">:</td>
                    <td class= "print">
                      <?php echo array_sum([$ttlearn,$employee->basicpay,$employee->holidaypay,$employee->regot,$employee->ot6,$employee->ot7,$employee->totalnd]) ?>
                    </td>
                  </tr>
                  <tr>
                    <td class= "print">Total Deduction</td>
                    <td class= "print">:</td>
                    <td class= "print">
                      <?php echo array_sum([$employee->deductions,$employee->loandeductions,$employee->tax,$employee->sss,$employee->phic,$employee->hdmf]) ?>
                      </td>
                  </tr>
                  <tr>
                    <td class= "print">Net Income</td>
                    <td class= "print">:</td>
                    <td class= "print"><strong><?php echo $employee->netpay ?></strong></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>  
</div>

<script>
function myFunction() {
  window.print();
}
</script>