<?php

  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Leave_type_model extends CI_Model{

	public function all(){
		$this->db->order_by('leave_name','ASC');
		$query = $this->db->get('leave_types');

		return $query->result();
	}


	public function create($leave){
		date_default_timezone_set('Asia/Kuala_Lumpur');

		$leave['createdttm'] = date('Y-m-d H:i:s');
		$leave['updatedttm'] = date('Y-m-d H:i:s');

		$this->db->insert('leave_types',$leave);
		return $this->db->affected_rows();
	}

}

