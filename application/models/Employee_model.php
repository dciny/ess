<?php

  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Employee_model extends CI_Model{

	public function get_employee_details($username){
		$this->db->select('*');
		$this->db->from('prlemployeemaster');
		$this->db->where('employeeid',$username);
		$query = $this->db->get();
		
		if($query->num_rows() > 0){
			return $query->row();
		}
	}

	public function is_active_user($username){
		$this->db->from('prlemployeemaster');
		$this->db->where('active', 0);
		$this->db->where('employeeid', $username);
		$numrows = $this->db->count_all_results();

		if($numrows > 0){
			return true;
		}else{
			return false;
		}

	}

	public function get_work_schedule($username){
		$this->db->select('sched.starttime,sched.endtime,sched.lunchstart,sched.lunchend,sched.mon,sched.tue,sched.wed,sched.thu,sched.fri,sched.sat,sched.sun');
		$this->db->from('prlemployeemaster as emp');
		$this->db->join('groupschedule as sched', 'sched.groupschedulename = emp.schedule', 'left');
		$this->db->where('employeeid',$username);
		$query = $this->db->get();
		
		if($query->num_rows() > 0){
			return $query->row();
		}
	}

	public function get_group_schedule(){
		$this->db->select('groupschedulename');
		$this->db->from('groupschedule');
		$query = $this->db->get();

		if($query->num_rows() > 0){
			return $query->result();
		}
		return null;
	}

	public function get_user_id($employeeid){
		$userids = $this->get_user_mapping($employeeid);
		$users = [];

		if(isset($userids)){
			foreach ($userids as $user) {
				array_push($users,$user['user_id']);
			}
			return $users;
		}
		return $userids;
	}

	public function get_employees($searchtxt=null, $limit=null, $quantity=null){
		$this->db->select('p.employeeid, p.firstname, p.lastname, la.leave_credits');
		$this->db->from('prlemployeemaster p');
		$this->db->join('leave_assignments la', 'la.employeeid = p.employeeid');
		$this->db->where('active',0);

		if(!empty($searchtxt)){
			$this->db->group_start()
					 ->where("CONCAT(p.firstname, ' ', p.lastname) LIKE '%". $searchtxt ."%'", NULL, FALSE)
					 ->or_where('p.employeeid',$searchtxt)
			         ->group_end();
		}

		$this->db->order_by('p.firstname');

		if(!empty($limit) && !empty($quantity))
			$this->db->limit($limit, $quantity);
		elseif(!empty($limit) && empty($quantity))
			$this->db->limit($limit);
		
		$query = $this->db->get();

		return $query->result();
		
	}

	private function get_user_mapping($employeeid){
		$this->db->select('user_id');
		$this->db->from('user_employee_mapping');
		$this->db->where('employeeid',$employeeid);
		$query = $this->db->get();

		if($query->num_rows() > 0){
			return $query->result_array();
		}
		return null;
	}
}

