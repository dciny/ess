<div class="container">
	<div class="row">
		<div class="col-md-12">
			<form action="<?php echo base_url('announcement/update') ?>" method="post">
				<input type="hidden" name="announcement[id]" value="<?php echo $announcement->id ?>">
				<div class="form-group">
					<label>Title</label>
					<input type="text" name="announcement[title]" class="form-control" value="<?php echo $announcement->title ?>">
				</div>
				<div class="form-group">
					<label>Body</label>
					<!-- <textarea class="wysiwyg" name="announcement[body]"></textarea> -->
					<textarea class="ckeditor5" name="announcement[body]"><?php echo $announcement->body ?></textarea>
				</div>
				<div class="d-flex" style="gap: 1rem">
					<input type="submit" class="btn btn-primary" value="Update">
					<span class="btn btn-secondary" onclick="window.history.back()">Cancel</span>
				</div>
			</form>
		</div>
	</div>
</div>