<?php
  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Policies extends CI_Controller {

	public function __construct(){

		parent::__construct();
		
		$this->load->model('employee_model');
		$this->load->helper('url');
	}

	
	public function _remap($method, $params = []){
		$login_session = $this->session->userdata('logged_in');

		if(isset($login_session)){
			
			$is_password_change =$this->session->userdata['logged_in']['is_password_change'];
			if($is_password_change == 0){
				redirect('auth/change-password', 'location');
			}

			$this->$method($params);
		}else{
			redirect('auth/index', 'location');
		}
	}


	public function index(){
		$username = $this->session->userdata['logged_in']['employeeid'];
 		$emp['employee'] = $this->employee_model->get_employee_details($username);
 		$emp['users'] = $this->employee_model->get_user_id($username);
 		$emp['header'] = 'FORMS / POLICIES';

		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Policies/index',$emp,'app');
	}
	

}
