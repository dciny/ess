<?php

  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Approver_model extends CI_Model{

	public function available_approvers(){
		$this->db->select('p.employeeid, p.firstname, p.lastname');
		$this->db->from('prlemployeemaster as p');
		$this->db->join('leave_approvers as la','p.employeeid = la.employeeid AND la.is_active = 1','left');
		$this->db->where('p.active',0);
		$this->db->where('la.employeeid IS NULL');
		$this->db->order_by('p.firstname', 'ASC');
		$query = $this->db->get();

		return $query->result();
	}

	public function approvers(){
		$this->db->select('la.id,la.employeeid, p.firstname, p.lastname');
		$this->db->from('leave_approvers as la');
		$this->db->join('prlemployeemaster as p','p.employeeid = la.employeeid','left');
		$this->db->where('is_active',1);
		$this->db->order_by('p.firstname', 'ASC');
		$query = $this->db->get();

		return $query->result();
	}

	public function is_exists($employeeid){
		$this->db->where('employeeid',$employeeid);
		// $this->db->where('is_active',1);
		$numrows = $this->db->count_all_results('leave_approvers');

		return $numrows > 0;
	}

	public function create($employeeid){
		date_default_timezone_set('Asia/Kuala_Lumpur');

		$data = array(
			'employeeid' => $employeeid,
			'is_active' => 1,
			'createdttm' => date('Y-m-d H:i:s'),
			'updatedttm' => date('Y-m-d H:i:s'),
		);

		$this->db->insert('leave_approvers',$data);
		return $this->db->affected_rows();
	}

	// will be called by update and delete function in controller
	public function update($employeeid,$is_active=1){
		date_default_timezone_set('Asia/Kuala_Lumpur');

		$data = array(
			'is_active' => $is_active,
			'updatedttm' => date('Y-m-d H:i:s'),
		);

		$this->db->where('employeeid',$employeeid);
		$this->db->update('leave_approvers',$data);
		return $this->db->affected_rows();
	}

	public function details($employeeid){
		$query = $this->db->from('approvers')
				          ->where('employeeid',$employeeid)
				          ->get();
	    return $query->row();
	}

}

