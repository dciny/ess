<?php
  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Leave_assignment extends CI_Controller {

	public function __construct(){

		parent::__construct();
		
		$this->load->model('leave_hierarchy_model','leave_hierarchy');
		$this->load->model('leave_type_model','leave_type');
		$this->load->model('leave_assignment_model','leave_assignment');
		$this->load->helper('url');
	}

	
	public function new(){
		$data['ind'] = 'LA'; // used in the sidebar of the layout
		$data['leave_hierarchies'] = $this->leave_hierarchy->all();

		// check if request data is from post/session/no data at all
		$lhid = $this->input->post('leave_hierarchy_id');
		$lhid = isset($lhid) && !empty($lhid) ? $lhid : $this->session->flashdata('leave_hierarchy_id');
		$lhid = isset($lhid) && !empty($lhid) ? $lhid : $data['leave_hierarchies'][0]->id;
		$data['lhid'] = $lhid;
		
		$data['members'] = $this->leave_assignment->get_membersby_leavehierarchy($lhid);
		$data['unassigned_members'] = $this->leave_assignment->get_unassigned_employee();
		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Leave_assignment/new',$data,'leave');
	}

	public function create(){
		$leave_hierarchy_id = $this->input->post('leave_hierarchy_id');
		$employeeid = $this->input->post('employeeid');

		$data['leave_hierarchy_id'] = $leave_hierarchy_id;
		$data['employeeid'] = $employeeid;
		
		$this->leave_assignment->insert_or_update($data);
		$this->session->set_flashdata('leave_hierarchy_id', $leave_hierarchy_id);
		redirect(base_url('leave-assignment/new'),'location');
	}

	public function delete(){
		$leave_hierarchy_id = $this->input->post('leave_hierarchy_id');
		$employeeid = $this->input->post('employeeid');

		$data['leave_hierarchy_id'] = $leave_hierarchy_id;
		$data['employeeid'] = $employeeid;

		$this->leave_assignment->delete($data);
		$this->session->set_flashdata('leave_hierarchy_id', $leave_hierarchy_id);
		redirect(base_url('leave-assignment/new'),'location');
	}

	public function fill_unassigned_emp(){
		$leave_type_id = $this->input->post('leave_type_id');

		$result = $this->leave_assignment->unassigned($leave_type_id);
		$json_obj = json_encode($result);

		header('Content-Type: application/json');
		echo $json_obj;
	}

}
