<div class="main container-fluid">
	<div class="row mb-5">
		<div class="col-md-12">
			<div class="ml-5">
				<a class="cta-def-btn" href="<?php echo base_url('employee/show'); ?>">
					<span class="back-icon">Back</span>
				</a>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 p-0">
			<div class="mx-5">
				<p class="text-danger">* Send change request to HR</p>
			</div>
			<div class="box cyan-lin-bg box-min-size mx-5">
				<h5>Work Details</h5>
				<form action="/request/work/create" method="post">
					<div class="table-responsive">
						<table class="table">
							<tbody>
								<tr>
									<td style="width: 120px">Position:</td>
									<td>
										<input type="text" 
										       name="position" 
											   class="form-control <?php if(form_error('position')) echo 'is-invalid' ?>" 
											   value="<?php echo $employee->position ?>">
										<?php echo form_error('position'); ?>
									</td>
								</tr>
								<tr>
									<td style="width: 120px">Schedule:</td>
									<td>
										<select class="custom-select" name="schedule">
											<?php 
												foreach ($groupscheds as $groupsched){
													$selected = $groupsched->groupschedulename == $employee->schedule ? 'selected' : '';
													echo "<option value='$groupsched->groupschedulename' $selected> $groupsched->groupschedulename </option>";
												} 
											?>
										</select>
									</td>
								</tr>
								<tr class="text-right">
									<td colspan="2">
										<input type="submit" name="submit" value="Send Change Request" class="btn cta-btn cyan-bg-btn">
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>