<div class="container-fluid">

	<div class="row mb-4">
		<div class="col-md-12">
			<div class="ml-4">
				<a class="cta-def-btn" href="<?php echo base_url(); ?>">
					<span class="back-icon">Back to home</span>
				</a>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="card border p-3">

				<div class="border-bottom mb-3">
					<ul class="d-flex m-0">
						<li class="p-4"><a href="<?php echo base_url('leave') ?>">Dashboard</a></li>
						<li class="p-4"><a href="<?php echo base_url('leave/apply') ?>">Apply Leave</a></li>
						<?php if($user_details->leave_role_id == 1){ ?>
							<li class="p-4 leave-tab-active">Leave Delegate</li>
						<?php } ?>
						<?php if($is_approver){ ?>
							<li class="p-4"><a href="<?php echo base_url('leave/requests') ?>">Leave Requests</a></li>
						<?php } ?>
						<?php if($user_details->leave_role_id == 1){ ?>
							<li class="p-4"><a href="<?php echo base_url('leave/tracker') ?>">Leave Tracker</a></li>
						<?php } ?>
						<?php if($user_details->leave_role_id == 1){ ?>
							<li class="p-4"><a href="<?php echo base_url('leave/monthly-report/approved') ?>">Monthly Tracker</a></li>
						<?php } ?>
					</ul>
				</div>

				<form action="" method="get">
					<div class="row">
						<div class="col-md-3 mb-3">
							<input type="text" name="stxt" placeholder="Search for name, employeeid" class="form-control" value="<?php echo !empty($stxt) ? $stxt : '' ?>">
						</div>
						<div class="col-md-6">
							<input type="submit" value="Search" class="btn btn-primary">
						</div>
					</div>
				</form>
				<div class="row mt-3">
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table border">
								<thead class="thead-dark border" style="border-color: #5a5c69 !important;">
									<tr class="text-center">
										<th>Employeeid</th>
										<th>First Name</th>
										<th>Last Name</th>
										<th>Leave Balance</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									<?php if(count($employees)){ ?>
										<?php foreach ($employees as $employee) { ?>
											<tr class="text-center">
												<td><?php echo $employee->employeeid ?></td>
												<td><?php echo $employee->firstname ?></td>
												<td><?php echo $employee->lastname ?></td>
												<td><?php echo $employee->leave_credits ?></td>
												<td><a class="btn btn-success" href="<?php echo base_url('leave/employee-calendar/'. $employee->employeeid) ?>">Apply Leave</a></td>
											</tr>
										<?php } ?>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="row mt-3">
					<div class="col-md-12">
						<div class="pagination-link">
							<?php echo $this->pagination->create_links(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>