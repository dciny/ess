<?php
  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Approver extends CI_Controller {

	public function __construct(){

		parent::__construct();
		
		$this->load->model('approver_model');
		$this->load->model('approver_hierarchy_model', 'approver_hierarchy');
		$this->load->helper('url');
		$this->load->library('session');
	}

	
	public function index(){
		$data['ind'] = 'AP'; // used in the sidebar of the layout
		$data['approvers'] = $this->approver_model->approvers();
		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Approver/index',$data,'leave');
	}

	public function show(){
		$data['ind'] = 'AP'; // used in the sidebar of the layout
		$data['avail_approvers'] = $this->approver_model->available_approvers();
		$data['approvers'] = $this->approver_model->approvers();
		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Approver/show',$data,'leave');
	}

	public function create(){
		$employeeid = $this->input->post('employeeid');

		if($this->approver_model->is_exists($employeeid)){
			$this->update();
		}else{
			$this->approver_model->create($employeeid);
		}

		redirect(base_url('approver/show'), 'location');
	}

	public function update(){
		$employeeid = $this->input->post('employeeid');

		if($this->approver_model->is_exists($employeeid)){
			$this->approver_model->update($employeeid);
		}else{
			$this->create();
		}

		redirect(base_url('approver/show'), 'location');
	}

	public function delete(){
		$employeeid = $this->input->post('employeeid');

		$is_assigned_approver = $this->approver_hierarchy->is_approver($employeeid);

		if($is_assigned_approver){
			//add flash data 
         	$this->session->set_flashdata('error','This approver is still assigned at least one leave hierarchy');
         	redirect(base_url('approver/show'), 'location');
		}

		if($this->approver_model->is_exists($employeeid)){
			$this->approver_model->update($employeeid,0); // set is_active to 0
		}

		redirect(base_url('approver/show'), 'location');
	}

}
