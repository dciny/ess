<?php
  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sendmail extends CI_Controller {

	public function __construct(){

		parent::__construct();
		
		$this->load->model('leave_emaillogs_model','leave_emaillogs');
		$this->load->model('sendmail_model');
		$this->load->library('email');
	}

	
	public function sendmail_leave(){
		$to_email = $this->leave_emaillogs->get_mail_logs();

		$to_approver = $to_email[0]->to_approver;

		$email_data = [];
		foreach ($to_email as $email) {
			if($to_approver != $email->to_approver){
				$this->send_email($to_approver, $email_data); // send email
				$to_approver = $email->to_approver; // change approver
				$email_data = []; // reset email data
			}
			$row = array(
							'id' => $email->id,
							'requestor_id'=> $email->requestor_id,
						 	'requestor_name'=> $email->requestor_name,
						 	'leave_dt' => $email->leave_dt,
						 	'leave_category' => $email->leave_category,
						 	'leave_type' => $email->leave_type
						);

			array_push($email_data, $row);
		}

		$this->send_email($to_approver, $email_data); // send email for last item
	}
	
	public function send_email($to_approver, $email_data){
		$mail_template = $this->sendmail_model->template();
		$mail_header = $this->sendmail_model->leave_approval_header();
		$mail_body = $this->sendmail_model->leave_approval_body($email_data);

		$mail_content = str_replace('{mail_body}',$mail_body,str_replace('{mail_header}', $mail_header, $mail_template));

		// set email configurations
		$mail_config = $this->sendmail_model->config();
		$this->email->initialize($mail_config);
		$this->email->set_newline("\r\n");
		$this->email->set_crlf( "\r\n" );

		$this->email->from($mail_config['smtp_user'], 'ESS');
		$this->email->to($to_approver);

		$this->email->subject('REQUEST: Leave Approval');
		$this->email->message($mail_content);

		if($this->email->send()){
			foreach ($email_data as $email) {
				$this->leave_emaillogs->tag_mail_as_sent($email['id']);
			}
		}
	}
}
