<?php

  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Leave_hierarchy_model extends CI_Model{

	public function leave_assignments($hierarchy_id){
		return $this->db->from('leave_assignments')
		                ->where('leave_hierarchy_id',$hierarchy_id)
		                ->where('is_active',1);
	}

	public function create($data){
		$this->db->insert('leave_hierarchies',$data);
		if($this->db->affected_rows() > 0)
			return $this->db->insert_id();
		else
			return false;
	}

	public function all(){
		$this->db->order_by('rule_name','ASC');
		$query = $this->db->get('leave_hierarchies'); 
		
		return $query->result();
	}

	public function find($id){
		$this->db->where('id',$id);
		$query = $this->db->get('leave_hierarchies');

		return $query->row();
	}

	public function update($id,$data){
		$this->db->where('id',$id);
		$this->db->update('leave_hierarchies',$data);
		if($this->db->affected_rows() > 0)
			return true;
		return false;
	}

	public function delete($id){
		$this->db->delete('leave_hierarchies',array('id' => $id));

		if($this->db->affected_rows() > 0)
			return true;
		return false;
	}

	public function unassigned_leave($employeeid){
		$this->db->from('leaves_types as lt');
		$this->db->join('leave_assignments as la',"la.leave_type_id = lt.id AND la.employeeid = '$employeeid'",'left');
		$this->db->where('la.id IS NULL');
		$query = $this->db->get();

		return $query->result();
	}

}

