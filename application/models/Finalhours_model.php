<?php

  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Finalhours_model extends CI_Model{

	public function get_year(){
		// $this->db->select_min('shiftdate');
		// $query = $this->db->get('finalhourstable')->row();

		// $min_date = $query->shiftdate;
		// $year = date('Y',strtotime($min_date));

		// return $year;
		$q = "SELECT DISTINCT YEAR(shiftdate) as year FROM finalhourstable";
		$query = $this->db->query($q);

		return $query->result();
	}

	
	public function get_schedule($employeeid, $searchStart, $searchEnd){
		// $this->db->select('shiftdate, userid, SUM(regular_hours) as sum_reg, SUM(nightdiff_hours) as sum_night, SUM(regular_ot_hours) as sum_ot, SUM(late) as sum_late, SUM(undertime) as sum_under, status,',false);
		// $this->db->from('finalhourstable');
		// $this->db->where('userid',$employeeid);
		// $this->db->where('shiftdate >=',$searchStart);
		// $this->db->where('shiftdate <=',$searchEnd);
		// $this->db->group_by(array('shiftdate', 'userid', 'status'));
		// $query = $this->db->get();

		// return $query->result();

		$q = "select fin.shiftdate,
			         fin.userid,
			       	 fin.sum_reg,
			         fin.sum_night,
			         fin.sum_ot,
			         fin.sum_late,
			         fin.sum_under,
			         fin.status,
			         t.shiftday,
			         t.startshift,
			         t.endshift,
			         t.otstart,
			         t.otend
		      from
			  (
			    select f.shiftdate,
			           f.userid,
			       	   sum(f.regular_hours) as sum_reg,
			           sum(f.nightdiff_hours) as sum_night,
			           sum(f.regular_ot_hours) as sum_ot,
			           sum(f.late) as sum_late,
			           sum(f.undertime) as sum_under,
			           f.status
			    from finalhourstable f
			    where f.userid = $employeeid
			    and shiftdate between '$searchStart' and '$searchEnd'
			    group by shiftdate, userid, status
			  ) fin
			  left join timelog t on t.userid = fin.userid and t.dates = fin.shiftdate";

		$query = $this->db->query($q);

		return $query->result();

	}

}