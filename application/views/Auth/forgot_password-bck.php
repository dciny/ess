<?php

	$msg = '';
	$border_color = '';
	$box_color = '';

	if($this->session->flashdata('error')){
		$msg = $this->session->flashdata('error');
		$border_color = 'border-danger';
		$box_color = 'msg-box-danger';
	}elseif($this->session->flashdata('success') != null){
		$msg = $this->session->flashdata('success');
		$border_color = 'border-success';
		$box_color = 'msg-box-success';
	}

?>


<div class="box">
	<nav class="navbar navbar-expand-sm bg-primary navbar-dark">
		<ul class="navbar-nav">
			<li class="nav-item">
				<a class="nav-link active" href="<?php echo base_url() ?>">Home</a>
			</li>
			<li class="nav-item">
				<a class="nav-link active" href="mailto:hr@digicononline.com">Contact HR</a>
			</li>
		</ul>
	</nav>

	<div class="container-fluid">

		<?php if(!empty($msg)){ ?>
			<div class="row">
				<div class="col-md-4">
					<div class="px-4 py-4 font-14">
						<div class="msg-box <?php echo $box_color ?> border-left border-10 <?php echo $border_color ?>">
							<p class="m-0"><?php echo $msg ?></p>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>


		<div class="row">
			<div class="col-md-4">
				<div class="px-4 py-4 font-14">
					<p>
						Enter your <strong>user id</strong> / <strong>employee id</strong>. Your password will be sent to your registered email and to the HR group email.
					</p>
					<form class="jp-reset" action="<?php echo base_url('auth/password-send') ?>" method="post">
						<div class="input-group mb-3">
							<input type="number" name="username" class="form-control font-14" placeholder="Employee id">
							<div class="input-group-append">
								<button class="btn btn-primary font-14">Reset</button>
							</div>
						</div>
					</form>
					<p>If you forgot your registered email, you can reach out to <a href="mailto:hr@digicononline.com">hr@digicononline.com</a> to verify the password reset action that will be sent to the email.</p>
					<p class="text-danger"><strong>Note:</strong> The email may be sent to your SPAM folder.</p>
				</div>
			</div>
		</div>

	</div>
</div>


<div class="spinner-wrapper">
	<div class="spinner-content">
		<span class="text-light">Processing. Please wait.</span>
		<div>
			<div class="spinner-grow text-warning spinner-grow-sm"></div>
			<div class="spinner-grow text-warning spinner-grow-sm"></div>
			<div class="spinner-grow text-warning spinner-grow-sm"></div>
		</div>
	</div>
</div>