<div class="row mb-3">
	<div class="col-md-11 mx-auto">
		<div class="d-flex announce-btn-options" style="gap: 1rem">
			<?php if($user->leave_role_id == 1){ ?>
			<a href="<?php echo base_url('announcement/new') ?>" class="btn btn-success">
				<small>
					<span class="fa fa-plus"></span>
					<span>&nbsp;New Announcement</span>
				</small>
			</a>
			<a href="<?php echo base_url('announcement/all') ?>" class="btn bg-navy-blue">
				<small>
					<span class="fa fa-bullhorn"></span>
					<span>&nbsp;See All</span>
				</small>
			</a>
			<?php } ?>
			<a href="<?php echo base_url('/') ?>" class="btn bg-navy-blue">
				<small>
					<span class="fa fa-dashboard"></span>
					<span>&nbsp;Back to Dashboard</span>
				</small>
			</a>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-11 mx-auto">
		<div class="card p-3 announce-page">
			<div class="row">
				<?php if(!empty($latest_announcement)){ ?>
					<div class="col-md-9" id="announce-highlight">
						<div class="border">
							<div class="announce-head pl-3 pt-3">
								<h1 class="text-dark"><strong class="text-dark"><?php echo ucwords($latest_announcement->title) ?></strong></h1>
								<em class="text-muted"><?php echo date('F d, Y h:i A',strtotime($latest_announcement->createdttm)) ?></em>
							</div>
							<div class="announce-body pl-3 pb-3 pr-3">
								<div class="text-dark mt-3">
									<?php echo $latest_announcement->body ?>
								</div>
							</div>
						</div>
						<?php if($user->leave_role_id == 1){ ?>
							<div class="d-flex mt-3" style="gap: 1rem">
								<div>
									<a href="<?php echo base_url('announcement/'.$latest_announcement->id.'/edit') ?>" class="btn btn-secondary">
										<small>
											<span class="fa fa-edit"></span>
											<span>Edit</span>
										</small>
									</a>
								</div>
								<div>
									<a href="<?php echo base_url('announcement/'.$latest_announcement->id.'/destroy') ?>" class="btn btn-danger del-announcement" data-redirect="<?php echo base_url('announcement/all/') ?>">
										<small>
											<span class="fa fa-remove"></span>
											<span>Delete</span>
										</small>
									</a>
								</div>
							</div>
						<?php } ?>
					</div>
				<?php } ?>
				<div class="col-md-3">
					<div class="announce-list announce-list-bg">
						<ul class="list-group-flush1 announce-list-bg">
							<li class="list-group-item1 announce-list-bg pt-0"><h5><strong class="text-danger">Announcements</strong></h5></li>
							<?php if(count($announcements)) { ?>
								<?php foreach ($announcements as $announcement) { ?>
									<?php 
										if($announcement->id == $current){
											$item_class = "announce-item active";
										}else{
											$item_class = "announce-item";
										}
									?>
									<li class="list-group-item1 announce-list-bg <?php echo $item_class ?>">
										<a href="<?php echo base_url('announcement/display/'.$announcement->id) ?>">
											<div>
												<strong><?php echo ucwords($announcement->title) ?></strong>
												<div><small class="text-muted"><?php echo date('F d, Y h:i A',strtotime($announcement->createdttm)) ?></small></div>
												<div class="ellipsis"><small><?php echo strip_tags($announcement->body) ?></small></div>
											</div>
										</a>
									</li>
								<?php } ?>
							<?php }else{ ?>
								<li class="list-group-item1 announce-list-bg">No announcement created yet</li>
							<?php } ?>
						</ul>
					</div>	
				</div>
			</div>
		</div>
	</div>
</div>