<?php

  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Employee_work_model extends CI_Model{

	public function is_exists($employeeid){
		$this->db->select('*');
		$this->db->from('req_employee_work');
		$this->db->where('employeeid',$employeeid);
		$this->db->where('status','For approval');
		$query = $this->db->get();

		if($query->num_rows() > 0){
			$data = $query->row();
			return $data->id;
		}
		return false;
	}


	public function get_work_details($id){
		$this->db->select('*');
		$this->db->from('req_employee_work');
		$this->db->where('id',$id);
		$query = $this->db->get();

		if($query->num_rows() > 0){
			return $query->row();
		}
		return null;
	}

	public function get_all_work_details($employeeid){
		$this->db->select('*');
		$this->db->from('req_employee_work');
		$this->db->where('employeeid',$employeeid);
		$query = $this->db->get();

		if($query->num_rows() > 0){
			return $query->result();
		}
		return null;
	}

	public function is_authorized($id,$employeeid){
		$this->db->select('employeeid');
		$this->db->from('req_employee_work');
		$this->db->where('id',$id);
		$query = $this->db->get();

		if($query->num_rows() > 0){
			$data = $query->row();
			if($data->employeeid == $employeeid)
				return true;
		}
		return false;
	}

	public function get_group_schedule(){
		$this->db->select('groupschedulename');
		$this->db->from('groupschedule');
		$query = $this->db->get();

		if($query->num_rows() > 0){
			return $query->result();
		}
		return null;
	}

}

