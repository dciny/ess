<div class="calendar-container">
	<div class="calendar-header yellow-lin-bg">
		<span class="fa fa-angle-left prev" data-date="<?php echo $prevmo_first_day ?>"></span>
		<span class="fa fa-angle-right next" data-date="<?php echo $nxtmo_first_day ?>"></span>
		<h5 id="month"><?php echo date('F',strtotime($curr_date)) ?></h5>
		<h6 id="date" data-date="<?php echo $curr_date ?>"><?php echo date('D F d Y',strtotime($curr_date)) ?></h6>
	</div>	
	<div id="calendar-grid">
		<?php 
			// print calendar day headers
			foreach ($days as $day){
				echo "<div class='p-3 text-center'>$day</div>";
			}

			// print previous month days
			if($prevmo_no_days > 0){
				for($prev = 0; $prev < $prevmo_no_days; $prev++){
					$prev_day = date('d', strtotime("$prev days", strtotime($prevmo_strt_day)));
					$prev_dt = date('Y-m-d', strtotime("$prev days", strtotime($prevmo_strt_day)));
					$dt_arr = getdate(strtotime($prev_dt));
					$dt_idx = $dt_arr['wday'];
					$cal_clickable = ($dt_idx > 0 && $dt_idx < 6) && !in_array($prev_dt,$leaves) && !in_array($prev_dt,$holidays_arr) ? 'cal-clickable' : '';
					$dt_selected = in_array($prev_dt, $seldt) ? 'dt-selected' : '';
					$applied = in_array($prev_dt,$leaves) ? 'Applied' : '';
					echo "<div class='$cal_clickable $dt_selected cal-border not-current c-days text-center' data-val='$prev_dt'><span class='d-num'>$prev_day</span><span class='dt-note'>$applied</span></div>";
				}
			}
			// print current month days
			$curr_cal_day = date('Y-m-d');
			for ($curr=1; $curr <= $currdt_no_days; $curr++) { 
				$cur_day = substr(implode('',['0',$curr]),-2);
				$curr_mo_dt = date("Y-m-$cur_day",strtotime($curr_date));
				$dt_arr = getdate(strtotime($curr_mo_dt));
				$dt_idx = $dt_arr['wday'];
				$current = ($curr_cal_day == $curr_mo_dt) ? 'current' : '';
				$cal_clickable = ($dt_idx > 0 && $dt_idx < 6) && !in_array($curr_mo_dt, $leaves) && !in_array($curr_mo_dt, $holidays_arr) ? 'cal-clickable' : '';
				$dt_selected = in_array($curr_mo_dt, $seldt) ? 'dt-selected' : '';
				$applied = in_array($curr_mo_dt,$leaves) ? 'Applied' : '';
				echo "<div class='$current $cal_clickable $dt_selected cal-border c-days text-center' data-val='$curr_mo_dt'><span class='d-num'>$curr</span><span class='dt-note'>$applied</span></div>";
			}

			// print next month days
			if($curr_last_day_idx < 6){
				for($nxt = 1; $nxt <= $nxtmo_no_days; $nxt++){
					$nxt_day = substr(implode('',['0',$nxt]),-2);
					$nxt_mo_dt = date("Y-m-$nxt_day",strtotime($nxtmo_first_day));
					$dt_arr = getdate(strtotime($nxt_mo_dt));
					$dt_idx = $dt_arr['wday'];
					$cal_clickable = ($dt_idx > 0 && $dt_idx < 6) && !in_array($nxt_mo_dt, $leaves) && !in_array($nxt_mo_dt, $holidays_arr) ? 'cal-clickable' : '';
					$dt_selected = in_array($nxt_mo_dt, $seldt) ? 'dt-selected' : '';
					$applied = in_array($nxt_mo_dt,$leaves) ? 'Applied' : '';
					echo "<div class='$cal_clickable $dt_selected cal-border not-current c-days text-center' data-val='$nxt_mo_dt'><span class='d-num'>$nxt</span><span class='dt-note'>$applied</span></div>";
				}
			}
		?>
	</div>
</div>