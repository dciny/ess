







<div class="border mx-4 leave-dashboard">
	<div class="border-bottom">
		<ul class="d-flex m-0">
			<li class="p-4"><a href="<?php echo base_url('leave') ?>">Dashboard</a></li>
			<li class="p-4"><a href="<?php echo base_url('leave/apply') ?>">Apply Leave</a></li>
			<?php if($user_details->leave_role_id == 1){ ?>
				<li class="p-4"><a href="<?php echo base_url('leave/delegate') ?>">Leave Delegate</a></li>
			<?php } ?>
			<?php if($is_approver){ ?>
				<li class="p-4"><a href="<?php echo base_url('leave/requests') ?>">Leave Requests</a></li>
				<li class="p-4 leave-tab-active">Leave Count</li>
			<?php } ?>
			<?php if($user_details->leave_role_id == 1){ ?>
				<li class="p-4"><a href="<?php echo base_url('leave/tracker') ?>">Leave Tracker</a></li>
			<?php } ?>
			<?php if($user_details->leave_role_id == 1){ ?>
				<li class="p-4"><a href="<?php echo base_url('leave/monthly-report/approved') ?>">Monthly Tracker</a></li>
			<?php } ?>
		</ul>
	</div>
	<form class="p-4" action="<?php echo base_url('leave/approver/leave-count-page-submit') ?>" method="get" id="hierarchy-form">
		<div class="row">
			<div class="col-md-5">
				<div class="row">
					<div class="col-md-5">
						<div class="form-group">
							<label><strong>From</strong></label>
							<input type="date" name="from" class="form-control" required>
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<label><strong>To </strong>(Optional)</label>
							<input type="date" name="to" class="form-control">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-10">
						<div id="accordion">
							<?php foreach ($grouped_approvees as $key => $approvees){ ?>
								<?php $id = uniqid() ?>
								<div class="card">
									
									<div class="card-header">
										<div class="d-flex">
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input hierachy-checkbox" id="<?php echo $id ?>" checked>
												<label class="custom-control-label" for="<?php echo $id ?>"></label>
											</div>

											<a class="card-link" data-toggle="collapse" href="#item-<?php echo $id ?>">
												<?php echo $key  ?>
											</a>
										</div>	
									</div>

									<div id="item-<?php echo $id ?>" class="collapse" >
										<div class="card-body p-0">
											<ul class="list-group list-group-flush1">
												<?php foreach ($approvees as $approvee){ ?>
													<?php $id_name = uniqid() ?>
													<li class="list-group-item1 py-1">
														<div class="d-flex">
															<div class="custom-control custom-checkbox">
																<input type="checkbox" class="custom-control-input" id="<?php echo $id_name ?>" name="employeeids[]" value="<?php echo $approvee->employeeid ?>" checked>
																<label class="custom-control-label" for="<?php echo $id_name ?>"></label>
															</div>
															<?php echo implode(', ',[$approvee->lastname,$approvee->firstname]) ?>
														</div>
													</li>
												<?php } ?>
											</ul>
										</div>
									</div>

								</div>
							<?php } ?>
						</div>
					</div>
				</div>
				<div class="row my-3">
					<div class="col-md-8">
						<input type="submit" class="btn btn-sm btn-primary" value="Submit">
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div id="table-count">
					<!-- will be populated using jquery -->
				</div>
			</div>	
		</div>
	</form>
</div>