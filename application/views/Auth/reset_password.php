<div class="box">
	<nav class="navbar navbar-expand-sm bg-primary navbar-dark">
		<ul class="navbar-nav">
			<li class="nav-item">
				<a class="nav-link active" href="<?php echo base_url() ?>">Home</a>
			</li>
			<li class="nav-item">
				<a class="nav-link active" href="mailto:hr@digicononline.com">Contact HR</a>
			</li>
		</ul>
	</nav>
	<div class="row">
		<div class="col-md-4">
			<div class="px-4 py-4 font-14">
				<?php echo $msg ?>

				<br>

				<?php if(isset($username)){ ?>

					<table class="table table-bordered">
						<tr>
							<td>Username:</td>
							<td><?php echo $username ?></td>
						</tr>
						<tr>
							<td>Password:</td>
							<td><?php echo $password ?></td>
						</tr>
					</table>
					<p class="text-danger"><strong>Note:</strong> Reloading this page will hide this details above. Please make sure to login to ess and update your password</p>
				<?php } ?>
			</div>
		</div>
	</div>
</div>