<div class="border mx-4 leave-dashboard">
	<div class="border-bottom">
		<ul class="d-flex m-0">
			<li class="p-4"><a href="<?php echo base_url('leave') ?>">Dashboard</a></li>
			<li class="p-4"><a href="<?php echo base_url('leave/apply') ?>">Apply Leave</a></li>
			<?php if($user_details->leave_role_id == 1){ ?>
				<li class="p-4"><a href="<?php echo base_url('leave/delegate') ?>">Leave Delegate</a></li>
			<?php } ?>
			<?php if($is_approver){ ?>
				<li class="p-4 leave-tab-active">Leave Requests</li>
				<li class="p-4"><a href="<?php echo base_url('leave/approver/leave-count-page') ?>">Leave Count</a></li>
			<?php } ?>
			<?php if($user_details->leave_role_id == 1){ ?>
				<li class="p-4"><a href="<?php echo base_url('leave/tracker') ?>">Leave Tracker</a></li>
			<?php } ?>	
			<?php if($user_details->leave_role_id == 1){ ?>
				<li class="p-4"><a href="<?php echo base_url('leave/monthly-report/approved') ?>">Monthly Tracker</a></li>
			<?php } ?>	
		</ul>
	</div>

	<div class="p-3 font-14">

		
			<div class="table-responsive">	

				<ul class="nav nav-tabs mb-3">
					<li class="nav-item">
						<a class="nav-link active">Requires Approval</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?php echo base_url('leave/requests_completed') ?>">Completed</a>
					</li>
				</ul>

				<div class="row">
					<div class="col-md-6 mb-2">
						<div class="d-flex">
							<select class='form-control form-control-sm mr-2' name="leave_status_id">
								<?php
									foreach ($leave_statuses as $status) {
										echo "<option value='$status->id'>$status->status_name</option>";
									}
								?>
							</select>
							<button class="btn btn-sm btn-primary" id="bulk-req-btn">Submit</button>
						</div>
					</div>

					<div class="col-md-6 mb-2">
						<form action="<?php echo base_url('leave/requests') ?>" method="get">
							<div class="input-group mb-3">
								<input type="text" name="searchtxt" class="form-control form-control-sm" placeholder="Employee Name" value="<?php echo $searchtxt ?>">
								<div class="input-group-append">
									<input type="submit" class="btn btn-sm btn-secondary" value="Search">
								</div>
							</div>
						</form>
					</div>
				</div>

				<table class="table table-sm table-striped">
					<thead class="thead-dark">
						<tr>
							<th class='align-middle' width="10px"><input type="checkbox" name="selectall"></th>
							<th class='align-middle' width="70px">Employeeid</th>
							<th class='align-middle'>Requestor</th>
							<th class='align-middle' width="100px">Leave Date</th>
							<th class='align-middle' width="100px">Date Created</th>							
							<th class='align-middle' width="150px">Leave Type</th>
							<th class='align-middle'>Leave Category</th>
							<th class='align-middle' width="400px">Reason</th>
							<th class='align-middle' colspan="2">Action</th>
						</tr>
					</thead>
					<tbody id="approve-req-tbl">
						<?php foreach ($requests as $request){
							$name = $request->firstname . ' ' . $request->lastname;
						?>

							<tr>
								<td class='align-middle'>
									<?php if($request->approvable && empty($request->rank_status)){ ?>
										<input type='checkbox'>
									<?php } ?>
								</td>
								<td class='align-middle'><?php echo $request->employeeid ?></td>
								<td class='align-middle'><span data-toggle="tooltip" title='<?php echo "Balance: $request->leave_balance | Used: $request->leaves_taken"  ?>'><?php echo $name ?></span></td>
								<td class='align-middle'><a href="<?php echo base_url("leave/show/$request->id") ?>"><?php echo $request->leavedt ?></a></td>
								<td class='align-middle'><?php echo date('M d, Y h:i A',strtotime($request->createdttm)) ?></td>
								<td class='align-middle'><?php echo $request->leave_name ?></td>
								<td class='align-middle'><?php echo ucwords($request->leave_category) ?></td>
								<td class='align-middle'><?php echo $request->leave_reason ?></td>
								<?php if($request->approvable && empty($request->rank_status)){ ?>
									<td>
										<select class='form-control form-control-sm' name="leave_status_id" form="form-<?php echo $request->id ?>">
											<?php
												foreach ($leave_statuses as $status) {
													echo "<option value='$status->id'>$status->status_name</option>";
												}
											?>
										</select>
									</td>
									<td class='align-middle'>
										<form id="form-<?php echo $request->id ?>" action="<?php echo base_url('leave-request-status/create') ?>" method="post">
											<input type="hidden" name="leave_request_id" value="<?php echo $request->id ?>">
											<input type="hidden" name="approver_rank" value="<?php echo $request->approver_rank ?>">
											<input type="hidden" name="leave_category" value="<?php echo $request->leave_category ?>">
											<input type="hidden" name="employeeid" value="<?php echo $request->employeeid ?>">
											<button class='btn btn-primary btn-sm'>Submit</button>
										</form>
									</td>
								<?php 
									}elseif(!empty($request->rank_status)){
										echo "<td colspan='2'><span>$request->rank_status</span></td>";
									}else{
										echo "<td colspan='2'>No action</td>";
									} 

								?>
							</tr>

						<?php } ?>
					</tbody>
				</table>
				<div class="pagination-link">
					<?php echo $this->pagination->create_links(); ?>
				</div>
			</div>
	</div>
</div>	
