<?php

  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Leave_request_model extends CI_Model{

	public function myleaves($employeeid){
		$this->db->select('lr.id, lr.leavedt, lt.leave_name, lr.leave_category, ls.status_name');
		$this->db->from('leave_requests lr');
		$this->db->join('leave_types lt','lt.id = lr.leave_type_id','left');
		$this->db->join('leave_statuses ls','ls.id = lr.overall_status','left');
		$this->db->where('employeeid',$employeeid);
		$this->db->order_by('lr.leavedt','desc');
		$query = $this->db->get();

		return $query->result();
	}

	public function leavedates($employeeid){
		$this->db->select('lr.leavedt');
		$this->db->from('leave_requests lr');
		$this->db->where('employeeid',$employeeid);
		$query = $this->db->get();

		return $query->result();
	}


	public function leavedates_arr($employeeid){
		$this->db->select('lr.leavedt');
		$this->db->from('leave_requests lr');
		$this->db->where('employeeid',$employeeid);
		$this->db->where_in('overall_status',[3,5]);
		$query = $this->db->get();

		$leaves = [];
		foreach ($query->result() as $result) {
			array_push($leaves, $result->leavedt);
		}

		return $leaves;
	}

	public function total_leaves($employeeid){
		$this->db->where('employeeid',$employeeid);
		$this->db->where_in('overall_status',[3,5]);
		$this->db->from('leave_requests');
		
		return $this->db->count_all_results();
	}

	public function details($leave_request_id){
		$this->db->select('lr.id, lr.leavedt, lt.leave_name, ls.status_name, lr.employeeid');
		$this->db->from('leave_requests lr');
		$this->db->join('leave_types lt','lt.id = lr.leave_type_id','left');
		$this->db->join('leave_statuses ls','ls.id = lr.overall_status','left');
		$this->db->where('lr.id',$leave_request_id);
		$query = $this->db->get();

		return $query->row();
	}

	public function maillog_details($leave_request_id){
		$this->db->select('lr.id, lr.employeeid, p.firstname, p.lastname, lr.leavedt, lr.leave_category, lt.leave_name');
		$this->db->from('leave_requests lr');
		$this->db->join('leave_types lt','lt.id = lr.leave_type_id','left');
		$this->db->join('prlemployeemaster p','p.employeeid = lr.employeeid','left');
		$this->db->where('lr.id',$leave_request_id);
		$query = $this->db->get();

		return $query->row();
	}	

	// get the unique approver rank ids
	public function get_leave_status($leave_request_id){
		$q = "
				SELECT ranks.approver_rank, lrs.leave_status_id, ls.status_name
				FROM
				(
				    select ah.approver_rank
					from approver_hierarchies ah
				    join leave_assignments la on la.leave_hierarchy_id = ah.leave_hierarchy_id
				    join leave_requests lr on lr.employeeid = la.employeeid
				    where lr.id = $leave_request_id
				    and ah.is_active = 1
				    group by ah.approver_rank
				    order by ah.approver_rank
				 )ranks
				 LEFT JOIN leave_request_statuses lrs on lrs.approver_rank = ranks.approver_rank AND lrs.leave_request_id = $leave_request_id
				 LEFT JOIN leave_statuses ls on ls.id = lrs.leave_status_id
			";

		$query = $this->db->query($q);

		return $query->result();
	}

	public function summary(){
		$q = "
				SELECT p.employeeid, firstname, lastname, la.leave_credits, p.leave_accrual, vl.ctr as vl, sl.ctr as sl, bvmt.ctr as bvmt, lpat.ctr as lpat, lmat.ctr as lmat, pto.ctr as pto, el.ctr as el, unpaid.ctr as unpaid
				FROM prlemployeemaster p
				LEFT JOIN leave_assignments la on la.employeeid = p.employeeid AND is_active = 1
				LEFT JOIN(
				    select employeeid, count(1) as ctr
				    from leave_requests
				    where overall_status = 5
				    and leave_type_id = 2
				    and leave_category = 'paid'
				    group by employeeid
				) as vl on vl.employeeid = p.employeeid
				LEFT JOIN(
				    select employeeid, count(1) as ctr
				    from leave_requests
				    where overall_status = 5
				    and leave_type_id = 3
				    and leave_category = 'paid'
				    group by employeeid
				) as sl on sl.employeeid = p.employeeid
				LEFT JOIN(
				    select employeeid, count(1) as ctr
				    from leave_requests
				    where overall_status = 5
				    and leave_type_id = 4
				    and leave_category = 'paid'
				    group by employeeid
				) as bvmt on bvmt.employeeid = p.employeeid
				LEFT JOIN(
				    select employeeid, count(1) as ctr
				    from leave_requests
				    where overall_status = 5
				    and leave_type_id = 5
				    and leave_category = 'paid'
				    group by employeeid
				) as lpat on lpat.employeeid = p.employeeid
				LEFT JOIN(
				    select employeeid, count(1) as ctr
				    from leave_requests
				    where overall_status = 5
				    and leave_type_id = 6
				    and leave_category = 'paid'
				    group by employeeid
				) as lmat on lmat.employeeid = p.employeeid
				LEFT JOIN(
				    select employeeid, count(1) as ctr
				    from leave_requests
				    where overall_status = 5
				    and leave_type_id = 7
				    and leave_category = 'paid'
				    group by employeeid
				) as pto on pto.employeeid = p.employeeid
				LEFT JOIN(
				    select employeeid, count(1) as ctr
				    from leave_requests
				    where overall_status = 5
				    and leave_type_id = 8
				    and leave_category = 'paid'
				    group by employeeid
				) as el on el.employeeid = p.employeeid
				LEFT JOIN(
				    select employeeid, count(1) as ctr
				    from leave_requests
				    where overall_status = 5
				    and leave_category = 'unpaid'
				    group by employeeid
				) as unpaid on unpaid.employeeid = p.employeeid
				WHERE p.active = 0
				ORDER BY lastname
			";

		$query = $this->db->query($q);

		return $query->result();
	}

	public function paid_leave_count($employeeid,$status_id){
		$this->db->from('leave_requests');
		$this->db->where('employeeid',$employeeid);
		$this->db->where('leave_category','paid');
		$this->db->where('overall_status',$status_id);
		return $this->db->count_all_results();
	}

	public function get_rank_approvers($leave_request_id, $approver_rank){
		$this->db->select('p.firstname, p.lastname');
		$this->db->from('leave_requests lr');
		$this->db->join('leave_assignments la','la.employeeid = lr.employeeid');
		$this->db->join('leave_hierarchies lh','lh.id = la.leave_hierarchy_id','left');
		$this->db->join('approver_hierarchies ah','ah.leave_hierarchy_id = lh.id and ah.is_active = 1');
		$this->db->join('leave_approvers lapp','lapp.id = ah.leave_approver_id','left');
		$this->db->join('prlemployeemaster p','p.employeeid = lapp.employeeid','left');
		$this->db->where('lr.id',$leave_request_id);
		$this->db->where('ah.approver_rank',$approver_rank);
		$query = $this->db->get();

		return $query->result();
	}

	// public function get_approver_requests_for_approval($employeeid, $limit=null, $quantity=null){
	// 	$this->db->select('p.employeeid, p.firstname, p.lastname, lr.id, lr.leavedt, lr.leave_category,lr.leave_reason, lt.leave_name');
	// 	$this->db->from('leave_requests lr');
	// 	$this->db->join('leave_types lt','lt.id = lr.leave_type_id','left');
	// 	$this->db->join('prlemployeemaster p','p.employeeid = lr.employeeid','left');
	// 	$this->db->join('leave_assignments la','la.employeeid = lr.employeeid');
	// 	$this->db->join('approver_hierarchies ah','ah.leave_hierarchy_id = la.leave_hierarchy_id and ah.is_active = 1');
	// 	$this->db->join('leave_approvers lapp','lapp.id = ah.leave_approver_id');
	// 	// $this->db->where('lr.is_active',1);
	// 	$this->db->where('lr.overall_status',3); // for approval
	// 	$this->db->where('lapp.employeeid',$employeeid);
		
	// 	if(!empty($limit) && !empty($quantity))
	// 		$this->db->limit($limit, $quantity);
	// 	elseif(!empty($limit) && empty($quantity))
	// 		$this->db->limit($limit);
		
	// 	$query = $this->db->get();

	// 	return $query->result();
	// }

	public function get_approver_requests_for_approval($employeeid, $searchtxt=null, $limit=null, $quantity=null){
		$this->db->select('p.employeeid, p.firstname, p.lastname, lr.id, lr.leavedt, lr.leave_category,lr.leave_reason, lt.leave_name, lr.createdttm');
		$this->db->from('leave_requests lr');
		$this->db->join('leave_types lt','lt.id = lr.leave_type_id','left');
		$this->db->join('prlemployeemaster p','p.employeeid = lr.employeeid','left');
		$this->db->join('leave_assignments la','la.employeeid = lr.employeeid');
		$this->db->join('approver_hierarchies ah','ah.leave_hierarchy_id = la.leave_hierarchy_id and ah.is_active = 1');
		$this->db->join('leave_approvers lapp','lapp.id = ah.leave_approver_id');
		// $this->db->where('lr.is_active',1);
		$this->db->where('lr.overall_status',3); // for approval
		$this->db->where('lapp.employeeid',$employeeid);
		
		if(!empty($searchtxt)){
			$this->db->like("CONCAT_WS(' ',p.firstname,p.lastname)",$searchtxt);
		}

		if(!empty($limit) && !empty($quantity))
			$this->db->limit($limit, $quantity);
		elseif(!empty($limit) && empty($quantity))
			$this->db->limit($limit);
		
		$this->db->order_by('lr.createdttm', 'ASC');

		$query = $this->db->get();

		return $query->result();
	}

	public function get_approver_requests_completed($employeeid, $searchtxt=null, $limit=null, $quantity=null){
		$this->db->select('lr.id, lr.employeeid, p.firstname, p.lastname, lr.leavedt, lt.leave_name, lr.leave_category, lr.leave_reason, ls.status_name, lr.createdttm');
		$this->db->from('leave_requests lr');
		$this->db->join('leave_request_statuses lrs','lrs.leave_request_id = lr.id AND lrs.leave_status_id != 3');
		$this->db->join('leave_assignments la','la.employeeid = lr.employeeid');
		$this->db->join('prlemployeemaster p','p.employeeid = lr.employeeid');
		$this->db->join('leave_types lt','lt.id = lr.leave_type_id');
		$this->db->join('leave_statuses ls','ls.id = lrs.leave_status_id');
		$this->db->join('approver_hierarchies ah','ah.leave_hierarchy_id = la.leave_hierarchy_id AND ah.approver_rank = lrs.approver_rank');
		$this->db->join('leave_approvers lapp','lapp.id = ah.leave_approver_id');
		$this->db->where('lapp.employeeid',$employeeid); // for approval

		if(!empty($searchtxt)){
			$this->db->like("CONCAT_WS(' ',p.firstname,p.lastname)",$searchtxt);
		}

		$this->db->order_by('lr.createdttm', 'ASC');

		if(!empty($limit) && !empty($quantity))
			$this->db->limit($limit, $quantity);
		elseif(!empty($limit) && empty($quantity))
			$this->db->limit($limit);

		$query = $this->db->get();

		return $query->result();
	}


	public function update_overallstatus($leave_request_id, $leave_status_id){
		$this->db->set('overall_status',$leave_status_id);
		$this->db->where('id',$leave_request_id);
		$this->db->update('leave_requests');
	}

	public function get_monthly_leave_count_approved($leave_hierarchy_ids = [],$date_from){
		$date_to = date('Y-m-t',strtotime($date_from)); // $date will contain the first day of the month

		// $this->db->distinct();
		// $this->db->select('lr.id');
		// $this->db->from('leave_requests lr');
		// // $this->db->join('prlemployeemaster p','p.employeeid = lr.employeeid','left');
		// $this->db->join('leave_assignments la','la.employeeid = lr.employeeid','left');
		// $this->db->where_in('la.leave_hierarchy_id',$leave_hierarchy_ids);
		// $this->db->where('lr.leavedt >=',$date_from);
		// $this->db->where('lr.leavedt <=',$date_to);
		// $this->db->where('lr.overall_status',5); // 5 approved
		// $query = $this->db->get();

		// return $query->num_rows();


		$this->db->from('leave_requests lr');
		$this->db->join('leave_assignments la','la.employeeid = lr.employeeid','left');
		$this->db->where_in('la.leave_hierarchy_id',$leave_hierarchy_ids);
		$this->db->where('lr.leavedt >=',$date_from);
		$this->db->where('lr.leavedt <=',$date_to);
		$this->db->where('lr.overall_status',5); // 5 approved
		$count = $this->db->count_all_results();

		
		return $count;
	}

	public function get_monthly_leave_count_pending($leave_hierarchy_ids = [],$date_from){
		$date_to = date('Y-m-t',strtotime($date_from)); // $date will contain the first day of the month

		// $this->db->distinct();
		// $this->db->select('lr.id');
		// $this->db->from('leave_requests lr');
		// // $this->db->join('prlemployeemaster p','p.employeeid = lr.employeeid','left');
		// $this->db->join('leave_assignments la','la.employeeid = lr.employeeid','left');
		// $this->db->where_in('la.leave_hierarchy_id',$leave_hierarchy_ids);
		// $this->db->where('lr.leavedt >=',$date_from);
		// $this->db->where('lr.leavedt <=',$date_to);
		// $this->db->where('lr.overall_status',3); // 3 pending
		// $query = $this->db->get();

		// return $query->num_rows();


		$this->db->from('leave_requests lr');
		$this->db->join('leave_assignments la','la.employeeid = lr.employeeid','left');
		$this->db->where_in('la.leave_hierarchy_id',$leave_hierarchy_ids);
		$this->db->where('lr.leavedt >=',$date_from);
		$this->db->where('lr.leavedt <=',$date_to);
		$this->db->where('lr.overall_status',3); // 3 pending
		$count = $this->db->count_all_results();

		
		return $count;

	}


	public function get_total_approved_leaves($employeeid){
		date_default_timezone_set('Asia/Kuala_Lumpur');

		$date_start = date('Y-01-01');
		$date_end = date('Y-12-31');

		$this->db->from('leave_requests lr');
		$this->db->where('lr.leavedt >=', $date_start);
		$this->db->where('lr.leavedt <=', $date_end);
		$this->db->where('lr.employeeid', $employeeid);
		$this->db->where('overall_status', 5);

		return $this->db->count_all_results();
	}

	public function get_calculated_leave_balance($employeeid){
		date_default_timezone_set('Asia/Kuala_Lumpur');

		$curr_month = date('n') - 1;
		$approved_leave = $this->get_total_approved_leaves($employeeid);

		$this->db->select('leave_accrual');
		$this->db->from('prlemployeemaster p');
		$this->db->where('employeeid', $employeeid);
		$query = $this->db->get();

		$result = $query->row();
		$leave_accrual = $result->leave_accrual;

		return ($curr_month * $leave_accrual) - $approved_leave;
	}

}

