<div class="row">
	<div class="col-md-11 mx-auto">
		<div class="card p-3 announce-page">
			<div class="row">
				<div class="col-md-9">
					<div class="announce-head pl-3 pt-3">
						<h1 class="text-dark"><strong class="text-dark"><?php echo ucwords($announcement->title) ?></strong></h1>
						<em class="text-muted"><?php echo date('F d, Y h:i A',strtotime($announcement->createdttm)) ?></em>
					</div>
					<div class="announce-body pl-3 pb-3 pr-3">
						<div class="text-dark mt-3">
							<?php echo $announcement->body ?>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="option-container">
						<div>
							<a href="<?php echo base_url('announcement/'.$announcement->id.'/edit') ?>" class="btn btn-secondary">
								<small>
									<span class="fa fa-edit"></span>
									<span>Edit</span>
								</small>
							</a>
						</div>
						<div>
							<a href="<?php echo base_url('announcement/'.$announcement->id.'/destroy') ?>" class="btn btn-danger del-announcement" data-redirect="<?php echo base_url('announcement/all/') ?>">
								<small>
									<span class="fa fa-remove"></span>
									<span>Delete</span>
								</small>
							</a>
						</div>
						<div>
							<a href="<?php echo base_url('announcement/all') ?>" class="btn btn-secondary">
								<small>
									<span class="fa fa-bullhorn"></span>
									<span>See All</span>
								</small>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
