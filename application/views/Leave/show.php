<div class="container-fluid">
	<div class="row mb-4">
		<div class="col-md-12">
			<div class="ml-3">
				<a class="cta-def-btn" href="#" onclick="window.history.back()">
					<span class="back-icon font-14">Back</span>
				</a>
			</div>
		</div>
	</div>
</div>

<div class="p-3 leave-dashboard-grid font-14">
	<div class="mb-3">
		<div class="border-bottom p-3 leave-dash-head"><strong>Leave Details</strong></div>
		<div class="d-flex border">
			<div class="py-3 flex-fill text-center d-flex flex-column">
				<span>Date</span>
				<span><?php echo $leave->leavedt ?></span>
			</div>
			<div class="py-3 flex-fill border-left border-right text-center d-flex flex-column">
				<span>Type</span>
				<span><?php echo $leave->leave_name ?></span>
			</div>
			<div class="py-3 flex-fill text-center d-flex flex-column">
				<span>Overall Status</span>
				<span><?php echo $leave->status_name ?></span>
			</div>
		</div>
	</div>

	<div class="border">
		<div class="p-3 leave-dash-head"><strong>Approvers and Status</strong></div>
		<div>
			<div class="table-responsive">
				<table class="table">
					<thead class="thead-light">
						<tr>
							<th>Approver/s</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
						<!-- <tr>
							<td>Jason Jaca, Kemny Rose Jaca</td>
							<td>Approved</td>
						</tr>
						<tr>
							<td>Mary Ann Labus</td>
							<td>For Approval</td>
						</tr> -->
						<?php
							foreach ($rank_status as $rank_stat) {
								$stat = empty($rank_stat->status) ? 'For Approval' : $rank_stat->status;
								echo "<tr>";
								echo "<td> $rank_stat->approvers </td>";
								echo "<td> $stat </td>";
								echo "</tr>";
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

</div>