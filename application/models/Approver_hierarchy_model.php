<?php

  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Approver_hierarchy_model extends CI_Model{

	public function create($data){
		$this->db->insert('approver_hierarchies',$data);
		
		return $this->db->affected_rows();
	}

	public function approvers($lh_id){
		$this->db->select('la.id, ah.approver_rank, la.employeeid, p.firstname, p.lastname');
		$this->db->from('approver_hierarchies as ah');
		$this->db->join('leave_approvers as la','la.id = ah.leave_approver_id','left');
		$this->db->join('prlemployeemaster as p','p.employeeid = la.employeeid','left');
		$this->db->where('ah.leave_hierarchy_id', $lh_id);
		$this->db->where('ah.is_active',1);
		$this->db->order_by('ah.approver_rank', 'ASC');
		$query = $this->db->get();

		return $query->result();
	}

	public function avail_approvers($lh_id){
		$this->db->select('la.id, p.firstname, p.lastname');
		$this->db->from('leave_approvers as la');
		$this->db->join('prlemployeemaster as p','p.employeeid = la.employeeid','left');
		$this->db->join('approver_hierarchies as ah',"la.id = ah.leave_approver_id AND ah.leave_hierarchy_id = $lh_id AND ah.is_active = 1",'left');
		$this->db->where('la.is_active',1);
		$this->db->where('ah.id IS NULL');
		$this->db->order_by('p.firstname', 'ASC');
		$query = $this->db->get();

		return $query->result();
	}

	public function delete($lhid,$app_ids){
		date_default_timezone_set("Asia/Kuala_Lumpur");
		$data['is_active'] = 0;
		$data['updatedttm'] = date('Y-m-d H:i:s');

		$this->db->where('leave_hierarchy_id',$lhid);
		$this->db->where_not_in('leave_approver_id',$app_ids);
		$this->db->update('approver_hierarchies',$data);

		if($this->db->affected_rows() > 0)
			return true;
		return false;
	}

	public function deletePerLeaveHierarchy($lhid){
		date_default_timezone_set("Asia/Kuala_Lumpur");
		$data['is_active'] = 0;
		$data['updatedttm'] = date('Y-m-d H:i:s');

		$this->db->where('leave_hierarchy_id',$lhid)
		         ->where('is_active',1);

		$this->db->update('approver_hierarchies',$data);

		if($this->db->affected_rows() > 0)
			return true;
		return false;
	}

	public function insert_or_update($lhid,$app_ids,$ranks){
		date_default_timezone_set("Asia/Kuala_Lumpur");

		foreach ($app_ids as $app_id){
			$data['is_active'] = 1;
			$data['approver_rank'] = $ranks["$app_id"];
			$data['updatedttm'] = date('Y-m-d H:i:s');

			if($this->find($lhid,$app_id)){
				$this->db->where('leave_hierarchy_id',$lhid);
				$this->db->where('leave_approver_id',$app_id);
				$this->db->update('approver_hierarchies',$data);
			}else{
				$data['leave_hierarchy_id'] = $lhid;
				$data['leave_approver_id'] = $app_id;
				$data['createdttm'] = date('Y-m-d H:i:s');
				$this->db->insert('approver_hierarchies',$data);
			}
		}

	}

	public function find($lhid,$app_id){
		$this->db->where('leave_hierarchy_id',$lhid);
		$this->db->where('leave_approver_id',$app_id);
		$numrows = $this->db->count_all_results('approver_hierarchies');

		if($numrows > 0)
			return true;
		return false;
	}

	public function get_approvers($employeeid){
		$this->db->select('p.firstname, p.lastname, ah.approver_rank');
		$this->db->from('leave_assignments la');
		$this->db->join('approver_hierarchies ah','ah.leave_hierarchy_id = la.leave_hierarchy_id and ah.is_active = 1');
		$this->db->join('leave_approvers lapp','lapp.id = ah.leave_approver_id','left');
		$this->db->join('prlemployeemaster p','p.employeeid = lapp.employeeid','left');
		$this->db->where('la.employeeid',$employeeid);
		$query = $this->db->get();

		return $query->result();
	}

	public function is_approver($employeeid){
		$this->db->from('approver_hierarchies ah');
		$this->db->join('leave_approvers la','la.id = ah.leave_approver_id');
		$this->db->where('la.employeeid',$employeeid);
		$this->db->where('ah.is_active',1);
		
		return $this->db->count_all_results();
	}

	public function approver_rank($leave_request_id, $employeeid){
		$this->db->select('ah.approver_rank');
		$this->db->from('leave_requests lr');
		$this->db->join('leave_assignments la','la.employeeid = lr.employeeid');
		$this->db->join('approver_hierarchies ah','ah.leave_hierarchy_id = la.leave_hierarchy_id and ah.is_active = 1');
		$this->db->join('leave_approvers lapp','lapp.id = ah.leave_approver_id');
		$this->db->where('lapp.employeeid', $employeeid);
		$this->db->where('lr.id', $leave_request_id);
		$query = $this->db->get();

		return $query->row();
	}


	// public function get_next_approver($leave_request_id, $rank){
	// 	$rank = $rank + 1;

	// 	$this->db->select('lr.id, lr.employeeid, p2.firstname, p2.lastname, p1.email1 as approver_email, lr.leavedt, lr.leave_category, lt.leave_name');
	// 	$this->db->from('leave_requests lr');
	// 	$this->db->join('leave_assignments la','la.employeeid = lr.employeeid');
	// 	$this->db->join('approver_hierarchies ah','ah.leave_hierarchy_id = la.leave_hierarchy_id and ah.is_active = 1');
	// 	$this->db->join('leave_approvers lapp','lapp.id = ah.leave_approver_id');
	// 	$this->db->join('prlemployeemaster p1','p1.employeeid = lapp.employeeid','left');
	// 	$this->db->join('prlemployeemaster p2','p2.employeeid = lr.employeeid','left');
	// 	$this->db->join('leave_types lt','lt.id = lr.leave_type_id','left');
	// 	$this->db->where('lr.id', $leave_request_id);
	// 	$this->db->where('ah.approver_rank', $rank);
	// 	$query = $this->db->get();

	// 	return $query->result();
	// }

	public function get_next_approver($leave_request_id, $rank_id){
		$rank = 0;
		// get next rank
		$this->db->select('ah.approver_rank');
		$this->db->from('approver_hierarchies ah');
		$this->db->join('leave_assignments la','la.leave_hierarchy_id = ah.leave_hierarchy_id');
		$this->db->join('leave_requests lr','lr.employeeid = la.employeeid');
		$this->db->where('lr.id',$leave_request_id);
		$this->db->where('ah.is_active',1);
		$this->db->where('ah.approver_rank > ', $rank_id);
		$this->db->group_by('ah.approver_rank');
		$this->db->order_by('ah.approver_rank');
		$this->db->limit(1);
		$rank_query = $this->db->get();

		if($rank_query->num_rows() > 0){
			$rank = $rank_query->row()->approver_rank;
		}

		// get all approvers with the approver rank
		$this->db->select('p.email1');
		$this->db->from('leave_requests lr');
		$this->db->join('leave_assignments la','la.employeeid = lr.employeeid');
		$this->db->join('approver_hierarchies ah','ah.leave_hierarchy_id = la.leave_hierarchy_id and ah.is_active = 1');
		$this->db->join('leave_approvers lapp','lapp.id = ah.leave_approver_id');
		$this->db->join('prlemployeemaster p','p.employeeid = lapp.employeeid','left');
		$this->db->where('lr.id', $leave_request_id);
		$this->db->where('ah.approver_rank', $rank);
		$approvers = $this->db->get()->result_array();
		$approvers_arr = [];
		foreach ($approvers as $approver) {
			array_push($approvers_arr, $approver['email1']);
		}

		if(!empty($approvers_arr)){
			// get leave request details
			$this->db->select('lr.id, lr.employeeid, p2.firstname, p2.lastname, lr.leavedt, lr.leave_category, lt.leave_name');
			$this->db->from('leave_requests lr');
			$this->db->join('leave_assignments la','la.employeeid = lr.employeeid');
			$this->db->join('approver_hierarchies ah','ah.leave_hierarchy_id = la.leave_hierarchy_id and ah.is_active = 1');
			$this->db->join('prlemployeemaster p2','p2.employeeid = lr.employeeid','left');
			$this->db->join('leave_types lt','lt.id = lr.leave_type_id','left');
			$this->db->where('lr.id', $leave_request_id);
			$lr_details = $this->db->get()->row_array();
			$lr_details['approver_email'] = implode(', ', $approvers_arr);

			return (object) $lr_details;
		}else{
			return null;
		}
	}


	public function get_first_approvers($leave_request_id){
		// get minimum approver rank
		$this->db->select_min('ah.approver_rank');
		$this->db->from('leave_requests lr');
		$this->db->join('leave_assignments la','la.employeeid = lr.employeeid');
		$this->db->join('approver_hierarchies ah','ah.leave_hierarchy_id = la.leave_hierarchy_id and ah.is_active = 1');
		$this->db->where('lr.id', $leave_request_id);
		$min_rank = $this->db->get()->row()->approver_rank;

		// get all approvers with minimum approver rank
		$this->db->select('p.email1');
		$this->db->from('leave_requests lr');
		$this->db->join('leave_assignments la','la.employeeid = lr.employeeid');
		$this->db->join('approver_hierarchies ah','ah.leave_hierarchy_id = la.leave_hierarchy_id and ah.is_active = 1');
		$this->db->join('leave_approvers lapp','lapp.id = ah.leave_approver_id');
		$this->db->join('prlemployeemaster p','p.employeeid = lapp.employeeid','left');
		$this->db->where('lr.id', $leave_request_id);
		$this->db->where('ah.approver_rank', $min_rank);

		return $this->db->get()->result();
	}


	public function get_last_approver_rank($leave_request_id){
		$this->db->select_max('ah.approver_rank');
		$this->db->from('leave_requests lr');
		$this->db->join('leave_assignments la', 'la.employeeid = lr.employeeid');
		$this->db->join('approver_hierarchies ah','ah.leave_hierarchy_id = la.leave_hierarchy_id and ah.is_active = 1');
		$this->db->where('lr.id', $leave_request_id);
		$max_rank = $this->db->get()->row()->approver_rank;

		return $max_rank;
	}

}

