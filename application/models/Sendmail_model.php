<?php

  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sendmail_model extends CI_Model{

	public function config(){
		// $config = array(
		// 	'mailtype' => 'html',
		// 	'protocol' => 'smtp',
		// 	'smtp_host' => 'ssl://smtp.gmail.com',
		// 	'smtp_port' => 465,
		// 	'smtp_user' => 'digicono.recruitment@gmail.com',
		// 	'smtp_pass' => 'vGmM%F9e5X+',
		// 	'charset' => 'iso-8859-1'
		// );

		$config = array(
			'mailtype' => 'html',
			'protocol' => 'smtp',
			'smtp_host' => 'smtp-mail.outlook.com',
			'smtp_port' => 587,
			'smtp_crypto' => 'tls',
			'smtp_user' => 'leadgen_corp@outlook.com',
			'smtp_pass' => 'vGmM%F9e5X+'
		);

		return $config;
	}

	public function get_email_user(){
		return $this->email_user;
	}

	public function template(){
		return "
			<!DOCTYPE html>
			<html>
			<head>
				<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
				<style>
					.mail-header{
						padding: 20px;
						background-color: #007bff;
						color: #fff;
						font-family: Arial;
						font-size: 16px;
					}

					.mail-body{
						padding: 20px;
						color: #404040;
					}
				</style>
			</head>
			<body>
				<div class='mail-header'>
					{mail_header}
				</div>
				<div class='mail-body'>
					{mail_body}
				</div>
			</body>
			</html>
		";
	}

	public function password_reset_header(){
		return 'Verify Password Reset';
	}

	public function leave_approval_header(){
		return 'Leave Request';
	}

	public function password_reset_body($params = []){
		return "
			<p>User: {$params['employeeid']}</p>
			<p>Click on the link below to reset your password.</p>
			<p><a href='http://ess.digicononline.com/auth/reset/{$params['employeeid']}/{$params['token']}'>Reset Password</a></p>
		";
	}


	public function leave_approval_body($params = []){
		$rows = '';
		foreach ($params as $row) {
			$rows = $rows . "<tr>";
			$rows = $rows . "<td style='padding: 10px; border: 1px solid;'>". $row['requestor_id'] ."</td>";
			$rows = $rows . "<td style='padding: 10px; border: 1px solid;'>". $row['requestor_name'] ."</td>";
			$rows = $rows . "<td style='padding: 10px; border: 1px solid;'>". $row['leave_dt'] ."</td>";
			$rows = $rows . "<td style='padding: 10px; border: 1px solid;'>". $row['leave_category'] ."</td>";
			$rows = $rows . "<td style='padding: 10px; border: 1px solid;'>". $row['leave_type'] ."</td>";
			$rows = $rows . "</tr>";
		}

		$table_raw = "<table style='border-collapse: collapse'>
					 <thead style='background-color: #00b3b3'>
					 	<tr style='color: #fff'>
					 		<th style='padding: 10px; border: 1px solid #000;'>Employee Id</th>
					 		<th style='padding: 10px; border: 1px solid #000;'>Name</th>
					 		<th style='padding: 10px; border: 1px solid #000;'>Date</th>
					 		<th style='padding: 10px; border: 1px solid #000;'>Category</th>
					 		<th style='padding: 10px; border: 1px solid #000;'>Type</th>
					 	</tr>
					 </thead>
					 <tbody>
					 	{rows}
					 </tbody>
		          </table>";

		$table = str_replace('{rows}',$rows,$table_raw);

		$content = "
			<p>Hi,</p>
			<p>There are leave request waiting for your approval. Refer on the table below:</p>
			{table}
		";

		return str_replace('{table}',$table,$content);
	}

}

