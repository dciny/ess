<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'dashboard';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['unauthorized'] = 'error_message/unauthorized_access';

$route['emergency-control'] = 'emergency_control/index';
$route['emergency-control/new-user'] = 'emergency_control/new_user';
$route['emergency-control/create-user'] = 'emergency_control/create_user';
$route['emergency-control/edit-password'] = 'emergency_control/edit_password';
$route['emergency-control/update-password'] = 'emergency_control/update_password';

$route['leave/approver/leave-count-page'] = 'leave/leave_count_page';
$route['leave/approver/leave-count-page-submit'] = 'leave/leave_count_page_submit';
$route['leave/employee/search'] = 'leave/search_employee';
$route['leave/employee-calendar/(:num)'] = 'leave/employee_calendar/$1';
$route['leave-console'] = 'leave_console/index';
$route['leave-type'] = 'leave_type/index';
$route['leave-type/new'] = 'leave_type/new';
$route['leave-type/create'] = 'leave_type/create';
$route['leave-type/delete'] = 'leave_type/delete';
$route['leave-status'] = 'leave_status/index';
$route['leave-status/new'] = 'leave_status/new';
$route['leave-status/create'] = 'leave_status/create';
$route['leave-status/delete'] = 'leave_status/delete';
$route['leave-hierarchy'] = 'leave_hierarchy/index';
$route['leave-hierarchy/new'] = 'leave_hierarchy/new';
$route['leave-hierarchy/create'] = 'leave_hierarchy/create';
$route['leave-hierarchy/edit/(:num)'] = 'leave_hierarchy/edit/$1';
$route['leave-hierarchy/update'] = 'leave_hierarchy/update';
$route['leave-hierarchy/delete'] = 'leave_hierarchy/delete';
$route['leave-assignment/new'] = 'leave_assignment/new';
$route['leave-assignment/create'] = 'leave_assignment/create';
$route['leave-assignment/delete'] = 'leave_assignment/delete';

$route['leave-request-status/create'] = 'leave_request_status/create';
$route['leave-request-status/bulk_create'] = 'leave_request_status/bulk_create';

$route['leave/monthly-report/approved'] = 'leave/monthly_report_approved';
$route['leave/monthly-report/pending'] = 'leave/monthly_report_pending';
// $route['leave/requests-completed'] = 'leave/requests_completed';

$route['auth/change-password'] = 'auth/change_password';
$route['auth/update-password'] = 'auth/update_password';
$route['auth/forgot-password'] = 'auth/forgot_password';
$route['auth/password-send'] = 'auth/password_send';

$route['employee'] = 'dashboard/index';
$route['request/basic-info'] = 'employee_basic/index';
$route['request/basic-info/index'] = 'employee_basic/index';
$route['request/basic-info/new'] = 'request/basic_info';
$route['request/basic-info/edit/(:any)'] = 'employee_basic/edit/$1';
$route['request/basic-info/create'] = 'employee_basic/create';
$route['request/basic-info/update/(:any)'] = 'employee_basic/update/$1';
$route['request/basic-info/(:any)'] = 'request/basic_info';

$route['request/contact'] = 'employee_contact/index';
$route['request/contact/index'] = 'employee_contact/index';
$route['request/contact/new'] = 'request/contact';
$route['request/contact/edit/(:any)'] = 'employee_contact/edit/$1';
$route['request/contact/create'] = 'employee_contact/create';
$route['request/contact/update/(:any)'] = 'employee_contact/update/$1';
$route['request/contact/(:any)'] = 'request/contact';

$route['request/address'] = 'employee_address/index';
$route['request/address/index'] = 'employee_address/index';
$route['request/address/new'] = 'request/address';
$route['request/address/edit/(:any)'] = 'employee_address/edit/$1';
$route['request/address/create'] = 'employee_address/create';
$route['request/address/update/(:any)'] = 'employee_address/update/$1';
$route['request/address/(:any)'] = 'request/address';

$route['request/work'] = 'employee_work/index';
$route['request/work/index'] = 'employee_work/index';
$route['request/work/new'] = 'request/work';
$route['request/work/edit/(:any)'] = 'employee_work/edit/$1';
$route['request/work/create'] = 'employee_work/create';
$route['request/work/update/(:any)'] = 'employee_work/update/$1';
$route['request/work/(:any)'] = 'request/work';


$route['announcement/(:num)'] = 'announcement/show/$1';
$route['announcement/(:num)/edit'] = 'announcement/edit/$1';
$route['announcement/(:num)/destroy'] = 'announcement/destroy/$1';

$route['request'] = 'error_message/page_not_found';


$route['reset-hash-password'] = 'auth/reset_and_hash_password';