<div class="mx-auto w-80pct">
	<h5>Leave Types</h5>
	<div class="table-responsive">
		<table class="table table-sm table-bordered fontsize-14">
			<thead class='thead-dark'>
				<tr class="text-center">
					<th>Leave Type</th>
					<th>Leave Description</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
	<?php 
					if(count($leave_types) > 0){  
						foreach ($leave_types as $leave_type) {
	?>
							<tr>
								<td class="align-middle"> <?php echo $leave_type->leave_name ?> </td>
								<td class="align-middle"> <?php echo $leave_type->leave_desc ?> </td>
								<td class="align-middle text-center"> 
									<button class="btn btn-sm btn-danger jrem-btn" data-id="<?php echo $leave_type->id ?>">Remove</button> 
								</td>
							</tr>
	<?php
						}
	?>

	<?php 
					}else{ 
	?>
						<tr class="text-center">
							<td colspan="4">No leave type found</td>
						</tr>
	<?php 
					} 
	?>
			</tbody>
		</table>
	</div>
</div>


<div class="modal-bg pos-fixed">
	<form action="<?php echo base_url('leave-type/delete') ?>" method="post">
		<div class="card w-400px modal-content pos-absolute">
			<div class="card-header text-secondary"><strong>Delete confirmation</strong></div>
			<div class="card-body">
				<p>Are you sure you want to delete this item?</p>
			</div>
			<div class="card-footer text-right">
				<input type="hidden" name="id">
				<input type="submit" class="btn btn-sm btn-primary px-3 mr-2" value="Yes">
				<span class="btn btn-sm btn-secondary px-3 jcancel-btn">No</span>
			</div>
		</div>
	</form>
</div>