<?php

  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth_model extends CI_Model{

	public function get_user_info($username){

		$condition = "employeeid =" . "'" . $username . "'";

		$this->db->select('*');
		$this->db->from('prlemployeemaster');
		$this->db->where($condition);
		$this->db->limit(1);

		$query = $this->db->get();

		if($query->num_rows() == 1){
			return $query->row();
		}else{
			return false;
		}
	}

	public function get_user($user){
		$this->db->where('username',$user);
		$query = $this->db->get('user');

		if($query->num_rows() == 1) {
			return $query->row();
		}else{
			return null;
		}
	}

	public function update_password($username,$new_pw,$ind = 1){
		$hashed_new_pw = password_hash($new_pw, PASSWORD_DEFAULT);

		$this->db->where('username', $username);
		$this->db->update('user', array('password' => $hashed_new_pw,'is_password_change' => $ind));

		return $this->db->affected_rows();
	}

	public function get_all_users(){
		$query = $this->db->get('user');

		return $query->result();
	}

	public function get_all_pw_not_updated_users(){
		$this->db->where('is_password_change',0);
		$query = $this->db->get('user');

		return $query->result();
	}

	private function is_active_password_reset_exist($employeeid){
		$this->db->where('employeeid',$employeeid);
		$this->db->where('status',0);
		$query = $this->db->get('password_reset');

		if($query->num_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	public function create_password_reset($employeeid){
		$request_exist = $this->is_active_password_reset_exist($employeeid);

		if(!$request_exist){ // insert if not exist
			date_default_timezone_set("Asia/Kuala_Lumpur");
			$data['employeeid'] = $employeeid;
			// $data['token'] = password_hash($employeeid, PASSWORD_DEFAULT);
			// $data['token'] = bin2hex(openssl_random_pseudo_bytes(16));
			$data['token'] = md5(uniqid(rand(), true));
			$data['createdttm'] = date('Y-m-d H:i:s');
			$data['updatedttm'] = date('Y-m-d H:i:s');
			$this->db->insert('password_reset',$data);

			if($this->db->affected_rows() != 1)
				return false;
		}

		return true; // always return true for existing row
	}


	public function get_password_reset_token($employeeid){
		$this->db->select('token');
		$this->db->where('employeeid',$employeeid);
		$this->db->where('status',0);
		$query = $this->db->get('password_reset');

		if($query->num_rows() > 0){
			$result = $query->row();
			return $result->token;
		}else{
			return null;
		}
	}


	public function get_password_reset_details($employeeid,$token){
		$this->db->select('id,status');
		$this->db->where('employeeid',$employeeid);
		$this->db->where('token',$token);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get('password_reset');

		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return null;
		}
	}

	public function set_password_reset_to_complete($id){
		$data = array(
		        'status' => 1
		);

		$this->db->where('id', $id);
		$this->db->update('password_reset', $data);
	}

}

