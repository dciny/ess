<?php
  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct(){

		parent::__construct();
		$this->load->model('Data_models');

	}

	public function index(){


		$this->load->view('welcome_message');
	}


	public function dashboard(){
		$username = $this->session->userdata['logged_in']['employeeid'];
 		$data1['value1'] = $this->Data_models->get_info($username);
		$this->load->view('Components/header',$data1);
 		$this->load->view('dashboard');
		$this->load->view('Components/footer');

	}

	public function login(){

		$this->form_validation->set_rules('username', 'username', 'trim|required');
		$this->form_validation->set_rules('password', 'password', 'trim|required');


		if($this->form_validation->run()){
			$data = array(

			'username' => $this->input->post('username'),

			'password' => $this->input->post('password')

			);
		}

		$result = $this->Data_models->login($data);


		if ($result == TRUE) {

			$username = $this->input->post('username');

			$result = $this->Data_models->get_user_info($username);


			$session_data = array(

			'employeeid' => $result[0]->employeeid

			);

 			$this->session->set_userdata('logged_in',$session_data);
 			return redirect('index.php/Welcome/dashboard');

 		}else{

 			$this->session->set_flashdata('error','Invalid Username or Password');
			redirect('index.php/Welcome');

 		}

 	}

 		public function payslip(){
		if(isset($this->session->userdata['logged_in'])){

 			$username = $this->session->userdata['logged_in']['employeeid'];
 			$data['value1'] = $this->Data_models->get_info($username);
 			$data['value2'] = $this->Data_models->get_payslip($username);
 			$data['value3'] = $this->Data_models->get_prlotherinclog($username);
 			$data['value7'] = $this->Data_models->get_prlotherinclog2($username);
 			$data['value4'] = $this->Data_models->get_prladjustmentlog($username);
 			$data['value5'] = $this->Data_models->get_prladjustmentlog2($username);
 			$data['value6'] = $this->Data_models-> get_prlloansgov($username);

 			$this->load->view('Components/header',$data);
 			$this->load->view('payslip',$data);
 			$this->load->view('Components/footer');
 		}else{
 			return redirect('index.php/Welcome');
 		}

 		}

 		 		
 		public function logout(){

			session_destroy();
			redirect('index.php/Welcome');

		}

		public function index2(){
			$this->load->view('construction/index');

		}
		public function Schedule(){
			$this->load->view('Schedule/index');
 			
 		}


 



	}
