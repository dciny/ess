<div class="mx-auto w-80pct">
	<h5>Leave Approvers</h5>
	<form action="" method="get" class="mb-2">
		<div class="search-grid">
			<select class="form-control form-control-sm">
				<option>Employee id</option>
				<option>First Name</option>
				<option>Last Name</option>
			</select>
			<div class="input-group">
				<input type="text" class="form-control form-control-sm" name="search" placeholder="Search...">
				<div class="input-group-append">
					<button class="btn btn-primary btn-sm" type="submit">Submit</button>
				</div>
			</div>
		</div>
	</form>
	<div class="table-responsive">
		<table class="table table-sm table-bordered fontsize-14">
			<thead class='thead-dark'>
				<tr class="text-center">
					<th>Employee ID</th>
					<th>First Name</th>
					<th>Last Name</th>
				</tr>
			</thead>
			<tbody>
	<?php 
					if(count($approvers) > 0){  
						foreach ($approvers as $approver) {
	?>
							<tr>
								<td class="align-middle"> <?php echo $approver->employeeid ?> </td>
								<td class="align-middle"> <?php echo $approver->firstname ?> </td>
								<td class="align-middle"> <?php echo $approver->lastname ?> </td>
							</tr>
	<?php
						}
	?>

	<?php 
					}else{ 
	?>
						<tr class="text-center">
							<td colspan="4">No Approvers found</td>
						</tr>
	<?php 
					} 
	?>
			</tbody>
		</table>
	</div>
</div>