<div>
	<div class="mx-auto w-80pct">
		<span class="fontsize-40">Leave Management Console</span>
	</div>
	<div class="intro-grid mt-5">
		<div class="intro-box border">
			<div class="border-bottom bg-warning-gradient p-2 text-light"><strong>Leave Approvers</strong></div>
			<div class="p-2 fontsize-14">
				<p>Create users to approve leaves which can be used as options on selecting approvers in hierarchy rules setting.</p>
				<p>Start by clicking on this <a href="#" class="dark-orange">link</a>.</p>
			</div>
		</div>
		<div class="intro-box border">
			<div class="border-bottom bg-warning-gradient p-2 text-light"><strong>Hierarchy Rules</strong></div>
			<div class="p-2 fontsize-14">
				<p>Create hierarchy rules on approving a leave.</p>
				<p>Start by clicking on this <a href="#" class="dark-orange">link</a>.</p>
			</div>
		</div>
		<div class="intro-box border">
			<div class="border-bottom bg-warning-gradient p-2 text-light"><strong>Leave Types</strong></div>
			<div class="p-2 fontsize-14">
				<p>Create type of leaves. This will be used in placing leave rules on the employee.</p>
				<p>Start by clicking on this <a href="#" class="dark-orange">link</a>.</p>
			</div>
		</div>
		<div class="intro-box border">
			<div class="border-bottom bg-warning-gradient p-2 text-light"><strong>Leave Request Assignment</strong></div>
			<div class="p-2 fontsize-14">
				<p>Assign leaves. This will be the status that the approver will select.</p>
				<p>Start by clicking on this <a href="#" class="dark-orange">link</a>.</p>
			</div>
		</div>
		<div class="intro-box border">
			<div class="border-bottom bg-warning-gradient p-2 text-light"><strong>Leave Request Status</strong></div>
			<div class="p-2 fontsize-14">
				<p>Create status that will be displayed on the leave request. This will be the status that the approver will select.</p>
				<p>Start by clicking on this <a href="#" class="dark-orange">link</a>.</p>
			</div>
		</div>
	</div>
</div>