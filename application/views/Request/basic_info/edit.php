<div class="main container-fluid">
	<div class="row mb-5">
		<div class="col-md-12">
			<div class="ml-5">
				<a class="cta-def-btn" href="<?php echo base_url('request/basic-info/index'); ?>">
					<span class="back-icon">Back</span>
				</a>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 p-0">
			<div class="mx-5">
				<p class="text-danger">* Send change request to HR</p>
			</div>
			<div class="box cyan-lin-bg box-min-size mx-5">
				<h5>Basic Info</h5>
				<form action='<?php echo base_url("request/basic-info/update/$basic_details->id") ?>' method="post">
					<div class="table-responsive">
						<table class="table">
							<tbody>
								<tr>
									<td style="width: 120px">First Name:</td>
									<td>
										<input type="text" 
										       name="firstname" 
											   class="form-control <?php if(form_error('firstname')) echo 'is-invalid' ?>" 
											   value="<?php echo $basic_details->firstname ?>">
										<?php echo form_error('firstname'); ?>
									</td>
								</tr>
								<tr>
									<td style="width: 120px">Middle Name:</td>
									<td>
										<input type="text" 
										       name="middlename" 
										       class="form-control <?php if(form_error('middlename')) echo 'is-invalid' ?>" 
										       value="<?php echo $basic_details->middlename ?>">
										<?php echo form_error('middlename'); ?>
									</td>
								</tr>
								<tr>
									<td style="width: 120px">Last Name:</td>
									<td>
										<input type="text" 
										       name="lastname" 
										       class="form-control <?php if(form_error('lastname')) echo 'is-invalid' ?>" 
										       value="<?php echo $basic_details->lastname ?>">
										<?php echo form_error('lastname'); ?>
									</td>
								</tr>
								<tr>
									<td>Birthday:</td>
									<td>
										<input type="date" 
										       class="form-control <?php if(form_error('birthdate')) echo 'is-invalid' ?>" 
										       name="birthdate" 
										       value="<?php echo $basic_details->birthdate ?>">
										<?php echo form_error('birthdate'); ?>
									</td>
								</tr>
								<tr class="text-right">
									<td colspan="2">
										<input type="submit" name="submit" value="Update Change Request" class="btn cta-btn cyan-bg-btn">
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>