<form action="<?php echo base_url('schedule/sched') ?>" method="post">
  <div class="sched-grid">
    <label for="year">Year:</label>
    <select class="form-control form-control-sm" id="year" name="year">
      <?php foreach ($curr_year as $finalhours_yr) { ?>
        <option value="<?php echo $finalhours_yr->year ?>" <?php echo $year == $finalhours_yr->year ? 'selected' : '' ?> >
          <?php echo $finalhours_yr->year ?>
        </option>
      <?php } ?>
    </select>
  
    <label for="month">Choose a Month:</label>
    <select class="form-control form-control-sm" id="month" name="month">
      <option value="1" <?php echo $month == '1' ? 'selected' : '' ?>>January</option>
      <option value="2" <?php echo $month == '2' ? 'selected' : '' ?>>February</option>
      <option value="3" <?php echo $month == '3' ? 'selected' : '' ?>>March</option>
      <option value="4" <?php echo $month == '4' ? 'selected' : '' ?>>April</option>
      <option value="5" <?php echo $month == '5' ? 'selected' : '' ?>>May</option>
      <option value="6" <?php echo $month == '6' ? 'selected' : '' ?>>June</option>
      <option value="7" <?php echo $month == '7' ? 'selected' : '' ?>>July</option>
      <option value="8" <?php echo $month == '8' ? 'selected' : '' ?>>August</option>
      <option value="9" <?php echo $month == '9' ? 'selected' : '' ?>>September</option>
      <option value="10" <?php echo $month == '10' ? 'selected' : '' ?>>October</option>
      <option value="11" <?php echo $month == '11' ? 'selected' : '' ?>>November</option>
      <option value="12" <?php echo $month == '12' ? 'selected' : '' ?>>December</option>
    </select>

    <label for="pay">Pay Period:</label>
    <select class="form-control form-control-sm" id="pay" name="pay">
      <option value="10" <?php echo $pay == '10' ? 'selected' : '' ?>>10</option>
      <option value="25" <?php echo $pay == '25' ? 'selected' : '' ?>>25</option>
    </select>

    <input class="btn btn-primary btn-sm" type="submit" name="submit">
  </div>
</form>




<div class="container mt-5">
  <h2>Approve Schedule</h2>
  <div class="row">
    <div class='col-sm-12'>
      <?php if(count($sched) > 0){ ?>
        <div class="table-responsive">
          <table class='table table-bordered'>
            <thead class="thead-dark">
              <tr>
                <th>Shift Day</th>
                <th>Status</th>
                <th>Regular Hours</th>
                <th>Nightdiff Hours</th>
                <th>Regular OT Hours</th>
                <th>Late</th>
                <th>Undertime</th>
                <th>Shiftday</th>
                <th>Start Shift</th>
                <th>End Shift</th>
                <th>OT Start</th>
                <th>OT End</th>
              </tr>
            </thead>
            <tbody class="bg-light">
              <?php
                foreach ($sched as $s) {
                  switch ($s->status) {
                    case 'Restday':
                       $classStatus = 'bg-warning';break;
                    case 'Present':
                     $classStatus = 'bg-success';break;
                    case 'Absent':
                      $classStatus = 'bg-danger';break;  
                    default:
                      $classStatus = 'text-dark';break;
                  }

                  echo "<tr class='$classStatus' style='color: #fff;'>";
                  echo "<td>$s->shiftdate</td>";
                  echo "<td>$s->status</td>";
                  echo "<td>$s->sum_reg</td>";
                  echo "<td>$s->sum_night</td>";
                  echo "<td>$s->sum_ot</td>";
                  echo "<td>$s->sum_late</td>";
                  echo "<td>$s->sum_under</td>";
                  echo "<td>$s->shiftday</td>";
                  echo "<td>$s->startshift</td>";
                  echo "<td>$s->endshift</td>";
                  echo "<td>$s->otstart</td>";
                  echo "<td>$s->otend</td>";
                  echo "</tr>";
                }
              ?>
            </tbody>
          </table>
        </div>
      <?php 
        }else{
          echo "<span>Schedule not Posted..</span>";
        }
      ?>
    </div>
  </div>
</div>



