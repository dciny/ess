<?php
	$ind = isset($ind) && !empty($ind) ? $ind : 'none';
?>

<!DOCTYPE html>
<html>
<head>
	<title><?php echo $header_title ?></title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	 <!-- reset css -->
  	<link href="<?php echo base_url('assets/css/reset.css') ?>" rel="stylesheet">
  	<!-- fontawesome -->
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  	<!-- Nunito font -->
  	<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<!-- https://www.npmjs.com/package/bootstrap-select-->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.18/dist/css/bootstrap-select.min.css">
	<!-- custom css -->
	<link href="<?php echo base_url('assets/css/leave-style.css') ?>" rel="stylesheet">
	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js" defer></script>
	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js" defer></script>
	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" defer></script>
	<!-- https://www.npmjs.com/package/bootstrap-select -->
	<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.18/dist/js/bootstrap-select.min.js" defer></script>
	<!-- Main js -->
	<script src="<?php echo base_url('assets/js/leave.js') ?>" defer></script>
	
</head>
<body>

	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<ul class="navbar-nav">
			<li class="nav-item">
				<a class="nav-link active" href="<?php echo base_url() ?>">ESS</a>
			</li>
			<li class="nav-item">
				<a class="nav-link active" href="mailto:hr@digicononline.com">Contact HR</a>
			</li>
		</ul>
	</nav>
	<div class="grid-body">
		<!-- top nav will only show at 740px screen width -->
		<div class="top-nav border mb-1">
			<span class="fa fa-bars ml-3 nav-toggle"></span>
			<div class="top-nav-content">
				<span class="fa fa-close nav-close"></span>
				<div>
					
				</div>
			</div>
		</div>
		<!-- end -->

		<div class="sidebar border-right mr-1">
			<div class="border-bottom p-1 sidebar-header">
				<h6 class="ml-3 mb-0 p-1">Configurations</h6>
				<span class="fa fa-gear text-secondary sidebar-gear-icon"></span>
			</div>
			
			<!-- AP -->
			<div class="fontsize-14 ml-5 mt-3">
				<ul>
					<li>
						<div class="grp-header pos-relative">
							<span class="fa <?php echo toggle_val($ind,'AP','fa-caret-down','fa-caret-right') ?> pos-absolute pos-left-caret fontsize-18"
								  data-stat="<?php echo toggle_val($ind,'AP','active','hidden') ?>">
							</span>
							<h6 class="dark-orange">Leave Approvers</h6>
						</div>
						<div class="grp-body" style="display: <?php echo toggle_val($ind,'AP','block','none') ?>">
							<ul>
								<li class="py-1"><a href="<?php echo base_url('approver') ?>">Approver List</a></li>
								<li class="py-1"><a href="<?php echo base_url('approver/show') ?>">Add/Remove Approvers</a></li>
							</ul>
						</div>
					</li>
				</ul>
			</div>

			<!-- HR -->
			<div class="fontsize-14 ml-5 mt-3">
				<ul>
					<li>
						<div class="grp-header pos-relative">
							<span class="fa <?php echo toggle_val($ind,'HR','fa-caret-down','fa-caret-right') ?> pos-absolute pos-left-caret fontsize-18"
								  data-stat="<?php echo toggle_val($ind,'HR','active','hidden') ?>">
							</span>
							<h6 class="dark-orange">Hierarchy Rules</h6>
						</div>
						<div class="grp-body" style="display: <?php echo toggle_val($ind,'HR','block','none') ?>">
							<ul>
								<li class="py-1"><a href="<?php echo base_url('leave-hierarchy') ?>">Rules List</a></li>
								<li class="py-1"><a href="<?php echo base_url('leave-hierarchy/new') ?>">Add Rules</a></li>
							</ul>
						</div>
					</li>
				</ul>
			</div>

			<!-- LT -->
			<div class="fontsize-14 ml-5 mt-3">
				<ul>
					<li>
						<div class="grp-header pos-relative">
							<span class="fa <?php echo toggle_val($ind,'LT','fa-caret-down','fa-caret-right') ?> pos-absolute pos-left-caret fontsize-18"
								  data-stat="<?php echo toggle_val($ind,'LT','active','hidden') ?>">
							</span>
							<h6 class="dark-orange">Leave Types</h6>
						</div>
						<div class="grp-body" style="display: <?php echo toggle_val($ind,'LT','block','none') ?>">
							<ul>
								<li class="py-1"><a href="<?php echo base_url('leave-type') ?>">Leave Type List</a></li>
								<li class="py-1"><a href="<?php echo base_url('leave-type/new') ?>">Add Leave Type</a></li>
							</ul>
						</div>
					</li>
				</ul>
			</div>

			<!-- LS -->
			<div class="fontsize-14 ml-5 mt-3">
				<ul>
					<li>
						<div class="grp-header pos-relative">
							<span class="fa <?php echo toggle_val($ind,'LS','fa-caret-down','fa-caret-right') ?> pos-absolute pos-left-caret fontsize-18"
								  data-stat="<?php echo toggle_val($ind,'LS','active','hidden') ?>">
							</span>
							<h6 class="dark-orange">Leave Status</h6>
						</div>
						<div class="grp-body" style="display: <?php echo toggle_val($ind,'LS','block','none') ?>">
							<ul>
								<li class="py-1"><a href="<?php echo base_url('leave-status') ?>">Leave Status List</a></li>
								<li class="py-1"><a href="<?php echo base_url('leave-status/new') ?>">Add Leave Status</a></li>
							</ul>
						</div>
					</li>
				</ul>
			</div>

			<!-- LA -->
			<div class="fontsize-14 ml-5 mt-3">
				<ul>
					<li>
						<div class="grp-header pos-relative">
							<span class="fa <?php echo toggle_val($ind,'LA','fa-caret-down','fa-caret-right') ?> pos-absolute pos-left-caret fontsize-18"
								  data-stat="<?php echo toggle_val($ind,'LA','active','hidden') ?>">
							</span>
							<h6 class="dark-orange">Leave Assignment</h6>
						</div>
						<div class="grp-body" style="display: <?php echo toggle_val($ind,'LA','block','none') ?>">
							<ul>
								<li class="py-1"><a href="<?php echo base_url('leave-assignment/new') ?>">Assign Leave</a></li>
							</ul>
						</div>
					</li>
				</ul>
			</div>

		</div>

		<div class="main-content">
			<?php echo $rendered_view ?>
		</div>
	</div>
</body>
</html>