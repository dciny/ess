<?php
  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Error_message extends CI_Controller {

	public function __construct(){

		parent::__construct();
		
	}

	public function page_not_found(){
		$data['heading'] = 'Page not found';
		$data['message'] = "<p>Cannot find the page you're looking for</p>";
		$this->load->view('errors/html/error_404',$data);
	}

	public function unauthorized_access(){
		$data['heading'] = 'Unauthorized Access';
		$data['message'] = "<p>You're not allowed to update this request. Go back to your list of <a href='/request/basic-info/index'>requests</a></p>";
		$this->load->view('errors/html/error_404',$data);
	}

}
