<?php
  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Leave_status extends CI_Controller {

	public function __construct(){

		parent::__construct();
		
		$this->load->model('leave_status_model','leave_status');
		$this->load->helper('url');
	}

	
	public function index(){
		$data['leave_statuses'] = $this->leave_status->all();
		$data['ind'] = 'LS'; // used in the sidebar of the layout
		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Leave_status/index',$data,'leave');
	}

	public function new(){
		$data['ind'] = 'LS'; // used in the sidebar of the layout
		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Leave_status/new',$data,'leave');
	}

	public function create(){
		$this->form_validation->set_error_delimiters('<p class="invalid-feedback m-0">', '</p>');
		$this->form_validation->set_rules('status_name', 'Status name', 'trim|required|is_unique[leave_statuses.status_name]');

		$leave['status_name'] = $this->input->post('status_name');
		$leave['status_desc'] = $this->input->post('status_desc');

		if($this->form_validation->run() == FALSE){
			// show again the edit form
			$data['ind'] = 'LS'; // used in the sidebar of the layout
			$this->load->library('layouts'); // load layout library
			$this->layouts->view('Leave_status/new',$data,'leave');
		}else{
			$this->leave_status->create($leave);
			redirect(base_url('leave-status'),'location');
		}

	}

	public function delete(){
		$leave['id'] = $this->input->post('id');
		$this->db->delete('leave_statuses', $leave);

		redirect(base_url('leave-status'),'location');
	}

}
