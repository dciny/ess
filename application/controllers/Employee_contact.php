<?php
  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Employee_contact extends CI_Controller {

	public function __construct(){

		parent::__construct();
		
		$this->load->model('employee_contact_model');
		$this->load->model('employee_model');
		$this->load->helper('url');
	}

	public function _remap($method, $params = []){
		$login_session = $this->session->userdata('logged_in');

		if(isset($login_session)){

			$is_password_change = $this->session->userdata['logged_in']['is_password_change'];
			if($is_password_change == 0){
				redirect('auth/change-password', 'location');
			}

			$this->$method($params);
		}else{
			redirect('auth/index', 'location');
		}
	}

	public function index(){
		$employeeid = $this->session->userdata['logged_in']['employeeid'];
		$data['employee'] = $this->employee_model->get_employee_details($employeeid);
		$data['users'] = $this->employee_model->get_user_id($employeeid);
		$data['contacts'] = $this->employee_contact_model->get_all_contact_details($employeeid);
		$data['header'] = 'REQUEST: CONTACTS';

		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Request/contact/index',$data,'app');
	}

	public function create(){
		$params = $this->set_params();
		$is_exists = $this->is_exists($params['employeeid']); // return an id if exists else false

		if(!$is_exists){
			$this->set_form_validation();
			if ($this->form_validation->run() == FALSE){
				$data['employee'] = $this->employee_model->get_employee_details($params['employeeid']);
				$data['users'] = $this->employee_model->get_user_id($params['employeeid']);
				$this->load->library('layouts'); // load layout library
				$this->layouts->view('Request/contact/new',$data,'app');
			}else{
				$this->db->insert('req_employee_contact', $params);
				redirect('request/contact/index', 'location');
			}
		}else{
			$this->update($is_exists);
		}
	}

	public function edit($id){
		$id = is_array($id) ? $id[0] : $id;
		$employeeid = $this->session->userdata['logged_in']['employeeid'];
		if($this->employee_contact_model->is_authorized($id,$employeeid)){
			$data['employee'] = $this->employee_model->get_employee_details($employeeid);
			$data['users'] = $this->employee_model->get_user_id($employeeid);
			$data['contacts'] = $this->employee_contact_model->get_contact_details($id);
			$this->load->library('layouts'); // load layout library
			$this->layouts->view('Request/contact/edit',$data,'app');
		}else{
			redirect('/unauthorized');
		}

	}

	public function update($id){
		$id = is_array($id) ? $id[0] : $id;
		$this->set_form_validation();
		$data['contacts'] = $this->employee_contact_model->get_contact_details($id);

		if ($this->form_validation->run() == FALSE){
			$data['employee'] = $this->employee_model->get_employee_details($params['employeeid']);
			$data['users'] = $this->employee_model->get_user_id($params['employeeid']);
			$this->load->library('layouts'); // load layout library
			$this->layouts->view('Request/contact/edit',$data,'app');
		}else{
			$params = $this->set_params();
			unset($params['createdttm']);
			if($this->employee_contact_model->is_authorized($id,$params['employeeid'])){
				$this->db->update('req_employee_contact', $params, array('id' => $id));
				redirect('request/contact/index', 'location');
			}else{
				redirect('/unauthorized');
			}
		}
	}


	private function is_exists($params){
		return $this->employee_contact_model->is_exists($params);
	}

	private function set_params(){
		date_default_timezone_set("Asia/Kuala_Lumpur");

		$params['employeeid'] = $this->session->userdata['logged_in']['employeeid'];
		$params['phone1'] = $this->input->post('phone1');
		$params['phone2'] = $this->input->post('phone2');
		$params['email1'] = $this->input->post('email1');
		$params['email2'] = $this->input->post('email2');
		$params['createdttm'] = date('Y-m-d H:i:s');
		$params['updatedttm'] = date('Y-m-d H:i:s');


		return $params;
	}

	private function set_form_validation(){
		$this->form_validation->set_error_delimiters('<p class="invalid-feedback m-0">', '</p>');
		// $this->form_validation->set_rules('phone1', 'Phone 1', 'trim|required');
		// $this->form_validation->set_rules('phone2', 'Phone 2', 'trim|required');
		$this->form_validation->set_rules('email1', 'Email 1', 'trim|valid_email');
		$this->form_validation->set_rules('email2', 'Email 2', 'trim|valid_email');
	}


}
