<?php
  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payslip extends CI_Controller {

	public function __construct(){

		parent::__construct();
		
		$this->load->model('payslip_model');
		$this->load->model('employee_model');
		$this->load->helper('url');
	}

	public function _remap($method, $params = []){
		$login_session = $this->session->userdata('logged_in');
		
		if(isset($login_session)){

			$is_password_change = $this->session->userdata['logged_in']['is_password_change'];
			if($is_password_change == 0){
				redirect('auth/change-password', 'location');
			}
			
			$this->$method($params);
		}else{
			redirect('auth/index', 'location');
		}
	}

	public function show(){
		$username = $this->session->userdata['logged_in']['employeeid'];

		$payperiod = $this->payslip_model->get_latest_payperiod();
		$period = $this->input->post('period');
		if(isset($period)){
			$payperiod = $period;
		}

		$data['periods'] = $this->payslip_model->get_all_payperiod($username);
		$data['employee'] =  $this->payslip_model->get_emp_payslip_info($username,$payperiod);
		$data['adjustmentlogs'] = $this->payslip_model->get_prladjustmentlog($username,$payperiod);
		$data['otherinclogs'] = $this->payslip_model->get_prlotherinclog($username,$payperiod);
		$data['otherinclogs2'] = $this->payslip_model->get_prlotherinclog2($username,$payperiod);
		$data['adjustmentlogs2'] = $this->payslip_model->get_prladjustmentlog2($username,$payperiod);
		$data['prlloansgovs'] = $this->payslip_model->get_prlloansgov($username);
		$data['users'] = $this->employee_model->get_user_id($username);
		$data['ttlearn'] = 0;
		$data['header'] = 'EARNINGS / PAYSLIP';

		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Payslip/show',$data,'app');

	}

}
