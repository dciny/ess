<?php

  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Employee_master_model extends CI_Model{

	public function employee_details($employeeid){
		$this->db->select('p.employeeid, p.schedule, g.starttime, g.endtime');
		$this->db->from('prlemployeemaster p');
		$this->db->join('groupschedule g','g.groupschedulename = p.schedule');
		$this->db->where('p.employeeid',$employeeid);
		$query = $this->db->get();

		return $query->row();
	}

	

}

