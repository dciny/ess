<div class="border mx-4 leave-dashboard">
	<div class="border-bottom">
		<ul class="d-flex m-0">
			<li class="p-4"><a href="<?php echo base_url('leave') ?>">Dashboard</a></li>
			<li class="p-4"><a href="<?php echo base_url('leave/apply') ?>">Calendar</a></li>
			<li class="p-4 leave-tab-active">Leave Requests</li>
		</ul>
	</div>

	<div class="p-3 leave-dashboard-grid font-14">

		<div class="table-responsive">
			<table class="table table-sm table-striped">
				<thead class="thead-dark">
					<tr>
						<th class='align-middle'><input type="checkbox" name="selectall"></th>
						<th class='align-middle'>Employeeid</th>
						<th class='align-middle'>Requestor</th>
						<th class='align-middle'>Leave Date</th>
						<th class='align-middle'>Leave Type</th>
						<th class='align-middle' colspan="2">Action</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($requests as $request){
						$name = $request->firstname . ' ' . $request->lastname;
					?>

						<tr>
							<td class='align-middle'>
								<?php if($request->approvable && empty($request->rank_status)){ ?>
									<input type='checkbox'>
								<?php } ?>
							</td>
							<td class='align-middle'><?php echo $request->employeeid ?></td>
							<td class='align-middle'><?php echo $name ?></td>
							<td class='align-middle'><a href="<?php echo base_url("leave/show/$request->id") ?>"><?php echo $request->leavedt ?></a></td>
							<td class='align-middle'><?php echo $request->leave_name ?></td>
							<?php if($request->approvable && empty($request->rank_status)){ ?>
								<td>
									<select class='form-control form-control-sm' name="leave_status_id" form="form-<?php echo $request->id ?>">
										<?php
											foreach ($leave_statuses as $status) {
												echo "<option value='$status->id'>$status->status_name</option>";
											}
										?>
									</select>
								</td>
								<td class='align-middle'>
									<form id="form-<?php echo $request->id ?>" action="<?php echo base_url('leave-request-status/create') ?>" method="post">
										<input type="hidden" name="leave_request_id" value="<?php echo $request->id ?>">
										<input type="hidden" name="approver_rank" value="<?php echo $request->approver_rank ?>">
										<button class='btn btn-primary btn-sm'>Submit</button>
									</form>
								</td>
							<?php 
								}elseif(!empty($request->rank_status)){
									echo "<td colspan='2'><span>$request->rank_status</span></td>";
								}else{
									echo "<td colspan='2'>No action</td>";
								} 

							?>
						</tr>

					<?php } ?>
				</tbody>
			</table>
		</div>

	</div>
</div>	
