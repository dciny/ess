<div class="border mx-4 leave-dashboard">
	<div class="border-bottom">
		<ul class="d-flex m-0">
			<li class="p-4"><a href="<?php echo base_url('leave') ?>">Dashboard</a></li>
			<li class="p-4"><a href="<?php echo base_url('leave/apply') ?>">Apply Leave</a></li>
			<?php if($is_approver){ ?>
				<li class="p-4 leave-tab-active">Leave Requests</li>
				<li class="p-4"><a href="<?php echo base_url('leave/approver/leave-count-page') ?>">Leave Count</a></li>
			<?php } ?>
			<?php if($user_details->leave_role_id == 1){ ?>
				<li class="p-4"><a href="<?php echo base_url('leave/tracker') ?>">Leave Tracker</a></li>
			<?php } ?>		
		</ul>
	</div>

	<div class="p-3 font-14">

		<div class="table-responsive">

			<ul class="nav nav-tabs mb-3">
				<li class="nav-item">
					<a class="nav-link" href="<?php echo base_url('leave/requests') ?>">Requires Approval</a>
				</li>
				<li class="nav-item">
					<a class="nav-link active">Completed</a>
				</li>
			</ul>

			<div class="row">
				<div class="col-md-6 mb-2">
					<form action="<?php echo base_url('leave/requests_completed') ?>" method="get">
						<div class="input-group mb-3">
							<input type="text" name="searchtxt" class="form-control form-control-sm" placeholder="Employee Name" value="<?php echo $searchtxt ?>">
							<div class="input-group-append">
								<input type="submit" class="btn btn-sm btn-secondary" value="Search">
							</div>
						</div>
					</form>
				</div>
			</div>

			<table class="table table-sm table-striped">
				<thead class="thead-dark">
					<tr>
						<th class='align-middle'>Employeeid</th>
						<th class='align-middle'>Requestor</th>
						<th class='align-middle'>Leave Date</th>
						<th class='align-middle'>Date Created</th>
						<th class='align-middle'>Leave Type</th>
						<th class='align-middle'>Leave Category</th>
						<th class='align-middle'>Reason</th>
						<th class='align-middle'>Status</th>
					</tr>
				</thead>
				<tbody id="approve-req-tbl">
					<?php foreach ($requests as $request){
						$name = $request->firstname . ' ' . $request->lastname;
					?>

						<tr>
							<td class='align-middle'><?php echo $request->employeeid ?></td>
							<td class='align-middle'><span data-toggle="tooltip" title='<?php echo "Balance: $request->leave_balance | Used: $request->leaves_taken"  ?>'><?php echo $name ?></span></td>
							<td class='align-middle'><a href="<?php echo base_url("leave/show/$request->id") ?>"><?php echo $request->leavedt ?></a></td>
							<td class='align-middle'><?php echo date('M d, Y h:i A',strtotime($request->createdttm)) ?></td>
							<td class='align-middle'><?php echo $request->leave_name ?></td>
							<td class='align-middle'><?php echo ucwords($request->leave_category) ?></td>
							<td class='align-middle'><?php echo $request->leave_reason ?></td>
							<td class='align-middle'><?php echo $request->status_name ?></td>
						</tr>

					<?php } ?>
				</tbody>
			</table>

			<div class="pagination-link">
				<?php echo $pagination_link ?>
			</div>
		</div>

	</div>
</div>	
