<div class="main container-fluid">
	<div class="row mb-5">
		<div class="col-md-12">
			<div class="ml-5">
				<a class="cta-def-btn" href="/">
					<span class="back-icon">Back</span>
				</a>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="grid">
				<div class="box cyan-lin-bg" id="basic">
					<!-- <a href="<?php echo base_url('request/basic-info/new') ?>"><span class="fa fa-gear icon pos-top-right"></span></a> -->
					<h6>Basic Info</h6>
					<table class="table">
						<tbody>
							<tr>
								<td style="width: 120px">Name:</td>
								<td><?php display_name($employee->firstname,$employee->middlename,$employee->lastname) ?></td>
							</tr>
							<tr>
								<td>Employee ID:</td>
								<td>
									<?php display_col($employee->employeeid) ?>
								</td>
							</tr>
							<tr>
								<td>Birthday:</td>
								<td>
									<?php display_date_col($employee->birthdate) ?>
								</td>
							</tr>
						</tbody>
					</table>
				</div>

				<div class="box cyan-lin-bg" id="contact">
					<a href="<?php echo base_url('request/contact/new') ?>"><span class="fa fa-gear icon pos-top-right"></span></a>
					<h6>Contacts</h6>
					<table class="table">
						<tbody>
							<tr>
								<td style="width: 120px">Phone1:</td>
								<td><?php display_col($employee->phone1) ?></td>
							</tr>
							<tr>
								<td>Phone2:</td>
								<td><?php display_col($employee->phone2) ?></td>
							</tr>
							<tr>
								<td>Email1:</td>
								<td><?php display_col($employee->email1) ?></td>
							</tr>
							<tr>
								<td>Email2:</td>
								<td><?php display_col($employee->email2) ?></td>
							</tr>
						</tbody>
					</table>
				</div>

				<div class="box cyan-lin-bg" id="addr">
					<a href="<?php echo base_url('request/address/new') ?>"><span class="fa fa-gear icon pos-top-right"></span></a>
					<h6>Addresses</h6>
					<table class="table">
						<tbody>
							<tr>
								<td style="width: 120px">Address1:</td>
								<td><?php display_col($employee->address1) ?></td>
							</tr>
							<tr>
								<td>Address2:</td>
								<td><?php display_col($employee->address2) ?></td>
							</tr>
						</tbody>
					</table>
				</div>	

				<div class="box yellow-lin-bg" id="gov">
					<!-- <span class="fa fa-gear pos-top-right"></span> -->
					<h6>Government Info</h6>
					<table class="table">
						<tbody>
							<tr>
								<td style="width: 120px">SSS:</td>
								<td><?php display_col($employee->ssnumber) ?></td>
							</tr>
							<tr>
								<td>Pagibig/HDMF:</td>
								<td><?php display_col($employee->hdmfnumber) ?></td>
							</tr>
							<tr>
								<td>Philhealth:</td>
								<td><?php display_col($employee->phnumber) ?></td>
							</tr>
							<tr>
								<td>BIR/Tin:</td>
								<td><?php display_col($employee->taxactnumber) ?></td>
							</tr>
						</tbody>
					</table>
				</div>	

				<div class="box blue-lin-bg" id="work">
					<a href="<?php echo base_url('request/work/new') ?>"><span class="fa fa-gear icon pos-top-right"></span></a>
					<h6>Work Details</h6>
					<div class="table-responsive">
						<table class="table">
							<tbody>
								<tr>
									<td style="width: 120px">Position:</td>
									<td><?php display_col($employee->position) ?></td>
								</tr>
								<tr>
									<td style="width: 120px">Monthly Rate:</td>
									<td><?php display_col($employee->periodrate) ?></td>
								</tr>
								<tr>
									<td style="width: 120px">Hourly Rate:</td>
									<td><?php display_col($employee->hourlyrate) ?></td>
								</tr>
								<tr>
									<td colspan="2">
										<div class="form-group">
											<label>Work Schedule:</label>
											<?php if(isset($work_sched)){  ?>
												<table class="table table-sm table-bordered">
													<thead class="thead-light">
														<tr class="text-center">
															<th>Start Time</th>
															<th>End Time</th>
															<th>Lunch Start</th>
															<th>Lunch End</th>
															<th>Mon</th>
															<th>Tue</th>
															<th>Wed</th>
															<th>Thu</th>
															<th>Fri</th>
															<th>Sat</th>
															<th>Sun</th>
														</tr>
													</thead>
													<tbody>
														<tr class="text-center">
															<td><?php echo $work_sched->starttime ?></td>
															<td><?php echo $work_sched->endtime ?></td>
															<td><?php echo $work_sched->lunchstart ?></td>
															<td><?php echo $work_sched->lunchend ?></td>
															<td>
																<input type="checkbox" <?php echo $work_sched->mon == 1 ? 'checked' : '' ?> disabled>
															</td>
															<td>
																<input type="checkbox" <?php echo $work_sched->tue == 1 ? 'checked' : '' ?> disabled>
															</td>
															<td>
																<input type="checkbox" <?php echo $work_sched->wed == 1 ? 'checked' : '' ?> disabled>
															</td>
															<td>
																<input type="checkbox" <?php echo $work_sched->thu == 1 ? 'checked' : '' ?> disabled>
															</td>
															<td>
																<input type="checkbox" <?php echo $work_sched->fri == 1 ? 'checked' : '' ?> disabled>
															</td>
															<td>
																<input type="checkbox" <?php echo $work_sched->sat == 1 ? 'checked' : '' ?> disabled>
															</td>
															<td>
																<input type="checkbox" <?php echo $work_sched->sun == 1 ? 'checked' : '' ?> disabled>
															</td>
														</tr>
													</tbody>
												</table>
											<?php 
												}else{
													echo '(schedule not set)';
												} 
											?>
										</div>
									</td>
								</tr>	
							</tbody>
						</table>	
					</div>
				</div>	
			</div>	
		</div>
	</div>
</div>