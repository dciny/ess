<div class="main container-fluid">
	<div class="row mb-5">
		<div class="col-md-12">
			<div class="ml-5">
				<a class="cta-def-btn" href="<?php echo base_url('employee/show'); ?>">
					<span class="back-icon">Back</span>
				</a>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 p-0">
			<div class="mx-5">
				<p class="text-danger">* Send change request to HR</p>
			</div>
			<div class="box cyan-lin-bg box-min-size mx-5">
				<h5>Contact</h5>
				<form id="req-form" action="/request/contact/create" method="post">
					<div class="table-responsive">
						<table class="table">
							<tbody>
								<tr>
									<td style="width: 120px">Phone1:</td>
									<td>
										<input type="text" 
										       name="phone1" 
											   class="form-control <?php if(form_error('phone1')) echo 'is-invalid' ?>" 
											   value="<?php echo $employee->phone1 ?>">
										<?php echo form_error('phone1'); ?>
									</td>
								</tr>
								<tr>
									<td style="width: 120px">Phone2:</td>
									<td>
										<input type="text" 
										       name="phone2" 
										       class="form-control <?php if(form_error('phone2')) echo 'is-invalid' ?>" 
										       value="<?php echo $employee->phone2 ?>">
										<?php echo form_error('phone2'); ?>
									</td>
								</tr>
								<tr>
									<td style="width: 120px">Email1:</td>
									<td>
										<input type="text" 
										       name="email1" 
										       class="form-control <?php if(form_error('email1')) echo 'is-invalid' ?>" 
										       value="<?php echo $employee->email1 ?>">
										<?php echo form_error('email1'); ?>
									</td>
								</tr>
								<tr>
									<td style="width: 120px">Email2:</td>
									<td>
										<input type="text" 
										       name="email2" 
										       class="form-control <?php if(form_error('email2')) echo 'is-invalid' ?>" 
										       value="<?php echo $employee->email2 ?>">
										<?php echo form_error('email2'); ?>
									</td>
								</tr>
								<tr class="text-right">
									<td colspan="2">
										<input type="submit" name="submit" value="Send Change Request" class="btn cta-btn cyan-bg-btn">
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>