<div class="container">
	<div class="row">
		<div class="col-md-12">
			<form action="<?php echo base_url('announcement/create') ?>" method="post">
				<div class="form-group">
					<label>Title</label>
					<input type="text" name="announcement[title]" class="form-control">
				</div>
				<div class="form-group">
					<label>Body</label>
					<textarea class="ckeditor5" name="announcement[body]"></textarea>
				</div>
				<div class="d-flex" style="gap: 1rem">
					<input type="submit" class="btn btn-primary" value="Create">
					<span class="btn btn-secondary" onclick="window.history.back()">Cancel</span>
				</div>
			</form>
		</div>
	</div>
</div>