<?php
  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Schedule extends CI_Controller {

	public function __construct(){

		parent::__construct();
		
		$this->load->model('dashboard_model');
		$this->load->model('employee_model');
		$this->load->model('finalhours_model','finalhours');
	}

/*	public function _remap($method, $params = []){
		$login_session = $this->session->userdata('logged_in');

		if(isset($login_session)){
			
			$is_password_change =$this->session->userdata['logged_in']['is_password_change'];
			if($is_password_change == 0){
				redirect('auth/change-password', 'location');
			}

			$this->$method($params);
		}else{
			redirect('auth/index', 'location');
		}
	}*/


	// public function sched(){
	// 	$username = $this->session->userdata['logged_in']['employeeid'];
 // 		$emp['employee'] = $this->dashboard_model->get_info($username);
 // 		$emp['users'] = $this->employee_model->get_user_id($username);

 // 		$emp['curr_year'] = $this->finalhours->get_year();
 // 		$month = $this->input->post('month');
 // 		$year = $this->input->post('year');
 // 		$pay = $this->input->post('pay');

 // 		// $month = '9';
 // 		// $year = '2020';
 // 		// $pay = '1:16:31';

 // 		$sched = [];
 // 		if(isset($month) && !empty($month)){

	//  		list($dateMinus, $start, $end) = explode(":", $pay);
	//  		$update= $month - $dateMinus;
	//  		if ($update==0) {
	// 			$update='12';
	// 		}

	// 		$dateStart = $update."-".$start;
	// 		$dateEnd = $update."-".$end;

	// 		$searchStart = $year . "-" . $dateStart;
	// 		$searchEnd = $year . "-" . $dateEnd;

	// 		$sched = $this->finalhours->get_schedule($username, $searchStart, $searchEnd);
	// 	}
		
	// 	$emp['sched'] = $sched;
	// 	$emp['year'] = isset($year) && !empty($year) ? $year : '';
	// 	$emp['month'] = isset($month) && !empty($month) ? $month : '1';
	// 	$emp['pay'] = isset($pay) && !empty($pay) ? $pay : '1:16:31';

	// 	$this->load->library('layouts'); // load layout library
	// 	$this->layouts->view('Schedule/sched',$emp,'app');
		
	// }

	public function sched(){
		$username = $this->session->userdata['logged_in']['employeeid'];
 		$emp['employee'] = $this->dashboard_model->get_info($username);
 		$emp['users'] = $this->employee_model->get_user_id($username);

 		$emp['curr_year'] = $this->finalhours->get_year();
 		$month = $this->input->post('month');
 		$year = $this->input->post('year');
 		$pay = $this->input->post('pay');

 		// $month = '9';
 		// $year = '2020';
 		// $pay = '1:16:31';

 		$sched = [];
 		if(isset($month) && !empty($month)){

	 		if($pay == '10'){
	 			$dateStart = 16;
	 			
	 			if($month == 1){
	 				$calc_month = 12;
	 				$calc_year = $year - 1;
	 			}else{
	 				$calc_month = $month - 1;
	 				$calc_year = $year;
	 			}

	 			$base_dt = $calc_year . '-' . $calc_month . '-' . '01';
	 			$dt = new DateTime($base_dt);
	 			$dateEnd = $dt->format( 't' );

	 		}else{
	 			$dateStart = 1;
	 			$dateEnd = 15;
	 			$calc_year = $year;
	 			$calc_month = $month;
	 		}

			$searchStart = $calc_year . "-" . $calc_month . "-" . $dateStart;
			$searchEnd = $calc_year . "-" . $calc_month . "-" . $dateEnd;

			$sched = $this->finalhours->get_schedule($username, $searchStart, $searchEnd);
		}
		
		$emp['sched'] = $sched;
		$emp['year'] = isset($year) && !empty($year) ? $year : '';
		$emp['month'] = isset($month) && !empty($month) ? $month : '1';
		$emp['pay'] = isset($pay) && !empty($pay) ? $pay : '10';

		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Schedule/sched',$emp,'app');
		
	}


}
