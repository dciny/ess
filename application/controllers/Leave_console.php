<?php
  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Leave_console extends CI_Controller {

	public function __construct(){

		parent::__construct();
		
		$this->load->model('employee_model');
		$this->load->helper('url');
	}

	
	public function index(){
		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Leave_console/index',[],'leave');
	}

	public function calendar($date="2020-12-12"){
		date_default_timezone_set("Asia/Kuala_Lumpur");

		$date = !empty($date) ? $date : date('Y-m-d');
		$employeeid = $this->session->userdata['logged_in']['employeeid'];

		$data['days'] = ['Su','Mo','Tu','We','Th','Fr','Sa'];

		// current month days: loop through currdt_no_days and add number of days in first_day
		$last_day = date('Y-m-t',strtotime($date));
		$last_day_arr = getdate(strtotime($last_day));
		$curr_first_day = date('Y-m-01',strtotime($date));
		$data['curr_last_day_idx'] = $last_day_arr['wday'];
		$data['currdt_no_days'] = date('t',strtotime($date));
		$data['curr_mo'] = $curr_first_day;
		$data['curr_date'] = date('Y-m-d',strtotime($date));
		
		// previous month days: loop through prevmo_no_days and add number of days in prevmo_strt_day
		$first_day_arr = getdate(strtotime($curr_first_day));
		$data['prevmo_no_days'] = $first_day_arr['wday'];
		$data['prevmo_strt_day'] = date('Y-m-d', strtotime("-{$data['prevmo_no_days']} days", strtotime($curr_first_day)));
		$data['prevmo_first_day'] = date('Y-m-01', strtotime("-1 months", strtotime($curr_first_day)));

		// next month
		$data['nxtmo_first_day'] = date('Y-m-01', strtotime("1 months", strtotime($curr_first_day))); // add 1 month to current month
		$nxtmo = getdate(strtotime($data['nxtmo_first_day']));
		$nxtmo_first_day_idx = $nxtmo['wday'];
		$data['nxtmo_no_days'] = 7 - $nxtmo_first_day_idx; // 7 for 7 days

		///////////////////////////////////////////////////////////////////////////////
		$data['employee'] = $this->employee_model->get_employee_details($employeeid);
		$data['users'] = $this->employee_model->get_user_id($employeeid);
		$data['header'] = 'Calendar';
		$data['css'] = 'calendar.css';
		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Leave_console/calendar',$data,'app');
	}

}
