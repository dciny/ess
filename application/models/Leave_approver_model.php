<?php

  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Leave_approver_model extends CI_Model{

	public function get_leave_approvers(){
		return $this->db->select('la.employeeid, p.firstname, p.lastname')
						->distinct()
		                ->from('leave_approvers la')
		                ->join('approver_hierarchies ah','ah.leave_approver_id = la.id')
		                ->join('prlemployeemaster p','p.employeeid = la.employeeid','left')
		                ->where('ah.is_active',1)
		                ->get()
		                ->result();
	}

	public function is_leave_approver($employeeid = 0){
		$leave_approvers = $this->get_leave_approvers();

		foreach ($leave_approvers as $approver) {
			if($approver->employeeid == $employeeid)
				return true;
		}

		return false;
	}

	public function get_list_approvee($employeeid = 0){
		return $this->db->select('p1.employeeid as approver_eid,
							     p1.firstname as approver_firstname, 
						         p1.lastname as approver_lastname,
						         p2.employeeid,
						         p2.firstname,
						         p2.lastname,
						         lass.leave_hierarchy_id,
						         lh.rule_name')
		                ->from('approver_hierarchies ah')
		                ->join('leave_approvers lapp','lapp.id = ah.leave_approver_id')
		                ->join('leave_assignments lass','lass.leave_hierarchy_id = ah.leave_hierarchy_id')
		                ->join('prlemployeemaster p1','p1.employeeid = lapp.employeeid','left')
		                ->join('prlemployeemaster p2','p2.employeeid = lass.employeeid','left')
		                ->join('leave_hierarchies lh','lh.id = lass.leave_hierarchy_id','left')
		                ->where('lapp.employeeid',$employeeid)
		                ->order_by('lh.rule_name ASC, p2.lastname ASC')
		                ->get()
		                ->result();
	}

	public function get_grouped_approvees($employeeid = 0){
		$approvees = $this->get_list_approvee($employeeid);
		$grouped_approvees = [];

		foreach ($approvees as $approvee) {
			// if key not exist, create an array key and initialize it to empty array
			if(!array_key_exists($approvee->rule_name,$grouped_approvees)){
				$grouped_approvees[$approvee->rule_name] = [];
			}

			array_push($grouped_approvees[$approvee->rule_name], $approvee);
		}

		return $grouped_approvees;
	}

	public function leave_count_per_date($from = null, $to = null, $employeeids = []){
		if(empty($from))
			throw new Exception('From cannot be empty');
		if(empty($employeeids))
			throw new Exception('No selected employee');

		if(empty($to))
			$to = $from;


		$employeeids = "'" . implode("','", $employeeids) . "'";

		$main_table    = "(select distinct lr.leavedt
						  from leave_requests lr
						  left join leave_assignments la on la.employeeid = lr.employeeid
						  where lr.leavedt >= '$from'
						  and lr.leavedt <= '$to'
						  and lr.employeeid in($employeeids)
						  ) as main";


		$pending_table = "(select lr.leavedt, count(1) as status_count
						  from leave_requests lr
						  left join leave_assignments la on la.employeeid = lr.employeeid
						  where lr.overall_status = 3
						  and lr.leavedt >= '$from'
						  and lr.leavedt <= '$to'
						  and lr.employeeid in($employeeids)
						  group by lr.leavedt) as pending";

		$approved_table = "(select lr.leavedt, count(1) as status_count
						  from leave_requests lr
						  left join leave_assignments la on la.employeeid = lr.employeeid
						  where lr.overall_status = 5
						  and lr.leavedt >= '$from'
						  and lr.leavedt <= '$to'
						  and lr.employeeid in($employeeids)
						  group by lr.leavedt) as approved";

		return $this->db->select('main.leavedt, COALESCE(pending.status_count, 0) as pending_status_count, COALESCE(approved.status_count, 0) as approved_status_count')
		                ->from($main_table)
		                ->join($pending_table,'main.leavedt = pending.leavedt','left',null)
		                ->join($approved_table,'main.leavedt = approved.leavedt','left',null)
		                ->order_by('main.leavedt','ASC')
		                ->get()
		                ->result();

	}
}

