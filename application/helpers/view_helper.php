<?php

function display_name($fname,$mname,$lname){
	$name = ['fname'=>$fname,'mname'=>$mname,'lname'=>$lname];

	if(!isset($fname) || empty($fname)){
		unset($name['fname']);
	}
	if(!isset($mname) || empty($mname)){
		unset($name['mname']);
	}
	if(!isset($lname) || empty($lname)){
		unset($name['lname']);
	}

	echo join(' ',$name);
}

function display_date_col($col){
	echo isset($col) && !empty($col) ? date_format(date_create($col),'F d, Y') : '(none)';
}

function display_datetime_col($col){
	echo isset($col) && !empty($col) ? date_format(date_create($col),'M d, Y h:i:s A') : '(none)';
}

function display_col($col){
	echo isset($col) && !empty($col) ? $col : '(none)';
}

function toggle_val($ind,$curr,$opt1='',$opt2=''){
	// $ind - module indicator
	// $curr - current module
	return $ind == $curr ? $opt1 : $opt2;
}

function approver_name($id){
	$this->load->model('approver_model');

	$approver = $this->approver_model->approver_details($id);

	return implode(' ', [$approver->firstname, $approver->lastname]);
}