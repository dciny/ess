<?php

	$msg = '';
	$border_color = '';
	$box_color = '';

	if($this->session->flashdata('error')){
		$msg = $this->session->flashdata('error');
		$border_color = 'border-danger';
		$box_color = 'msg-box-danger';
		$text_color = 'text-danger';
	}elseif($this->session->flashdata('success') != null){
		$msg = $this->session->flashdata('success');
		$border_color = 'border-success';
		$box_color = 'msg-box-success';
		$text_color = 'text-success';
	}

?>


<div class="box">
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<ul class="navbar-nav">
			<li class="nav-item">
				<a class="nav-link active" href="<?php echo base_url() ?>">Home</a>
			</li>
			<li class="nav-item">
				<a class="nav-link active" href="mailto:hr@digicononline.com">Contact HR</a>
			</li>
		</ul>
	</nav>

	<div class="mt-4">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-2 mb-3">
					<div class="font-14">
						<ul class="list-group">
							<li class="list-group-item text-info-comp bg-light">Create user</li>
							<li class="list-group-item text-light bg-info-comp"><a href="<?php echo base_url('emergency-control/edit-password') ?>">Update user password</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-8 col-xl-4 col-lg-6">
					<form action="<?php echo base_url('emergency-control/create-user') ?>" method="post">
						<div class="bg-info-comp text-light p-2">New User Form</div>
						<div class="font-14 border p-3 bg-light">

							<?php if(!empty($msg)){ ?>
								<div class="mb-3 font-14 pos-relative">
									<span class="fa fa-close pos-absolute btn-close-top-right <?php echo $text_color ?> close-parent"></span>
									<div class="msg-box <?php echo $box_color ?> border-left border-10 <?php echo $border_color ?>">
										<p class="m-0"><?php echo $msg ?></p>
									</div>
								</div>
							<?php } ?>

							<div class="form-group">
								<label class="text-info-comp">Username</label>
								<input type="number" name="username" class="form-control <?php if(form_error('username')) echo 'is-invalid' ?>" value="<?php echo set_value('username'); ?>">
								<?php echo form_error('username'); ?>
							</div>
							<div class="form-group">
								<label class="text-info-comp">Password</label>
								<input type="password" name="password" class="form-control <?php if(form_error('password')) echo 'is-invalid' ?>" value="<?php echo set_value('password'); ?>">
								<?php echo form_error('password'); ?>
							</div>
							<div class="form-group">
								<label class="text-info-comp">Leave Role</label>
								<select name="leave_role_id" class="form-control <?php if(form_error('leave_role_id')) echo 'is-invalid' ?>">
									<?php foreach ($leave_roles as $role) {
										echo "<option value='$role->id'>$role->role_name</option>";
									} ?>
								</select>
								<?php echo form_error('leave_role_id'); ?>
							</div>
							<div class="form-group">
								<input type="submit" name="submit" class="btn btn-info-comp" value="Submit">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>




