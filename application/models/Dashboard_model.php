<?php

  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_model extends CI_Model{

	public function get_info($username){
		$this->db->select('*');
		$this->db->from('prlemployeemaster');
		$this->db->where('employeeid',$username);
		$query = $this->db->get();
		
		if($query->num_rows() > 0){
			return $query->row();
		}
	}

}

