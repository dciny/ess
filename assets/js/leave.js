$(document).ready(function(){
	$(document).on('click','.nav-toggle',function(){
		$('.top-nav-content').css({'display':'block'});
	});


	$(document).on('click','.nav-close',function(){
		$('.top-nav-content').css({'display':'none'});
	});


	$(document).on('click','.grp-header',function(){
		var caret = $(this).find('[data-stat]'); 
		var stat = caret.data('stat');
		var grp_body = $(this).siblings('.grp-body');

		if(stat == 'hidden'){
			grp_body.css({'display':'block'});
			caret.removeClass('fa-caret-right').addClass('fa-caret-down');
			caret.data('stat','active');
		}else{
			grp_body.css({'display':'none'});
			caret.removeClass('fa-caret-down').addClass('fa-caret-right');
			caret.data('stat','hidden');
		}
	});


	$(document).on('click','.jrem-btn',function(){
		var id = $(this).data('id');
		var modal = $('.modal-bg');

		modal.find("[name='id']").val(id);
		// modal.css({'display':'block'});
		modal.fadeIn(300);
	});

	$(document).on('click','.jcancel-btn',function(){
		var modal = $(this).parents('.modal-bg');

		modal.find("[name='id']").val('');
		modal.fadeOut(300);
	});

	$(document).on('click','.jadd-aprvr-hrcy',function(){
		var parent_container = $(this).parents('.jparent-cntr');
		var approver_id = parent_container.data('id');
		var name = parent_container.find('.fname').text().trim();
		var approver_list = $('#japrvr-hrcy-list');

		var approver_template = approver_tbl_entry(approver_id,name);

		parent_container.remove();
		approver_list.append(approver_template);

		// update options.
		// this will only update the options before the new element was added
		approver_list.children().each(function(){
			var select_el = $(this).find('select');
			var select_val = select_el.val();
			var new_option = update_options(select_val);

			select_el.empty()
			         .append(new_option);
		});
	});

	$(document).on('click','.rem-aprvr',function(){
		var parent_container = $(this).parents('tr');
		var approver_id = parent_container.find("[name='approver_ids[]']").val();
		var name = parent_container.find('.fname').text().trim();
		var approvers_list = $('#japrvr-list');
		var approver_template = approver_li_entry(approver_id,name);

		var is_appended = false;
		approvers_list.children().each(function(){
			var node_name = $(this).find('.fname').text().trim();
			if(name <= node_name){
				$(this).before(approver_template);
				is_appended = true;
				return false;
			}
		});

		if(!is_appended){
			approvers_list.append(approver_template);
		}

		parent_container.remove();

		$('#japrvr-hrcy-list').children().each(function(){
			var select_el = $(this).find('select');
			var new_option = update_options(1);

			select_el.empty()
			         .append(new_option);
		});
		
	});


	function approver_li_entry(approver_id,name){
		return `
			<li class="list-group-item p-0 jparent-cntr" data-id="{approver_id}">
				<table class="table table-borderless table-sm fontsize-14 mb-0">
					<tbody>
						<tr class="text-center">
							<td class="align-middle fname" style="width: 362px">
								{name}
							</td>
							<td><button class="btn btn-sm btn-success jadd-aprvr-hrcy">Add</button></td>
						</tr>	
					</tbody>
				</table>
			</li>
		`.replace('{approver_id}',approver_id)
		 .replace('{name}',name)
		 .trim();
	}

	function approver_tbl_entry(approver_id,name){
		var options = generate_options();

		return `
			<tr>
				<td class="align-middle fname">{full_name}</td>
				<td>
					<select name="ranks[{id}]" class="form-control form-control-sm">
						{options}
					</select>
					<input type="hidden" name="approver_ids[]" value="{id}">
				</td>
				<td class="borderless"><button class="btn btn-sm btn-danger rem-aprvr">Remove</button></td>
			</tr>
		`.replace('{options}',options)
		 .replace('{full_name}',name)
		 .replace(/{id}/g,approver_id)
		 .trim();
	}

	function generate_options(){
		var ctr = approver_list_ctr();

		var i;
		var str = '';
		for(i = 1; i <= ctr; i++){
			str = str + "<option value='{val}'>{val}</option>".replace(/{val}/g,i);
		}
		str = str + "<option value='{val}' selected>{val}</option>".replace(/{val}/g,i);

		return str.trim();
	}

	function update_options(opt_val){
		var ctr = approver_list_ctr();

		var i;
		var str = '';
		for(i = 1; i <= ctr; i++){
			var selected = (i == opt_val) ? 'selected' : '';
			str = str + "<option value='{val}' {selected}>{val}</option>".replace(/{val}/g,i).replace('{selected}',selected);
		}

		return str.trim();
	}

	function approver_list_ctr(){
		return $('#japrvr-hrcy-list').children().length;
	}


	$(document).on('change','.jleave-select',function(){
		var leave_type_id = $(this).val();

		$.ajax({
			url: '/leave_assignment/fill_unassigned_emp',
			method: 'post',
			data: {
				leave_type_id: leave_type_id
			},
			success: function(response){
				var options = '';

				response.forEach(function(item){
					var name = [item.firstname, item.lastname].join(' ');
					options = options + `<option value="{empid}">{name}</option>`.replace('{empid}',item.employeeid).replace('{name}',name);	
				});

				var html = `<select name="employeeids[]" class="form-control form-control-sm selectpicker border" multiple data-live-search="true">{options}</select>`.replace('{options}',options);
				$('.junassigned-emp').html(html);
				$('.selectpicker').selectpicker('refresh');
			}
		});
	});


	$(document).on('change','#jhrule',function(){
		$(this).parents('form').submit();
	});

});