<?php
  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Leave_request_status extends CI_Controller {

	public function __construct(){

		parent::__construct();
		
		$this->load->model('employee_model');
		$this->load->model('leave_request_model','leave_request');
		$this->load->model('leave_assignment_model','leave_assignment');
		$this->load->model('approver_hierarchy_model','approver_hierarchy');
		$this->load->model('leave_emaillogs_model','leave_emaillogs');
		$this->load->model('employee_master_model','employee_master');

		$this->load->helper('url');
	}

	
	public function _remap($method, $params = []){
		$login_session = $this->session->userdata('logged_in');

		if(isset($login_session)){
			
			$is_password_change =$this->session->userdata['logged_in']['is_password_change'];
			if($is_password_change == 0){
				redirect('auth/change-password', 'location');
			}

			$this->$method($params);
		}else{
			redirect('auth/index', 'location');
		}
	}


	public function create(){
		date_default_timezone_set("Asia/Kuala_Lumpur");

		$leave_request_id = $this->input->post('leave_request_id');
		$approver_rank = $this->input->post('approver_rank');
		$leave_status_id = $this->input->post('leave_status_id');
		$leave_category = $this->input->post('leave_category');
		// $leave_category_orig = $this->input->post('leave_category_orig');
		$employeeid = $this->input->post('employeeid');


		$data = [
			'leave_request_id' => $leave_request_id,
			'approver_rank' => $approver_rank,
			'leave_status_id' => $leave_status_id,
			'createdttm' => date('Y-m-d'),
			'updatedttm' => date('Y-m-d')
		];


		$this->db->insert('leave_request_statuses', $data);
		$aff_rows = $this->db->affected_rows();

		if($aff_rows > 0){
			// return 1 leave credit if category is paid and status is not approved
			if($leave_category == 'paid' && $leave_status_id != 5){
				$leave_details = $this->leave_assignment->get_leave_details($employeeid);
				$leave_credits = $leave_details->leave_credits + 1;
				$this->leave_assignment->update_duration($employeeid,$leave_credits);
			}

			// update overall status
			$overall_status = $this->get_overall_status($leave_request_id);
			$this->leave_request->update_overallstatus($leave_request_id, $overall_status);


			// log to email logs if approved and log to timelog if approver is last approver
			if($leave_status_id == 5){
				$email_log_params = $this->approver_hierarchy->get_next_approver($leave_request_id, $approver_rank);
				if(isset($email_log_params)){
					$this->leave_emaillogs->create($email_log_params);
				}

				// log to timelog if approver is last approver
				$max_approver_rank = $this->approver_hierarchy->get_last_approver_rank($leave_request_id);
				if($approver_rank == $max_approver_rank){
					$this->log_to_timelog($leave_request_id);
				}
			}
		}

		redirect('leave/requests', 'location');

	}

	public function bulk_create(){
		date_default_timezone_set("Asia/Kuala_Lumpur");
		$req = $this->input->post('req');

		foreach ($req as $request) {
			$data = [
				'leave_request_id' => $request['leave_request_id'],
				'approver_rank' => $request['approver_rank'],
				'leave_status_id' => $request['leave_status_id'],
				'createdttm' => date('Y-m-d'),
				'updatedttm' => date('Y-m-d')
			];

			$this->db->insert('leave_request_statuses', $data);
			$aff_rows = $this->db->affected_rows();

			if($aff_rows > 0){
				$leave_category = $request['leave_category'];
				$employeeid = $request['employeeid'];

				// return 1 leave credit if category is paid and status is not approved
				if($leave_category == 'paid' && $leave_status_id != 5){
					$leave_details = $this->leave_assignment->get_leave_details($employeeid);
					$leave_credits = $leave_details->leave_credits + 1;
					$this->leave_assignment->update_duration($employeeid,$leave_credits);
				}

				// update overall status
				$overall_status = $this->get_overall_status($request['leave_request_id']);
				$this->leave_request->update_overallstatus($request['leave_request_id'], $overall_status);

				// log to email logs if approved
				if($request['leave_status_id'] == 5){
					$email_log_params = $this->approver_hierarchy->get_next_approver($request['leave_request_id'], $request['approver_rank']);
					if(isset($email_log_params)){
						$this->leave_emaillogs->create($email_log_params);
					}

					// log to email logs if approved and log to timelog if approver is last approver
					$max_approver_rank = $this->approver_hierarchy->get_last_approver_rank($request['leave_request_id']);
					if($request['approver_rank'] == $max_approver_rank){
						$this->log_to_timelog($request['leave_request_id']);
					}
				}
			}
		}

		// redirect('leave/requests', 'location');
		return true;
	}

	private function log_to_timelog($leave_request_id){
		$leave_request = $this->leave_request->details($leave_request_id);
		$employee_info = $this->employee_master->employee_details($leave_request->employeeid);

		// if endtime is lesser than startime, means shift ends on the next day
		$date_out = $leave_request->leavedt;
		// parse time to get the hours, minutes, and seconds
		$starttime = date_parse($employee_info->starttime);
		$endtime = date_parse($employee_info->endtime);

		// get the total seconds
		$starttm_sec = ($starttime['hour'] * 3600) + ($starttime['minute'] * 60) + $starttime['second'];
		$endtm_sec = ($endtime['hour'] * 3600) + ($endtime['minute'] * 60) + $endtime['second'];
		
		if($endtm_sec < $starttm_sec){
			$date_out = date('Y-m-d', strtotime($date_out . ' +1 day'));
		}

		$data = array(
			'dates' => $leave_request->leavedt,
			'shiftday' => date('D',strtotime($leave_request->leavedt)),
			'status' => $leave_request->leave_name,
			'userid' => $leave_request->employeeid,
			'skedin' => $leave_request->leavedt . ' ' . $employee_info->starttime,
			'skedout' => $date_out . ' ' . $employee_info->endtime,
			'timein' => '0000-00-00 00:00:00',
    		'timeout' => '0000-00-00 00:00:00',
    		'earlytimedate' => '0000-00-00 00:00:00',
    		'startshift' => $leave_request->leavedt . ' ' . $employee_info->starttime,
    		'endshift' => $date_out . ' ' . $employee_info->endtime,
    		'otstart' => '0000-00-00 00:00:00',
    		'otend' => '0000-00-00 00:00:00',
    		'thisdaysshift' => $employee_info->schedule
		);

		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, 'http://local.digicononline.com/api/?controller=timelog');                                                                    
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
		

		$result = curl_exec($ch);
	}

	// private function get_overall_status($leave_request_id){
	// 	// $status = 'For Approval';
	// 	$lr_status = $this->leave_request->get_leave_status($leave_request_id);
	// 	// $last_approver_stat = end($lr_status);
	// 	// $status = empty($last_approver_stat->status_name) ? 'For Approval' : $last_approver_stat->status_name;

	// 	$status = '';
	// 	foreach ($lr_status as $s) {
	// 		// $s->status_name
	// 		if(empty($s->status_name)){
	// 			$status = 3; break; // For Approval
	// 		}

	// 		if($s->status_name != 'Approved'){
	// 			$status = $s->id; break;
	// 		}
	// 	}

	// 	if(empty($status)){
	// 		$status = 5;  // Approved
	// 	}



	// 	return $status;
	// }

	private function get_overall_status($leave_request_id){

		$lr_statuses = $this->leave_request->get_leave_status($leave_request_id);


		$status = '';
		foreach($lr_statuses as $lr_status){
			if(empty($lr_status->leave_status_id) || $lr_status->leave_status_id == 3){
				$status = 3;
			}elseif($lr_status->leave_status_id != 5){
				$status = $lr_status->leave_status_id; 
				break;
			}	
		}

		if(empty($status)){
		  $status = 5;
		}



		return $status;
	}

}
