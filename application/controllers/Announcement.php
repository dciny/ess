<?php
  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Announcement extends CI_Controller {

	public function __construct(){

		parent::__construct();
		
		$this->load->model('employee_model');
		$this->load->model('user_model');
		$this->load->model('announcement_model');
		$this->load->helper('url');
	}

	
	public function index(){
		$employeeid = $this->session->userdata['logged_in']['employeeid'];
		$data['user'] = $this->user_model->get_details($employeeid);
		$data['employee'] = $this->employee_model->get_employee_details($employeeid);
		$data['users'] = $this->employee_model->get_user_id($employeeid);

		$data['announcements'] = $this->announcement_model->all();
		$data['latest_announcement'] = empty(!$data['announcements']) ? $data['announcements'][0] : []; // first element of announcement is latest since it is sorted in descendin order
		$data['current'] = empty(!$data['latest_announcement']) ? $data['latest_announcement']->id : 0;

		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Announcement/index',$data,'app');
	}

	public function all(){
		$data['notice'] = $this->input->get('notice');
		$data['action'] = $this->input->get('action');
		$data['announcements'] = $this->announcement_model->all();
		$employeeid = $this->session->userdata['logged_in']['employeeid'];
		$data['employee'] = $this->employee_model->get_employee_details($employeeid);
		$data['users'] = $this->employee_model->get_user_id($employeeid);
		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Announcement/all',$data,'app');
	}


	public function new(){
		$employeeid = $this->session->userdata['logged_in']['employeeid'];
		$data['employee'] = $this->employee_model->get_employee_details($employeeid);
		$data['users'] = $this->employee_model->get_user_id($employeeid);
		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Announcement/new',$data,'app');
	}


	public function create(){
		date_default_timezone_set('Asia/Kuala_Lumpur');
		$announcement = $this->input->post('announcement');
		$announcement['createdttm'] = date('Y-m-d H:i:s');
		$announcement['updatedttm'] = date('Y-m-d H:i:s');
		$this->db->insert('announcements',$announcement);
		$id = $this->db->insert_id();

		redirect("announcement/$id");
	}

	public function show($id){
		$employeeid = $this->session->userdata['logged_in']['employeeid'];
		$data['employee'] = $this->employee_model->get_employee_details($employeeid);
		$data['users'] = $this->employee_model->get_user_id($employeeid);

		$data['announcement'] = $this->announcement_model->find_by_id($id);
		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Announcement/show',$data,'app');
	}

	// show page for ajax content
	public function display($id){
		$employeeid = $this->session->userdata['logged_in']['employeeid'];
		$data['user'] = $this->user_model->get_details($employeeid);
		$data['announcement'] = $this->announcement_model->find_by_id($id);
		$this->load->view('Announcement/display', $data);
	}

	public function edit($id){
		$employeeid = $this->session->userdata['logged_in']['employeeid'];
		$data['employee'] = $this->employee_model->get_employee_details($employeeid);
		$data['users'] = $this->employee_model->get_user_id($employeeid);

		$data['announcement'] = $this->announcement_model->find_by_id($id);
		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Announcement/edit',$data,'app');
	}


	public function update(){
		date_default_timezone_set('Asia/Kuala_Lumpur');
		$announcement = $this->input->post('announcement');
		$announcement['updatedttm'] = date('Y-m-d H:i:s');

		$id = $announcement['id'];
		unset($announcement['id']);

		$this->db->where('id', $id);
		$this->db->update('announcements', $announcement);

		redirect("announcement/$id");
		
	}


	public function destroy($id){
		$this->announcement_model->delete_by_id($id);
	}

}
