<?php

class Layouts{
	private $CI;

	private $header = 'EMPLOYEE SELF SERVICE PORTAL';
	private $css = '';

	public function __construct(){
		$this->CI =& get_instance();
	}

	public function view($view_name, $params = array(), $layout = 'default'){
		$rendered_view = $this->CI->load->view($view_name, $params, TRUE);
		if(isset($params['header']))
			$this->header = $params['header'];
		if(isset($params['css']))
			$this->css = $params['css'];

		$params['rendered_view'] = $rendered_view; // store this in rendered_view variable. this variable is use to render the content on the layout
		$params['header_title'] = $this->header;
		$params['css'] = $this->css;

		$this->CI->load->view('Layouts/'. $layout, $params); // pass params so that variables in controller is accessible in the layout
	}
}