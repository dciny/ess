<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Digital Connection I.T Services</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style2.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

</head>
<body>

  <div class="container">
    <div id="particles-js"></div>
      <div class="main">
        <div class="login">
          <div class="logo">
            <center>
              <img src="<?php echo base_url(); ?>assets/image/logowhite.svg"  height="40%" width="60%">
            </center>
          </div>
          
          <?php if($error = $this->session->flashdata('error')): ?>
          
            <div class="alert alert-danger alert-danger-style1">
              <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                <span class="icon-sc-cl" aria-hidden="true">&times;</span>
              </button>
              <span class="adminpro-icon adminpro-checked-pro admin-check-sucess"></span>
              <p><strong><?php echo $error; ?></strong></p>
            </div>
          
          <?php endif; ?>

          <br>
          <br>
          <br>

          <center style="position: relative;">
            <form class="form" action = "<?php echo base_url('auth/login'); ?>" method="POST">
              <input type="text" class="form-control col-sm-10" name="username" placeholder="Employee ID" style="border-radius: 25px;"><br/>
              <input type="password" class="form-control col-sm-10" name="password" placeholder="Password" style="border-radius: 25px;">
              <br>
              <br>
              <input type="submit" id="btn" class="form-control col-sm-10" value=" Login " name="submit" style="border-radius: 25px;"/>
            </form>
            <p class="mt-4"><a href="<?php echo base_url('auth/forgot-password') ?>" class="text-light">Forgot Password?</a></p>
          </center> 

          <br>
          <br>
          <br>  
        </div>
      </div>
    </div>
  </div>

  <script src="https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/particles.js"></script>
  <script type="text/javascript">
    
    particlesJS.load('particles-js', '<?php echo base_url(); ?>assets/js/particles2.json', function() {
      console.log('particles.js loaded - callback');
    });

  </script>
  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  
</body>
</html>