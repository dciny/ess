<div class="box">
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<ul class="navbar-nav">
			<li class="nav-item">
				<a class="nav-link active" href="<?php echo base_url() ?>">Home</a>
			</li>
			<li class="nav-item">
				<a class="nav-link active" href="mailto:hr@digicononline.com">Contact HR</a>
			</li>
		</ul>
	</nav>

	<div class="mt-4">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-2">
					<div class="font-14">
						<ul class="list-group">
							<li class="list-group-item text-light bg-info-comp"><a href="<?php echo base_url('emergency-control/new-user') ?>">Create user</a></li>
							<li class="list-group-item text-light bg-info-comp"><a href="<?php echo base_url('emergency-control/edit-password') ?>">Update user password</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>