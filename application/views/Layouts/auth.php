<!DOCTYPE html>
<html>
<head>
	<title><?php echo $header_title ?></title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	 <!-- reset css -->
  	<link href="<?php echo base_url('assets/css/reset.css') ?>" rel="stylesheet">
  	<!-- fontawesome -->
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  	<!-- Nunito font -->
  	<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<!-- custom css -->
	<link href="<?php echo base_url('assets/css/auth-style.css') ?>" rel="stylesheet">
	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js" defer></script>
	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js" defer></script>
	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" defer></script>
	<!-- Main js -->
	<script src="<?php echo base_url('assets/js/main.js') ?>" defer></script>
	
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12 p-0">
				<?php echo $rendered_view ?>
			</div>
		</div>
	</div>
</body>
</html>