<div class="mx-auto w-80pct">

	<?php if(!empty($this->session->flashdata('error'))){ ?>
		<div class="alert alert-danger alert-dismissible fade show">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Failed!</strong> <?php echo $this->session->flashdata('error') ?>
		</div>
	<?php }elseif(!empty($this->session->flashdata('success'))){ ?>
		<?php
			$messages = $this->session->flashdata('success');
			foreach ($messages as $msg) {  ?>
				<div class="alert alert-success alert-dismissible fade show">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Success!</strong> <?php echo $msg ?>
				</div>
		<?php } ?>
	<?php } ?>

	<h5>Leave Hierarchies</h5>
	<div class="table-responsive">
		<table class="table table-sm table-bordered fontsize-14">
			<thead class='thead-dark'>
				<tr class="text-center">
					<th>Rule Name</th>
					<th>Rule Description</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
	<?php 
					if(count($leave_hierarchies) > 0){  
						foreach ($leave_hierarchies as $leave_hierarchy) {
	?>
							<tr>
								<td class="align-middle"> 
									<a href='<?php echo base_url("leave-hierarchy/edit/$leave_hierarchy->id") ?>'>
										<?php echo $leave_hierarchy->rule_name ?> 
									</a>
								</td>
								<td class="align-middle"> <?php echo $leave_hierarchy->rule_desc ?> </td>
								<td class="align-middle text-center"> 
									<button class="btn btn-sm btn-danger jrem-btn" data-id="<?php echo $leave_hierarchy->id ?>">Remove</button> 
								</td>
							</tr>
	<?php
						}
	?>

	<?php 
					}else{ 
	?>
						<tr class="text-center">
							<td colspan="4">No leave hierarchy found</td>
						</tr>
	<?php 
					} 
	?>
			</tbody>
		</table>
	</div>
</div>


<div class="modal-bg pos-fixed">
	<form action="<?php echo base_url('leave-hierarchy/delete') ?>" method="post">
		<div class="card w-400px modal-content pos-absolute">
			<div class="card-header text-secondary"><strong>Delete confirmation</strong></div>
			<div class="card-body">
				<p>Are you sure you want to delete this item?</p>
			</div>
			<div class="card-footer text-right">
				<input type="hidden" name="id">
				<input type="submit" class="btn btn-sm btn-primary px-3 mr-2" value="Yes">
				<span class="btn btn-sm btn-secondary px-3 jcancel-btn">No</span>
			</div>
		</div>
	</form>
</div>