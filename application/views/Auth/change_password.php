<div class="main container-fluid">

	<?php if($is_password_change == 1){ ?>
		<div class="row mb-5">
			<div class="col-md-12">
				<div class="ml-5">
					<a class="cta-def-btn" href="<?php echo base_url() ?>">
						<span class="back-icon">Back</span>
					</a>
				</div>
			</div>
		</div>
	<?php } ?>

	<div class="row mb-5">
		<div class="col-md-12 p-0">
			<div class="mx-5">
				<p class="text-danger"><strong>Note</strong>: Once you've updated your password, you will be logged out to enter your new credentials</p>
			</div>
			<div class="box cyan-lin-bg box-min-size mx-5">
				<h5>Change Password</h5>
				<form action='<?php echo base_url("auth/update-password") ?>' method="post">
					<div class="table-responsive">
						<table class="table">
							<tbody>
								<tr>
									<td style="width: 160px">Username:</td>
									<td>
										<input type="text" class="form-control" value="<?php display_col($username) ?>" disabled>
										<input type="hidden" name="username" value="<?php display_col($username) ?>">
									</td>
								</tr>
								<tr>
									<td style="width: 160px">Old Password:</td>
									<td>
										<input type="password" name="old_pw" class="form-control <?php if(form_error('old_pw')) echo 'is-invalid' ?>" 
										       value="<?php echo set_value('old_pw') ?>">
										<?php echo form_error('old_pw'); ?>
									</td>
								</tr>
								<tr>
									<td style="width: 160px">New Password:</td>
									<td>
										<input type="password" name="new_pw" class="form-control <?php if(form_error('new_pw')) echo 'is-invalid' ?>" 
										       value="<?php echo set_value('new_pw') ?>">
										<?php echo form_error('new_pw'); ?>
									</td>
								</tr>
								<tr>
									<td style="width: 160px">Confirm Password:</td>
									<td>
										<input type="password" name="confirm_pw" class="form-control <?php if(form_error('confirm_pw')) echo 'is-invalid' ?>" 
										       value="<?php echo set_value('confirm_pw') ?>">
										<?php echo form_error('confirm_pw'); ?>
									</td>
								</tr>
								<tr class="text-right">
									<td colspan="2">
										<input type="submit" name="submit" value="Submit" class="btn cta-btn cyan-bg-btn">
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>