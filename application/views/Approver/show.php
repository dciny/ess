<div class="mx-auto w-80pct">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12 col-lg-5">
				<h5>List Of Employees</h5>
				<div class="input-group mb-1">
					<input type="text" class="form-control form-control-sm" name="search" placeholder="Search...">
					<div class="input-group-append">
						<button class="btn btn-secondary btn-sm" type="submit">Submit</button>
					</div>
				</div>
				<div class="list-box border">
					<ul class="list-group list-group-flush">
						<?php foreach ($avail_approvers as $avail_approver) { ?>
							<li class="list-group-item p-0">
								<form action="<?php echo base_url('approver/create') ?>" method="post">
									<table class="table table-borderless table-sm fontsize-14 mb-0">
										<tbody>
											<tr class="text-center">
												<td class="align-middle" style="width: 362px">
													<?php echo implode(' ',[$avail_approver->firstname,$avail_approver->lastname]) ?>
													<input type="hidden" name="employeeid" value="<?php echo $avail_approver->employeeid ?>">
												</td>
												<td><input type="submit" name="submit" class="btn btn-sm btn-success" value="Add"></td>
											</tr>
										</tbody>
									</table>
								</form>
							</li>
						<?php } ?>
					</ul>
				</div>
			</div>
			<div class="col-md-12 col-lg-1 my-4"></div>
			<div class="col-md-12 col-lg-5">

				<?php if(!empty($this->session->flashdata('error'))){ ?>
					<div class="alert alert-danger alert-dismissible fade show">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<strong>Failed!</strong> <?php echo $this->session->flashdata('error') ?>
					</div>
				<?php } ?>

				<h5>List Of Approvers</h5>
				<div class="input-group mb-1">
					<input type="text" class="form-control form-control-sm" name="search" placeholder="Search...">
					<div class="input-group-append">
						<button class="btn btn-secondary btn-sm" type="submit">Submit</button>
					</div>
				</div>
				<div class="list-box border">
					<ul class="list-group list-group-flush">
						<?php foreach ($approvers as $approver) { ?>
							<li class="list-group-item p-0">
								<form action="<?php echo base_url('approver/delete') ?>" method="post">
									<table class="table table-borderless table-sm fontsize-14 mb-0">
										<tbody>
											<tr class="text-center">
												<td class="align-middle" style="width: 362px">
													<?php echo implode(' ',[$approver->firstname,$approver->lastname]) ?>
													<input type="hidden" name="employeeid" value="<?php echo $approver->employeeid ?>">
												</td>
												<td><input type="submit" name="submit" class="btn btn-sm btn-danger" value="Remove"></td>
											</tr>
										</tbody>
									</table>
								</form>
							</li>
						<?php } ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
