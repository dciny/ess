<div class="container-fluid">
  <div class="row mb-4">
    <div class="col-md-12">
      <div class="ml-4">
        <a class="cta-def-btn" href="<?php echo base_url(); ?>">
          <span class="back-icon">Back to home</span>
        </a>
      </div>
    </div>
  </div>
</div>

<div class="border mx-4 leave-dashboard">
	<div class="border-bottom">
		<ul class="d-flex m-0">
			<li class="p-4 leave-tab-active">Dashboard</li>
			<li class="p-4"><a href="<?php echo base_url('leave/apply') ?>">Apply Leave</a></li>
			<?php if($user_details->leave_role_id == 1){ ?>
				<li class="p-4"><a href="<?php echo base_url('leave/delegate') ?>">Leave Delegate</a></li>
			<?php } ?>
			<?php if($is_approver){ ?>
				<li class="p-4"><a href="<?php echo base_url('leave/requests') ?>">Leave Requests</a></li>
				<li class="p-4"><a href="<?php echo base_url('leave/approver/leave-count-page') ?>">Leave Count</a></li>
			<?php } ?>
			<?php if($user_details->leave_role_id == 1){ ?>
				<li class="p-4"><a href="<?php echo base_url('leave/tracker') ?>">Leave Tracker</a></li>
			<?php } ?>
			<?php if($user_details->leave_role_id == 1){ ?>
				<li class="p-4"><a href="<?php echo base_url('leave/monthly-report/approved') ?>">Monthly Tracker</a></li>
			<?php } ?>
		</ul>
	</div>
	<div class="p-3 leave-dashboard-grid font-14">
		
		<div class="border d-flex flex-column">
			<div class="border-bottom mb-3">
				<div class="border-bottom p-3 leave-dash-head"><strong>Leave Summary</strong></div>
				<div class="d-flex">
					<div class="py-3 flex-fill text-center d-flex flex-column">
						<span>Total Leaves</span>
						<span><?php echo $is_assigned ? $leave_total : 'N/A' ?></span>
					</div>
					<div class="py-3 flex-fill border-left border-right text-center d-flex flex-column">
						<span>Used Leaves</span>
						<span><?php echo $is_assigned ? $leave_used : 'N/A' ?></span>
					</div>
					<div class="py-3 flex-fill text-center d-flex flex-column">
						<span>Balance Leaves</span>
						<span><?php echo $is_assigned ? $leave_balance : 'N/A' ?></span>
					</div>
				</div>
			</div>

			<div class="border-top">
				<div class="border-bottom p-3 leave-dash-head"><strong>Leave Approvers</strong></div>
				<div style="padding: 12px">
					<?php
						$ctr = 1;
						while(count($approvers) > 0){
							echo "<strong>Approver $ctr</strong>";
							echo "<ul>";
							foreach ($approvers as $key => $approver) {
								if($approver->approver_rank == $ctr){
									$name = $approver->firstname . ' ' . $approver->lastname;
									echo "<li>$name</li>";
									unset($approvers[$key]);
								}
							}
							echo "</ul>";
							$ctr++;
						}
					?>
				</div>
			</div>
		</div>

		<div class="border">
			<div class="p-3 leave-dash-head"><strong>Applied Summary</strong></div>
			<div>
				<div class="table-responsive">
					<table class="table">
						<thead class="thead-light">
							<tr>
								<th>Dates</th>
								<th>Type</th>
								<th>Category</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
							<?php
								if($is_assigned){
									foreach ($myleaves as $myleave){
										$date = date('m/d/Y',strtotime($myleave->leavedt));
										$url = base_url('leave/show/' . $myleave->id);
										echo "<tr>";
										echo "<td><a href='$url'>$date</a></td>";
										echo "<td>$myleave->leave_name</td>";
										echo "<td>" . ucwords($myleave->leave_category) . "</td>";
										echo "<td>$myleave->status_name</td>";
										echo "</tr>";
									}
								}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>

	</div>
</div>