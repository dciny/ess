<?php
  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Leave_type extends CI_Controller {

	public function __construct(){

		parent::__construct();
		
		$this->load->model('leave_type_model','leave_type');
		$this->load->helper('url');
	}

	
	public function index(){
		$data['leave_types'] = $this->leave_type->all();
		$data['ind'] = 'LT'; // used in the sidebar of the layout
		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Leave_type/index',$data,'leave');
	}

	public function new(){
		$data['ind'] = 'LT'; // used in the sidebar of the layout
		$this->load->library('layouts'); // load layout library
		$this->layouts->view('Leave_type/new',$data,'leave');
	}

	public function create(){
		$this->form_validation->set_error_delimiters('<p class="invalid-feedback m-0">', '</p>');
		$this->form_validation->set_rules('leave_name', 'Leave type', 'trim|required|is_unique[leave_types.leave_name]');

		$leave['leave_name'] = $this->input->post('leave_name');
		$leave['leave_desc'] = $this->input->post('leave_desc');

		if($this->form_validation->run() == FALSE){
			// show again the edit form
			$data['ind'] = 'LT'; // used in the sidebar of the layout
			$this->load->library('layouts'); // load layout library
			$this->layouts->view('Leave_type/new',$data,'leave');
		}else{
			$this->leave_type->create($leave);
			redirect(base_url('leave-type'),'location');
		}

	}

	public function delete(){
		$leave['id'] = $this->input->post('id');
		$this->db->delete('leave_types', $leave);

		redirect(base_url('leave-type'),'location');
	}

}
