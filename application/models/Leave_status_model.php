<?php

  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Leave_status_model extends CI_Model{

	public function all(){
		$query = $this->db->where('id !=', 3)->get('leave_statuses');

		return $query->result();
	}


	public function create($leave){
		date_default_timezone_set('Asia/Kuala_Lumpur');

		$leave['createdttm'] = date('Y-m-d H:i:s');
		$leave['updatedttm'] = date('Y-m-d H:i:s');

		$this->db->insert('leave_statuses',$leave);
		return $this->db->affected_rows();
	}

	public function get_max_rank_status($leave_request_id){
		$max = (object) ['approver_rank' => 0, 'leave_status_id' => 5]; // leave status 5 for approved

		$this->db->select('approver_rank,leave_status_id');
		$this->db->from('leave_request_statuses');
		$this->db->where('leave_request_id', $leave_request_id);
		$this->db->order_by('approver_rank','desc');
		$this->db->limit(1);
		$query = $this->db->get();

		if($query->num_rows() > 0){
			$max = $query->row();
		}

		return $max;
	}

	public function get_next_rank($leave_request_id, $rank_id){
		$this->db->select('ah.approver_rank');
		$this->db->from('approver_hierarchies ah');
		$this->db->join('leave_assignments la','la.leave_hierarchy_id = ah.leave_hierarchy_id');
		$this->db->join('leave_requests lr','lr.employeeid = la.employeeid');
		$this->db->where('lr.id',$leave_request_id);
		$this->db->where('ah.is_active',1);
		$this->db->where('ah.approver_rank > ', $rank_id);
		$this->db->group_by('ah.approver_rank');
		$this->db->order_by('ah.approver_rank');
		$this->db->limit(1);
		$query = $this->db->get();

		if($query->num_rows() > 0)
			return $query->row()->approver_rank;
		return 0;


	}

	public function get_rank_status($leave_request_id, $rank){
		$this->db->select('ls.id,ls.status_name');
		$this->db->from('leave_request_statuses lrs');
		$this->db->join('leave_statuses ls','ls.id = lrs.leave_status_id','left');
		$this->db->where('lrs.leave_request_id', $leave_request_id);
		$this->db->where('lrs.approver_rank', $rank);
		$query = $this->db->get();

		if($query->num_rows() > 0){
			return $query->row()->status_name;
		}

		return '';
		
	}
}

